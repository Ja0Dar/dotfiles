#!/usr/bin/env bash

ensure_arch_distrobox() {
    image="ghcr.io/ublue-os/arch-distrobox"

    if distrobox list | grep "$image" | grep -q " arch "; then
        printf ""
    else
        distrobox-create --name arch --image "$image" --pull
        # Then (studio-3t needs swt)
        distrobox enter arch -- bash -c "paru -Syu --noconfirm jdk17-openjdk studio-3t swt"
    fi
}

ensure_flatpak_app() {
    app="$1"
    if ! flatpak list | grep -q "$app"; then
        flatpak install flathub "$app"
    fi
}

ensure_webui_running() {
    if podman ps | grep -q open-webui; then
        echo "open-webui is already running"

    elif podman ps -a | grep -q open-webui; then
        podman start open-webui
    else
        podman run -p 127.0.0.1:3000:8080 --network=pasta:-T,11434 --add-host=ollama.local:127.0.0.1 \
            --env 'OLLAMA_BASE_URL=http://ollama.local:11434' --env 'ANONYMIZED_TELEMETRY=False' \
            -v open-webui:/app/backend/data --label io.containers.autoupdate=registry \
            --rm \
            --name open-webui ghcr.io/open-webui/open-webui:main

    fi
}

case "$(basename "$0")" in
    "kasts")
        ensure_flatpak_app org.kde.kasts
        flatpak run org.kde.kasts
        ;;

    wire-desktop)
        ensure_flatpak_app com.wire.WireDesktop
        # FIXME - remove last part when upstream fixes https://github.com/wireapp/wire-desktop/issues/7764
        flatpak run com.wire.WireDesktop --enable-features=UseOzonePlatform --ozone-platform=wayland --password-store=gnome-libsecret
        ;;
    "studio-3t" | "studio3t")
        ensure_arch_distrobox
        distrobox enter arch -- /usr/sbin/studio-3t
        ;;

    "openwebui")
        if [ "$1" == "down" ]; then
            podman stop open-webui
        else
            ensure_webui_running
        fi
        ;;

    "help")
        echo "launcher for non-nix native apps"
        ;;
esac
