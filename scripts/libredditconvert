#!/usr/bin/env python3

import sys
import argparse
from typing import Dict, Any, List
from urllib.parse import unquote, quote
import json
import webbrowser



# https://libredd.it/settings/restore/?theme=system&front_page=default&layout=card&wide=off&comment_sort=confidence&show_nsfw=on&use_hls=on&hide_hls_notification=off&subscriptions=oddlysatisfying%2BUnixHumor
def dict_to_url(instance: str, s: Dict[str, Any]) -> str:
    query = "?"
    for key, value in s.items():
        if key == "subscriptions":
            value = quote("+".join(value))
        query = f"{query}{key}={value}&"
    query = query[:-1]
    return f"https://{instance}/settings/restore/{query}"


def url_to_dict(url: str) -> Dict[str, Any]:
    # https://libredd.it/settings/restore/?theme=system&front_page=default&layout=card
    # &wide=off&comment_sort=confidence&show_nsfw=on&use_hls=on&hide_hls_notification=off&subscriptions=oddlysatisfying%2BUnixHumor
    res = dict()

    for kw in [kw.split("=") for kw in url.split("?")[1].split("&")]:
        key = kw[0]

        if key == "subscriptions":
            value = unquote(kw[1]).split("+")
        else:
            value = kw[1]
        res[key] = value
    return res


def restore(backup_fn: str, instances: List[str]) -> None:

    with open(backup_fn, "r") as f:
        backup_dict = json.load(f)

        for instance in instances:
            url = dict_to_url(instance, backup_dict)

            if url:
                webbrowser.open_new_tab(url)


def cmdline_args():
    # Make parser object
    p = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )

    group1 = p.add_mutually_exclusive_group(required=True)
    group1.add_argument(
        "--to-json",
        help="Convert libreddit settings url form arg or stdin to json.",
        action="store_true",
    )
    group1.add_argument(
        "--to-url",
        help="Convert libreddit json to url form arg or stdin.",
        action="store_true",
    )

    group1.add_argument(
        "--restore",
        help="Backup json filename to restore from",
    )

    p.add_argument("url", nargs="?")

    return p.parse_args()


if __name__ == "__main__":
    global_instance = "reddit.baby"
    # https://github.com/libreddit/libreddit-instances/blob/master/instances.md

    if sys.version_info < (3, 5, 0):
        sys.stderr.write("You need python 3.5 or later to run this script\n")
        sys.exit(1)

    cmdline_args()
    try:
        args = cmdline_args()

        if args.restore:
            restore(args.restore, [global_instance])
            exit(0)

        if args.url is None:
            body = ""
            for line in sys.stdin:
                body = body + line.rstrip()
        else:
            body = args.url

        if args.to_json:
            print(json.dumps(url_to_dict(body)))
        else:
            print(dict_to_url(global_instance, json.loads(body)))

    except Exception as e:
        print(e)
        print("Try libredditconvert --help")

    print()

if __name__ == "__main__":
    pass
