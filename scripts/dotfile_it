#!/usr/bin/env bash

set -euo pipefail

if [ "$#" -ne 1 ]; then
    echo "Usage: $(basename "$0") <file_or_directory>"
    echo "Must be run from within ~/.config/"
    exit 1
fi

# Verify we're in ~/.config
if [[ $(pwd) != "$HOME/.config"* ]]; then
    echo "Error: Must be run from within ~/.config/"
    exit 1
fi

target="$1"
full_path="$(realpath "$target")"
rel_path="${full_path#"$HOME/.config/"}"
repo_base="/home/owner/git/dotfiles"

# Let user select the target directory using sk
selected_dir=$(echo -e "common\nlinux" | sk --prompt="Select target directory: ")
sym_dir="$repo_base/sym/$selected_dir"

dest_path="$sym_dir/dot_config/$rel_path"

# Create parent directories if they don't exist
mkdir -p "$(dirname "$dest_path")"

# Copy the file/directory to the repository
if [ -d "$full_path" ]; then
    cp -r "$full_path" "$dest_path"
else
    cp "$full_path" "$dest_path"
fi

# Remove the original and create a symbolic link
mv "$full_path" "$full_path.bak"
ln -s "$dest_path" "$full_path"

echo "Moved $rel_path to repository and created symbolic link in $selected_dir"
