{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  buildInputs = with pkgs; [

    #TODO - checkout nixos kali
    ### Recon
    dnsrecon
    whois
    theharvester


    ### Exploit
    # metasploit
    # sqlmap
    # beef # Browser Exploitation Framweork


    ### Wireless
    aircrack-ng
    # macchanger



    ###  Network
    # p0f  # Passive Network fignerprinting https://lcamtuf.coredump.cx/p0f3/
    nmap
    bettercap
    # mitmproxy
    # sslsplit # https://github.com/droe/sslsplit
    hping
    # wfuzz
    # zaproxy


    ### Passwords
    # thc-hydra
    # john

    ### Media manipulation

    exiv2 # exif tool
    # zbar        # qr code commandline reader
    # exifprobe   # exif tool

    ### Binary exploitation etc
    # ghidra-bin
    # gdb
    # python39Packages.binwalk # or binwalk-full

    ### Misc
    # pwntools # For ctf https://github.com/Gallopsled/pwntools#readme
  ];
}
