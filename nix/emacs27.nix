{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  buildInputs = with pkgs; [ emacs27 ];

  shellHook = ''
    PATH=${pkgs.emacs27}:$PATH
  '';

}
