{
  description = "Minimal NixOS installation media";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/24.05";
  outputs = { self, nixpkgs }: {
    nixosConfigurations = {
      exampleIso = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ({ pkgs, modulesPath, ... }: {
            imports = [ (modulesPath + "/installer/cd-dvd/installation-cd-graphical-calamares-plasma6.nix") ];
            boot.kernelPackages = pkgs.linuxPackages_latest;
            boot.supportedFilesystems.zfs = pkgs.lib.mkForce false;

            environment.systemPackages = [ pkgs.neovim ];
          })
        ];
      };
    };
  };
}
