# -*- mode: snippet -*-
# name: Scala file template
# key: __.scala
# --
package `(+scala-current-package)`

object `(+scala-current-class)` {
    $0
}