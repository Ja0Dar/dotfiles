;;; +ox-jira.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Jakub Darul
;;
;; Author: Jakub Darul <https://github.com/JakDar>
;; Maintainer: Jakub Darul
;; Created: February 12, 2022
;; Modified: February 12, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/JakDar/+ox-jira
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:


(defadvice! +ox-jira-link (link desc info)
  :override 'ox-jira-link
  (let* ((type (org-element-property :type link))
         (raw-path (org-element-property :path link))
         (smart-link (s-ends-with? "|smart-link" desc))
         (smart-card (s-ends-with? "|smart-card" desc))
         (desc (and (not (string= desc "")) (string-remove-suffix "|smart-card" (string-remove-suffix "|smart-link" desc))))
         (path (cond
                ((member type '("http" "https" "ftp" "mailto" "doi"))
                 (concat type ":" raw-path))
                ((string-prefix-p "~accountid" raw-path)
                 raw-path)
                ((string= type "file")
                 (org-export-file-uri raw-path))
                ((string= type "custom-id")
                 (if desc (concat "#" desc) (concat "#" raw-path)))
                ((string-prefix-p "*" raw-path)
                 (concat "#" (seq-subseq raw-path 1)))
                ((org-export-custom-protocol-maybe link desc 'jira info))
                (t raw-path))))
    (cond
     ;; Link with description and suffix
     ((and path desc smart-link) (format "[%s|%s|smart-link]" desc path))
     ((and path desc smart-card) (format "[%s|%s|smart-card]" desc path))
     ;; Link with description
     ((and path desc) (format "[%s|%s]" desc path))
     ;; Link without description
     (path (format "[%s]" path))
     ;; Link with only description?!
     (t desc))))



(provide '+ox-jira)
;;; +ox-jira.el ends here
