;;; conf/my-base.el -*- lexical-binding: t; -*-

;; FIXME doom doesn't play well here with newest dirvish
(setq dired-omit-files "\\`[.]?#\\|\\`[.][.]?\\'")

(setq
 shell-file-name (executable-find "bash")
 doom-symbol-font (font-spec :family "Noto Color Emoji")
 ;; Patched version of ligatured hack  available in this repo
 doom-font (font-spec :family "Hack FC Ligatured" :size 15)

;; FIXME: broke after update, not sure why
;; doom-variable-pitch-font (font-spec :family "Iosevka Comfy Duo" :size 20)


 ;; NOTE: raising log level to supress emacs-jupyter <-> org mode warnings
 warning-minimum-level :error
 doom-font-increment 1)

(setq doom-localleader-key "C-,")
(setq doom-localleader-alt-key doom-localleader-key)

(put 'narrow-to-region 'disabled nil)


;; company-ispell config from : https://emacs.stackexchange.com/a/54742
;; Dictionary for completion.
(setq ispell-complete-word-dict
      (expand-file-name (concat user-emacs-directory "aspell_words.txt")))


;;speedup Tramp
(setq
 tab-width 4
 dired-auto-revert-buffer nil
 auth-source-do-cache nil
 history-length 50
 vc-ignore-dir-regexp
 (format "%s\\|%s"
         vc-ignore-dir-regexp
         tramp-file-name-regexp)
 tramp-verbose 1
 ;; tramp-completion-reread-directory-timeout nil
 tramp-use-ssh-controlmaster-options nil
 remote-file-name-inhibit-cache nil
 auth-sources '("~/.authinfo.gpg"))

;;Housekeeping
(when (eq system-type 'gnu/linux)
  (setq x-select-enable-clipboard-manager nil))


(setq display-line-numbers-type 'relative
      global-auto-revert-mode t
      pixel-scroll-mode t
      evil-collection-outline-enable-in-minor-mode-p nil
      evil-emacs-state-cursor 'bar
      evil-want-fine-undo t
      browse-url-chromium-program "brave"
      browse-url-browser-function #'browse-url-firefox


      ;; Let's tweak modeline a little remove time, I
      doom-modeline-buffer-file-name-style 'truncate-with-project
      doom-modeline-time-icon  nil
      doom-modeline-time nil)


(after! persp-mode
  (require 'jworkspaces)

  (map!
   (:leader
    :prefix "TAB"
    "<" #'+workspace/swap-left
    ">" #'+workspace/swap-right)
   "M-[" #'+workspace:switch-previous
   "M-]" #'+workspace:switch-next))


(require 'jthemes)
(autoload 'jspell-mode "/home/owner/git/dotfiles/doom_d/lib/jspell.el"
  "a wrapper for spell-fu + company ispell")
(autoload 'j/util-open-or-create-bug "/home/owner/git/dotfiles/doom_d/lib/jutil.el")

;; TODO make it more visible :https://protesilaos.com/emacs/modus-themes  section 6.3.
(setq modus-themes-org-blocks 'gray-background)
(require 'jevil-symbol-mode)

;; Save all buffers when Emacs loses focus
(add-function :after after-focus-change-function #'j/save-all-if-not-popup)
(add-hook 'doom-switch-window-hook #'j/save-all-if-not-popup)
