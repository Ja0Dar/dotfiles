;;; con/my-org.el -*- lexical-binding: t; -*-


;; (require 'my-org-excalidraw)


(setq ob-mermaid-cli-path "/home/owner/.nix-profile/bin/mmdc")

(after! ob-jupyter
  ;; FIXME: workaround from https://github.com/doomemacs/doomemacs/issues/7354#issuecomment-1872392448
  (org-babel-jupyter-aliases-from-kernelspecs)
  (pushnew!  jupyter-org-mime-types :text/html))

(defun ++org-init-smartparens-h()
  (require 'smartparens-org)
  ;; TODO make it work
  ;; (require 'rotate-text)
  ;; (require 'rotate-text-paren)
  ;; (pushnew! rotate-text-local-paren-pairs '("==" "~~"  ))
  )

(defun ++org-init-jagenda-h() ;; TODO Should be in jagenda?
  (setq
   org-agenda-window-setup 'only-window
   org-agenda-skip-additional-timestamps-same-entry t ;; So that entries from org-gtd don't duplicate in calfw/ regular agenda
   org-agenda-show-future-repeats nil
   org-agenda-start-on-weekday nil
   org-agenda-start-day "today"
   org-deadline-warning-days 7
   org-log-done 'time
   org-agenda-block-separator 9472  ;; straight line
   org-columns-default-format-for-agenda "%5TODO %40ITEM(Task) %PRIORITY(Priority) %17Effort(Estimated Effort){:} %CLOCKSUM %40TAGS"
   org-agenda-tags-column 100
   org-habit-graph-column 80
   org-agenda-compact-blocks t
   diary-show-holidays-flag nil
   org-agenda-breadcrumbs-separator " / "
   org-agenda-prefix-format '((agenda . " %i %-12:c%?-12t% s")
                              (timeline . "  % s")
                              (todo . "%i %-12:c%b")
                              (tags . "%i %-12:c%b")
                              (search . " %i %-12:c"))))

(defun j/org-init-capture-defaults-h()
  "."
  (setq
   org-load-parrot t
   org-default-notes-file
   (expand-file-name +org-capture-notes-file org-directory)
   +org-capture-journal-file
   (expand-file-name +org-capture-journal-file org-directory)
   org-capture-templates
   `(("t" "Todo" entry
      (file+headline ,#'org-gtd-inbox-path "Inbox")
      "* [#C] %?‍\n:PROPERTIES:\n:CAPTURE_TIME: %U\n:END:\n%i\n%a" ;; Using "ZERO WIDTH JOINER" at end of line to leave the space after priority 🤷‍♂️
      :kill-buffer t)
     ("p" "Protocol" entry
      (file+headline ,#'org-gtd-inbox-path "Inbox")
      "* [#C] %?‍\n:PROPERTIES:\n:CAPTURE_TIME: %U\n:END:\n From: [[%:link][%:description]]\n#+BEGIN_QUOTE\n%i\n#+END_QUOTE\n" :kill-buffer t)
     ("L" "Protocol Link" entry
      (file+headline ,#'org-gtd-inbox-path "Inbox")
      "* [#C] Checkout %? ‍\n:PROPERTIES:\n:CAPTURE_TIME: %U\n:END:\n Link: [[%:link][%:description]]" :kill-buffer t)
     ))

  (require 'j-inbox))

(defun ++org-init-mine-h()
  (setq org-directory "~/next/notes"
        org-agenda-files '(;; "~/next/notes/gtd/inbox.org"
                           "~/next/notes/gtd/mobile.org"
                           "~/next/notes/gtd/org-gtd-tasks.org"
                           "~/next/notes/gtd/recurring.org"
                           "~/next/notes/gtd/remote-calendar.org"
                           "~/next/notes/gtd/tickler.org"
                           ;; "~/.jira/CD.org"
                           )
        ;; NOTE: I am refiling all captures anyway. Sorting with refiling with capturing causes inbox.org to loose data.
        +org-capture-todo-file "~/tmp/.capture-scratch.org"
        +org-capture-notes-file "~/next/notes/cabinet/scratch.org"
        +org-capture-journal-file "~/next/notes/cabinet/journal.org"
        org-refile-targets '(("~/next/notes/gtd/org-gtd-tasks.org" :level . 2)
                             ("~/next/notes/gtd/defer/someday.org" :maxlevel . 3)
                             ("~/next/notes/gtd/defer/checkout.org" :level . 2)
                             ("~/next/notes/gtd/inbox.org" :level . 1)
                             ("~/next/notes/gtd/recurring.org" :level . 1)
                             ("~/next/notes/gtd/remote-calendar.org" :level . 2))
        org-contacts-files '("~/next/notes/cabinet/acquaintances.org")
        org-contacts-birthday-format "%h: (%y) birthday. %l"
        org-clock-sound "/home/owner/next/config/end.wav"
        org-tag-persistent-alist '(("@work" . ?w) ("@home" . ?h) ("@me" . ?m) ("@config" . ?c) ("week9to5" . ?d) ("today" . ?t))
        org-tag-alist nil))

(remove-hook! 'org-load-hook
  #'+org-init-smartparens-h
  #'+org-init-agenda-h
  #'+org-init-habit-h
  #'+org-init-capture-defaults-h)

(after! org
  (++org-init-smartparens-h)
  (++org-init-jagenda-h)
  (++org-init-mine-h)
  (j/org-init-capture-defaults-h)

  ;; FIXME - remove this when https://github.com/doomemacs/doomemacs/pull/7509 is merged
  (remove-hook 'org-mode-hook #'+org-make-last-point-visible-h))

(defun ++custom-set-faces() ;TODO:check if it can be better initialised
  "Set my preferred org faces. Needs to be set after org vcolumns."
  (if (and (eq major-mode 'org-agenda-mode) (fboundp 'elegant-agenda--enable)) (elegant-agenda--enable))
  (custom-set-faces!
    '(org-beamer-tag :background "gray" :box nil :height 1.0) ;; FIX for company-box
    '(outline-1 :weight bold :height 1.25)
    '(outline-2 :weight bold :height 1.15)
    '(outline-3 :weight bold :height 1.12)
    '(outline-4 :weight bold :height 1.09)
    '(shr-h1 :height 1.4 :foreground "#51afef" )
    '(shr-h2 :height 1.2 :foreground "#c678dd" )
    '(shr-h3 :height 1.1 :foreground "#a9a1e1" ))


  (set-face-attribute 'org-document-title nil
                      :foreground "dark grey"
                      :background 'unspecified
                      :height 1.7
                      :weight 'normal
                      :family "IM FELL English"))

(defun ++org-make-outlines-fixed-height(&optional global columns-fmt-string)
  (custom-set-faces!
    '(outline-1 :weight bold :height 1.0)
    '(outline-2 :weight bold :height 1.0)
    '(outline-3 :weight bold :height 1.0)
    '(outline-4 :weight bold :height 1.0)))

(setq org-priority-lowest ?F) ;; For ejira to work

(after! ox-jira
  (require '+ox-jira))

(use-package! ox-gfm
  :when (executable-find "pandoc")
  :after ox
  :init
  (add-to-list 'org-export-backends 'gfm))

(after! ox-slack
  (require '+ox-slack))

(use-package! org-contacts
  :commands org-contacts-anniversaries)

(use-package! org-appear
  :hook (org-mode . org-appear-mode) ;; that one is default
  :config
  (setq org-appear-autoemphasis t
        org-appear-autosubmarkers t
        org-appear-autolinks nil
        org-hide-emphasis-markers t
        )
  ;; for proper first-time setup, `org-appear--set-elements'
  ;; needs to be run after other hooks have acted.
  (run-at-time nil nil #'org-appear--set-elements))





(defun j/setup-jagenda-h()
  (after! org-agenda
    (require 'jagenda) ;FIXME make it not load after mu4e or org?
    (add-hook! 'org-agenda-mode-hook #'org-super-agenda-mode)
    (add-hook! 'org-agenda-mode-hook #'org-edna-mode)
    (add-hook! 'org-agenda-mode-hook #'elegant-agenda-mode)

    (remove-hook 'org-agenda-mode-hook #'+org-habit-resize-graph-h )
    (advice-add 'org-agenda-todo :after #'org-save-all-org-buffers)))

;; (add-hook! doom-load-theme-hook #'++custom-set-faces)
(defun j/daemon-init-ui-h(frame)
  (message "Setup custom ran")
  (++custom-set-faces)
  (j/setup-jagenda-h)

  (remove-hook 'window-buffer-change-functions #'j/daemon-init-ui-h))

(add-hook 'window-buffer-change-functions #'j/daemon-init-ui-h)

(use-package! org-alert
  :after org-agenda
  :config
  (setq alert-default-style 'libnotify
        org-alert-interval 300 ;; seconds
        org-alert-notify-cutoff 10
        org-alert-notify-after-event-cutoff 60
        org-alert-notification-title "Agenda alert")
  (org-alert-enable))

(after! org
  (require 'jorg)
  (when (and (daemonp) (fboundp 'nov)) (require 'nov))

  (defun j/org-sort-my-tasks()
    "Open org-gtd-tasks.org, find 'Actions' heading, and run j/org-sort-custom there."
    (interactive)
    (let ((file-path "/home/owner/next/notes/gtd/org-gtd-tasks.org")
          (current-buf (current-buffer))
          (current-pos (point)))
      (save-window-excursion
        (with-temp-buffer
          (find-file file-path)
          (widen)
          (goto-char (point-min))
          (if (re-search-forward ":ORG_GTD:\\s-*Actions" nil t)
              (progn
                (org-back-to-heading t)
                (j/org-sort-custom)
                (save-buffer))
            (message "Actions heading not found in the file."))))
      (switch-to-buffer current-buf)
      (goto-char current-pos)))

(defun j/update-inbox-count ()
  "Update Inbox heading with subtree count."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (when (re-search-forward "^\\* Inbox" nil t)
      (let ((count (j/count-subtree-items (point-at-bol))))
        (org-set-tags (format "%d" count))))))

  (setq org-tree-slide-cursor-init nil)


  (setq org-re-reveal-theme "white"
        org-re-reveal-transition "slide"
        org-re-reveal-plugins '(markdown notes math search zoom))

  ;; (add-hook! org-after-refile-insert #'j/org-sort-partent-if-gtd-h)
  (defun j/org-remove-capture-artifacts-h()
    "Open inbox.org, remove ZERO WIDTH JOINER (inserted in templates)"
    (interactive)
    (if-let ((file-path (file-exists-p! "/home/owner/next/notes/gtd/inbox.org"))
             (current-buf (current-buffer))
             (current-pos (point)))
        (save-window-excursion
          (with-temp-buffer
            (find-file file-path)
            (widen)
            (goto-char (point-min))
            (j/emacs--replace-in-buf "‍" "")))
      (switch-to-buffer current-buf)
      (goto-char current-pos)))
  ;; (add-hook! org-after-refile-insert #'j/org--remove-zero-width-from-heading-h)
  (add-hook! org-capture-after-finalize #'j/org-remove-capture-artifacts-h)


  (setq org-list-demote-modify-bullet '(("+" . "-") ("-" . "+") ("*" . "+") ("1." . "a.")))

  (after! company-files
    ;; Fix `company-files' completion for org (file|pdf):* links
    (add-to-list 'company-files--regexps "\\(file\\|pdf\\):\\(\\(?:\\.\\{1,2\\}/\\|~/\\|/\\)[^\]\n]*\\)"))

  (custom-declare-face '+org-todo-next
                       '((t (:inherit (bold font-lock-constant-face org-todo)
                             :foreground "DodgerBlue"))) "")

  (setq org-todo-keywords
        '((sequence
           "TODO(t)"  ; A task that needs doing & is ready to do
           "NEXT(n)"  ; A next action
           "PROJ(p)"  ; A project, which usually contains other tasks
           "LOOP(r)"  ; A recurring task
           "STRT(s)"  ; A task that is in progress
           "WAIT(w)"  ; Something external is holding up this task
           "HOLD(h)"  ; This task is paused/on hold because of me
           "|"
           "DONE(d)"  ; Task successfully completed
           "KILL(k)") ; Task was cancelled, aborted or is no longer applicable
          (sequence
           "[ ](T)"   ; A task that needs doing
           "[-](S)"   ; Task is in progress
           "[?](W)"   ; Task is being held up or paused
           "|"
           "[X](D)")  ; Task was completed
          (sequence
           "|"
           "YES(Y)"
           "NO(N)"))
        org-todo-keyword-faces
        '(("[-]"  . +org-todo-active)
          ("STRT" . +org-todo-active)
          ("NEXT" . +org-todo-next)
          ("[?]"  . +org-todo-onhold)
          ("WAIT" . +org-todo-onhold)
          ("HOLD" . +org-todo-onhold)
          ("PROJ" . +org-todo-project)
          ("KILL" . +org-todo-cancel)
          ("NO" . +org-todo-cancel))
        org-columns-default-format "%5TODO %40ITEM(Task) %PRIORITY(Priority) %17Effort(Estimated Effort){:} %CLOCKSUM %40TAGS")

  ;; NOTE: aplay is troublesome with my nix-on-arch (probably my fault).
  (defadvice! org-clock-play-sound-mine (&optional clock-sound)
    :override 'org-clock-play-sound
    (when (and (stringp org-clock-sound) (executable-find "mpv") (file-exists-p org-clock-sound))
      (start-process "org-clock-play-notification" nil
		     "mpv" "--start=0" "--loop=3" org-clock-sound)))

  ;; FIXME: can fix or remove src emph? https://emacs.stackexchange.com/questions/63643/noweb-references-in-sh-blocks-breaks-the-syntax-highlighting
  ;; (require 'org-src-emph)

  (defun locally-defer-font-lock ()
    "Set jit-lock defer and stealth, when buffer is over a certain size."
    (when (> (buffer-size) 50000)
      (setq-local jit-lock-defer-time 0.05
                  jit-lock-stealth-time 1)))

  (defun +org-hook-my-h()
    (setq-local line-spacing 0.15))

  (add-hook! org-mode
             #'locally-defer-font-lock
             #'++custom-set-faces
             #'+org-hook-my-h)

  (defun +display-ansi-colors ()
    (ansi-color-apply-on-region (point-min) (point-max)))

  (add-hook 'org-babel-after-execute-hook #'+display-ansi-colors)

  (setq
   org-priority-faces
   '((?A . error)
     (?B . warning)
     (?C . success)
     (?D . success)
     (?E . org-formula)
     (?F . org-formula)
     ))


  (advice-add 'org-columns :after #'++org-make-outlines-fixed-height)
  (advice-add 'org-agenda-columns :before #'elegant-agenda--disable)
  (advice-add 'org-columns-quit :after #'++custom-set-faces)

  (add-to-list 'org-modules 'org-habit t)
  (require 'org-contacts)
  (require 'org-id) ;;NOTE: required for org-ciontacts-anniversaries
  (set-popup-rule! "^\\*Org Src" :ignore t)

  ;; Make background for generated pngs
  (defun j/org--create-image-with-background-color (args)
    "Specify background color of Org-mode inline image through modify `ARGS'. From https://emacs.stackexchange.com/a/37927"
    (let* ((file (car args))
           (type (cadr args))
           (data-p (caddr args))
           (props (cdddr args)))
      ;; get this return result style from `create-image'
      (append (list file type data-p)
              ;; (list :background (face-background 'default))
              (list :background "#EFFAFF")
              props)))

  (advice-add 'create-image :filter-args
              #'j/org--create-image-with-background-color)

  (setq org-export-with-sub-superscripts nil ;; To avoid x_i being subscript
        org-export-with-smart-quotes nil ;; REVIEW: benefitial or not?
        org-html-validation-link nil
        org-ellipsis "  " ;; folding symbol
        org-fontify-done-headline t
        org-fontify-quote-and-verse-blocks t
        org-cycle-separator-lines -1 ;; HACK for broken org-elipsis
        )

  (plist-put! org-format-latex-options :scale 1.28 :background "Transparent")


  (org-defkey org-mode-map [(meta return)] 'org-meta-return)
  (map! :map org-mode-map
        "C-c M-w" #'j/org-refile-local)

  (defun ++org-archive-save-buffer-h ()
    (let ((afile (car (org-all-archive-files))))
      (if (file-exists-p afile)
          (let ((buffer (find-file-noselect afile)))
            (with-current-buffer buffer
              (save-buffer)))
        (message "Ooops ... (%s) does not exist." afile))))

  (add-hook 'org-archive-hook '++org-archive-save-buffer-h)

  (defun j/org-archive-subtree-dont-save-to-kill-ring ()
    "Like `org-archive-subtree', but don't save to kill ring.
Prevents e.g. crashing copyq when archiving subsequent subtrees."
    (interactive)
    (let ((select-enable-clipboard nil))
      (call-interactively #'org-archive-subtree)))


  (setq org-archive-default-command 'j/org-archive-subtree-dont-save-to-kill-ring)



  (defun ++org-update-all-statistics-cookies-adv (&optional a)
    "For wehen we archive last entry and our point is on next parent-level heading.
Otherwise it is handled by saving after archive."
    (save-excursion
      (org-backward-heading-same-level nil)
      (org-update-statistics-cookies nil)))

  (advice-add 'org-archive-subtree :after #'++org-update-all-statistics-cookies-adv)

  ;;  EMBARK - org links (https://github.com/Porcupine96/emacs-config/commit/a28ac52ae4570e5f32647255cfdc83158b7f7d5d)

  (after! embark
    (require '+embark-org)))


(use-package! org-modern
  :hook (org-mode . org-modern-mode)
  :config

  (add-hook 'org-agenda-finalize-hook #'org-modern-agenda) ;; Probably stupid to call  it here
  (setq ;; org-modern-star '("◉" "○" "✸" "✿" "✤" "✜" "◆" "▶")
        org-modern-table nil
        ;; org-modern-table-vertical 1
        org-modern-tag t
        org-modern-timestamp t
        ;; org-modern-table-horizontal 0.5
        org-modern-list '((?+ . "➤")
                          (?- . "–")
                          (?* . "•"))
        org-modern-todo-faces
        '(("[-]"  :inverse-video t :inherit +org-todo-active)
          ("STRT" :inverse-video t :inherit +org-todo-active)
          ("NEXT" :inverse-video t :inherit +org-todo-next)
          ("[?]"  :inverse-video t :inherit +org-todo-onhold)
          ("WAIT" :inverse-video t :inherit +org-todo-onhold)
          ("HOLD" :inverse-video t :inherit +org-todo-onhold)
          ("PROJ" :inverse-video t :inherit +org-todo-project)
          ("KILL" :inverse-video t :inherit +org-todo-cancel)
          ("NO" :inverse-video t :inherit +org-todo-cancel))
        org-modern-footnote nil
        ;; org-modern-block-fringe nil
        org-modern-block-name
        '((t . nil)
          ("src" "»" "«")
          ("example" "»–" "–«")
          ("property" . "☸")
          ("quote" "❝" "❞")
          ("export" "⏩" "⏪"))
        ;; org-modern-block-name nil
        org-modern-todo t
        org-modern-keyword nil
        org-modern-checkbox nil
        org-modern-progress '("○" "◔" "◐" "◕" "●")
        org-modern-priority '(
                              (?A . "!")
                              (?B . "⬆")
                              (?C . "○")
                              (?D . "⬇")
                              (?E . "▾")
                              (?F . "⍊"))
        org-modern-horizontal-rule t)
  (custom-set-faces! '(org-modern-statistics :inherit org-checkbox-statistics-todo)))

(use-package! org-appear
  :hook (org-mode . org-appear-mode)
  :config
  (setq org-appear-autoemphasis t
        org-appear-autosubmarkers t
        org-appear-autolinks nil)
  ;; for proper first-time setup, `org-appear--set-elements'
  ;; needs to be run after other hooks have acted.
  (run-at-time nil nil #'org-appear--set-elements))


(after! org-jira
  (require 'jorg-jira)
  (setq org-jira-download-comments nil))

(setq org-roam-directory "/home/owner/next/notes/roam")

(after! org-roam
  (remove-hook 'org-roam-mode-hook #'turn-on-visual-line-mode))

(use-package! websocket :after org-roam)
(use-package! org-roam-ui
  :after org-roam ;; or :after org
  :commands (org-roam-ui-mode)
  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-browser-function #'browse-url-chromium
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))

                                        ;TODO:bcm  make unified setup - take sth from doom
(unless (daemonp) (j/setup-jagenda-h))

;; Prevent "j/k" hijacking evil movement
(after! org-agenda
  (map! :map org-agenda-mode-map
         "j" nil
         "k" nil))


;; Therefore drag-n-drop uses attachments folder:https://orgmode.org/manual/Drag-and-Drop-_0026-yank_002dmedia.html
(after! org

  (setq org-yank-dnd-method 'attach
        org-attach-dir-relative t
        org-attach-id-dir "attachments/"
        )
  )

(setq org-download-image-dir "./images"
      org-download-heading-lvl 'nil)

;; But if we do org-download-yank, we get 
;; copied form doom and https://discordapp.com/channels/406534637242810369/406554085794381833/804772883448070195
(use-package! org-download
  :commands
  org-download-dnd
  org-download-yank
  org-download-screenshot
  org-download-clipboard
  org-download-dnd-base64
  :init
  ;; HACK We add these manually so that org-download is truly lazy-loaded
  (setq dnd-protocol-alist nil)
  (pushnew! dnd-protocol-alist
            '("^\\(?:https?\\|ftp\\|file\\|nfs\\):" . org-download-dnd)
            '("^data:" . org-download-dnd-base64))
  (advice-add #'org-download-enable :override #'ignore)

  :config
  (setq
   org-download-method 'directory
   org-download-timestamp "%Y%m%d-%H%M%S_"
   org-download-link-format "[[file:%s]]\n"
   org-download-screenshot-method "import %s" ;; imagemagick
   org-download-link-format-function
   (lambda (filename)
     (format (concat (unless (image-type-from-file-name filename)
                       (concat (+org-attach-icon-for filename)
                               " "))
                     org-download-link-format)
             (org-link-escape (file-relative-name filename))))))

(after! org-toc
  (set-popup-rules!
    '(("^\\*org-toc*" :side right :size 60 :select nil)))

  (setq org-toc-default-depth 2
        org-toc-follow-mode t)

  (map!
   (:map org-toc-mode-map
    "<tab>" #'org-toc-cycle-subtree
    "RET"  #'org-toc-jump
    "<ret>"  #'org-toc-jump
    "<return>"  #'org-toc-jump
    "j"  #'org-toc-next
    "k"  #'org-toc-previous
    :nvi  "q" #'org-toc-quit
    )))
