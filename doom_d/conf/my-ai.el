;;; conf/my-ai.el -*- lexical-binding: t; -*-

(use-package aider
  :config
  (setq aider-args '("--model" "sonnet")))

(use-package! gptel
  :defer t
  :commands gptel gptel-rewrite
  :config
  (setq gptel-default-mode 'org-mode)
  (require 'jai)
  (require 'my-gptel-tools)
  (j/ai-read-ai-keys-from-authinfo)

  (gptel-make-ollama "Ollama"             ;Any name of your choosing
    :host "localhost:11434"               ;Where it's running
    :stream t                             ;Stream responses
    :models '("llama3.2:latest" "deepseek-r1:1.5b"))

  (gptel-make-anthropic "Claude"          ;Any name you want
    :stream t                             ;Streaming responses
    :key gptel-anthropic-key)

  (gptel-make-gemini "Gemini"          ;Any name you want
    :stream t                             ;Streaming responses
    :key gptel-gemini-key)

  (defun my/gptel--rewrite-difftastic (&optional ovs)
    "Diff pending LLM responses in OVS or at point."
    (interactive (list (gptel--rewrite-overlay-at)))
    (when-let* ((ov-buf (overlay-buffer (or (car-safe ovs) ovs)))
                ((buffer-live-p ov-buf)))
      (require 'difftastic)
      (let* ((newbuf (gptel--rewrite-prepare-buffer ovs))
             (oldbuf (if-let ((buf-file (buffer-file-name ov-buf)))
                         (get-file-buffer (expand-file-name buf-file))
                       ov-buf))
             (diff-buf (difftastic-buffers (buffer-name oldbuf) (buffer-name newbuf) nil)))
        (with-current-buffer diff-buf
          (setq-local diff-jump-to-old-file t)))))


  (map! :map gptel-rewrite-actions-map
        "C-c D" #'my/gptel--rewrite-difftastic))

(map!
 :v "C-<return>" #'gptel-rewrite)


;; FIXME: this makes https://github.com/karthink/gptel/issues/583 less annoying... but it is still terribly annoying.
(setq transient-show-during-minibuffer-read t)


(use-package! copilot
  :hook (prog-mode . copilot-mode)
  :bind   :bind (("C-c M-f" . copilot-complete)
                 :map copilot-completion-map
                 ("C-g" . 'copilot-clear-overlay)
                 ("M-p" . 'copilot-previous-completion)
                 ("M-n" . 'copilot-next-completion)
                 ("<tab>" . 'copilot-accept-completion)
                 ("M-f" . 'copilot-accept-completion-by-word)
                 ("M-<return>" . 'copilot-accept-completion-by-line))
  :config
  ;; From https://github.com/zerolfx/copilot.el/issues/103#issuecomment-1486084717
  (defun j/copilot-clear-overlay-h ()
    "Like `copilot-clear-overlay', but returns `t' if the overlay was visible."
    (when (copilot--overlay-visible)
      (copilot-clear-overlay) t))
  (add-hook 'doom-escape-hook #'j/copilot-clear-overlay-h)


  ;; FIXME : editorconig broke sth
  (setq copilot-indentation-alist (assq-delete-all 'emacs-lisp-mode copilot-indentation-alist))
  (pushnew! copilot-indentation-alist '(emacs-lisp-mode 4))
  )



;; TODO: add repo-local version for dotfiles
(setq copilot-chat-commit-prompt
      "Here is the result of running `git diff --cached`.
Generate a commit message in Conventional Commits specification.
Do not use any markers around the commit message.
Don't add anything else to the response.

The following describes Conventional Commits.

# Conventional Commits 1.0.0

## Summary

The commit message should be structured as follows:

---

```
<type><scope>: <description>

[optional body]

[optional footer(s)]
```
---

<br />
The commit contains the following structural elements, to communicate intent to the
consumers of your library:

1. **fix:** a commit of the _type_ `fix` patches a bug in your codebase (this correlates with [`PATCH`](http://semver.org/#summary) in Semantic Versioning).
1. **feat:** a commit of the _type_ `feat` introduces a new feature to the codebase (this correlates with [`MINOR`](http://semver.org/#summary) in Semantic Versioning).
1. **chore:** a commit of the _type_ `chore` is a task that is not visible to the end user, and should not affect the semantic versioning (example tasks: bumping dependencies).
1. **tweak** a commit of the _type_ `tweak` is a small change that is not visible to the end user, often it is a subtle improvement in codebase or a small refactor.
1. **BREAKING CHANGE:** a commit that has a footer `BREAKING CHANGE:`, or appends a `!` after the type/scope, introduces a breaking API change (correlating with [`MAJOR`](http://semver.org/#summary) in Semantic Versioning).
A BREAKING CHANGE can be part of commits of any _type_.
1. _footers_ other than `BREAKING CHANGE: <description>` may be provided and follow a convention similar to
  [git trailer format](https://git-scm.com/docs/git-interpret-trailers).

Additional types are not mandated by the Conventional Commits specification, and have no implicit effect in Semantic Versioning (unless they include a BREAKING CHANGE).
<br /><br />
A scope may be provided to a commit's type, to provide additional contextual information and is contained within parenthesis, e.g., `feat(parser): add ability to parse arrays`.

## Examples

### Commit message with `!` to draw attention to breaking change
```
feat!(emails): send an email to the customer when a product is shipped
```

### Commit message with scope and `!` to draw attention to breaking change
```
feat(api)!: send an email to the customer when a product is shipped
```

### Commit message with both `!` and BREAKING CHANGE footer
```
chore!(build): drop support for Node 6

BREAKING CHANGE: use JavaScript features not available in Node 6.
```

### Commit message with no body
```
fix(docs): correct spelling of CHANGELOG
```

### Commit message with scope
```
feat(lang): add Polish language
```

### Commit message with multi-paragraph body and multiple footers
```
fix(requests): prevent racing of requests

Introduce a request id and a reference to latest request. Dismiss
incoming responses other than from latest request.

Remove timeouts which were used to mitigate the racing issue but are
obsolete now.

Reviewed-by: Z
Refs: #123
```

---

The following describes type.

# type

Must be one of the following:

* **feat**: A new feature
* **fix**: A bug fix
* **chore:** a task that is not visible to the end user, and should not affect the application behavior (example upgrade dependencies, etc)
* **tweak** a small change that is not visible to the end user, often it is a subtle improvement or a small refactor.
* **perf**: A code change that improves performance

---
Here is the result of `git diff --cached`:
")
