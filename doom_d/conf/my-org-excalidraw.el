;;; conf/my-org-excalidraw.el -*- lexical-binding: t; -*-

(use-package! org-excalidraw
  :defer t
  :after org
  :init

  (setq org-excalidraw-directory "~/next/notes/roam/diagrams")

  ;; Tweak from https://github.com/wdavew/org-excalidraw/issues/2
  (pushnew! org-file-apps '("\\.excalidraw.svg\\'" . "echo '%s' | sed 's/.svg//' | xargs xdg-open"))

  ;; NOTE: installed kroki from https://github.com/wdavew/org-excalidraw/pull/6
  ;; NOTE: sends excalidraw image to remote api

  (advice-add #'org-excalidraw--shell-cmd-to-svg :override (lambda (path)
                                                             (format "~/go/bin/kroki convert \"%1$s\" -o \"%1$s.svg\"" path)))


(advice-add #' org-excalidraw-create-drawing :override (lambda ()
  "Create an excalidraw drawing and insert an 'org-mode' link to it at Point."
  (let* ((filename (format "%s.excalidraw" (org-id-uuid)))
         (path (expand-file-name filename org-excalidraw-directory))
         (link (format "[[file:%s.svg]]" path))) ;; only change
    (org-excalidraw--validate-excalidraw-file path)
    (insert link)
    (with-temp-file path (insert org-excalidraw-base))
    (shell-command (org-excalidraw--shell-cmd-open path system-type)))))

  (org-excalidraw-initialize))

(provide 'my-org-excalidraw)
