;;; +embark-org.el --- Description -*- lexical-binding: t; -*-

;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
(require 'cl)

;; FIXME - remove it when byte compiling doesn't fail
(defun embark-org-target-element-context ()
  "Target all Org elements or objects around point."
  (when (derived-mode-p 'org-mode 'org-agenda-mode)
    (cl-loop
     for elt = (org-element-lineage (org-element-context) embark-org--types t)
     then (org-element-lineage elt embark-org--types)
     while elt
     ;; clip bounds to narrowed portion of buffer
     for begin = (max (org-element-property :begin elt) (point-min))
     for end = (min (org-element-property :end elt) (point-max))
     for target = (buffer-substring begin end)
     ;; Adjust table-cell to exclude final |. (Why is that there?)
     ;; Note: We are not doing this is an embark transformer because we
     ;; want to adjust the bounds too.
     ;; TODO? If more adjustments like this become necessary, add a
     ;; nice mechanism for doing them.
     when (and (eq (car elt) 'table-cell) (string-suffix-p "|" target))
     do (setq target (string-trim (string-remove-suffix "|" target))
              end (1- end))
     collect `(,(intern (format "org-%s" (car elt))) ,target ,begin . ,end))))

(defun org-link-finder ()
  (let ((context (and (derived-mode-p 'org-mode) (org-element-context))))
    (if (and context (equal (org-element-type context) 'link))
    	(let* (
               ;; Org 9.6 supported, maybe 9.7 too?
               (beg (org-element-property :begin context))
               (end (org-element-property :end context))
               ;; Org 9.7:
               ;; (beg (org-element-begin context ))
               ;; (end (org-element-end context ))
               (link-text (buffer-substring-no-properties beg end)))

          (if (not (or (s-starts-with? "http" link-text)
                       (s-starts-with? "[[http" link-text)))
              `(link ,link-text ,beg . ,end))))))

(defun j/org-open-at-point-other-window (x)
  (cl-letf (((symbol-function 'nov--find-file) #'j/nov--find-file-other-window)
            (org-link-frame-setup '((file . find-file-other-window)))
            (large-file-warning-threshold 1000000000000000) ;; FIXME - be more sensible and use doom-large-file-size-alist
            )

    (if (eq 1 (length (window-list)))
        (split-window-horizontally))
    (org-open-at-point)))

(defun j/org-rename-file-and-link (raw-link)
  (string-match "\\[\\[\\(.*?\\)\]\\(\\[.*?\\]\\)?\\]" raw-link)

  (when-let* ((link-path (or (match-string  1 raw-link) raw-link))
              (_ (s-starts-with? "file:" link-path))
              (file-path (s-replace "file:" "" link-path))
              (new-path (read-string "New name? " file-path))
              (new-raw-link (s-replace file-path new-path raw-link)))

    (if (file-exists-p file-path)
        (rename-file file-path new-path))

    (j/org-delete-url-from-point)
    (insert new-raw-link)
    (org-restart-font-lock)))

(defvar-keymap embark-link-map
  :doc "Keymap to work with links"
  "o" #'j/org-open-at-point-other-window
  "r" #'j/org-rename-file-and-link)

;; Stop treating file as url. I won't use it in eww!
(setq thing-at-point-uri-schemes (remove "file:/" thing-at-point-uri-schemes))
(add-to-list 'embark-target-finders 'org-link-finder)
(add-to-list 'embark-keymap-alist '(link . embark-link-map))


(provide '+embark-org)
;;; +embark-org.el ends here
