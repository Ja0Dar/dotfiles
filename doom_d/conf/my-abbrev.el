;;; conf/my-abbrev.el -*- lexical-binding: t; -*-

(set-default 'abbrev-mode t)
(setq save-abbrevs nil)

(define-abbrev-table 'global-abbrev-table
  '(
    ("minn" "m. in")
    ("TOOD" "TODO")
    ))

(when (boundp 'rescript-mode-abbrev-table)
  (clear-abbrev-table rescript-mode-abbrev-table))
(define-abbrev-table 'rescript-mode-abbrev-table
  '(
    ("val" "let")
    ))


(when (boundp 'rustic-mode-abbrev-table)
  (clear-abbrev-table rustic-mode-abbrev-table))
(define-abbrev-table 'rustic-mode-abbrev-table
  '(
    ("val" "let")
    ))
(when (boundp 'scala-mode-abbrev-table)
  (clear-abbrev-table scala-mode-abbrev-table))
(define-abbrev-table 'scala-mode-abbrev-table
  '(
    ("let" "val")
    ("True" "true") ;; Needed after writting some python
    ("False" "false")
    ("entities" "entries")
    ("entitiesPerPage" "entriesPerPage")
    ))

(when (boundp 'protobuf-mode-abbrev-table)
  (clear-abbrev-table protobuf-mode-abbrev-table))
(define-abbrev-table 'protobuf-mode-abbrev-table
  '(
    ("req" "required")
    ("rep" "repeated")
    ("opt" "optional")
    ("entities" "entries")
    ("entitiesPerPage" "entriesPerPage")
    ))

(when (boundp 'python-mode-abbrev-table)
  (clear-abbrev-table python-mode-abbrev-table))
(define-abbrev-table 'python-mode-abbrev-table
  '(
    ("let" "")
    ("val" "")
    ("true" "True")
    ("false" "False")
    ))

(when (boundp 'typescript-ts-mode-abbrev-table)
  (clear-abbrev-table typescript-ts-mode-abbrev-table))
(define-abbrev-table 'typescript-ts-mode-abbrev-table
  '(
    ("val" "const")
    ("True" "true")
    ("False" "false")
    ("in" "of")
    ))
