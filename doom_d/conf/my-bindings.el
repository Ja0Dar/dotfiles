;;; conf/my-bindings.el -*- lexical-binding: t; -*-
(global-set-key (kbd "C-x C-x") 'kill-buffer)
(global-set-key (kbd "C-x C-i") nil)
(global-set-key (kbd "C-;") #'iedit-mode)
(global-set-key (kbd "M-;") #'jscala/comment-dwim)
(global-set-key (kbd "M-j") #'join-line)

(evil-define-key '(normal) global-map
  (kbd "C-S-a") 'evil-numbers/inc-at-pt
  (kbd "C-S-a") 'evil-numbers/inc-at-pt
  (kbd "C-S-x") 'evil-numbers/dec-at-pt
  (kbd "C-S-x") 'evil-numbers/dec-at-pt)

(global-set-key (kbd "S-<f6>") 'jthemes-cycle)

(map!
 (:prefix "gz"
  :nv "s" #'evil-mc-skip-and-goto-next-match
  :nv "S" #'evil-mc-skip-and-goto-prev-match)

 :n "!"  #'rotate-text-paren-or-text

 "M-S-<up>" #'drag-stuff-up
 "M-S-<down>" #'drag-stuff-down
 :nvi "M-w" #'er/expand-region

 (:prefix "C-x"
          (:map org-mode-map
           :prefix "n"
           :nvi "s" #'org-narrow-to-subtree))

 (:map org-mode-map
  :prefix "C-c"
  :nvi "s" #'j/org-sort-custom)

 (:map evil-treemacs-state-map
       "s" #'treemacs-toggle-show-dotfiles)

 (:map lsp-ui-flycheck-list-mode-map
  "q" #'lsp-ui-flycheck-list--quit
  :nvi "q" #'lsp-ui-flycheck-list--quit)


 (:leader
  "S-<return>" #'j/run-dired-hydra

  (:prefix "b"
   :desc "Switch buffer (Spacemacs)" :n "b" #'switch-to-buffer)

  (:prefix-map ("g" . "git")
   :desc "Hunking"            "h"   #'diff-hl-show-hunk)

  (:prefix "l"
   :desc "One line to multiline" :n "m" #'+jemacs/single-to-muliline)


  (:prefix "o"
   :desc "Calendar" :nvi "c" #'=calendar)

  (:prefix "p"
           (:when (modulep! :completion ivy)
             :desc "Find in git" :n "G" #'counsel-git-grep)
           (:when (modulep! :completion vertico)
             :desc "Search in git root" :n "g" #'j/search-git-root
             :desc "Find file in git root" :n "f" #'j/search-find-file-git-root))

  (:prefix "s"
   :desc "Evil symbol" "w" #'jevil-symbol-mode)

  (:prefix "t"
   :desc "Transpose words" :nv "w" #'transpose-words
   :desc "Trim in sexp" :nv "i" #'j/emacs-trim-in-sexp
   :desc "Toggle company-box"  "p" #'company-box-mode
   :desc "Spell"  "s" #'jspell-mode
   )

  (:prefix "w"
   :desc "Window hydra" :nv "N" #'+hydra/window-nav/body
   :desc "Force maximize window" :nv "M" #'delete-other-windows-internal
   )

  (:map projectile-mode-map
   :prefix "TAB"
   :desc "Set workspace name as current project" :nvi "c" #'j/workspaces-rename-to-current-project
   :desc "Kill other workspaces" :nvi "o" #'j/workspaces-kill-other-workspaces)
  )

 (:map evil-window-map
       ;; Navigation
       "<left>"          #'evil-window-left
       "<down>"          #'evil-window-down
       "<up>"            #'evil-window-up
       "<right>"         #'evil-window-right
       "S-<left>"        #'+evil/window-move-left
       "S-<down>"        #'+evil/window-move-down
       "S-<up>"          #'+evil/window-move-up
       "S-<right>"       #'+evil/window-move-right)

 (:nvi
  "C-S-<left>" #'evil-window-left
  "C-S-<up>" #'evil-window-up
  "C-S-<right>" #'evil-window-right
  "C-S-<down>" #'evil-window-down
  :map org-mode-map
  "C-S-<left>" nil
  "C-S-<up>" nil
  "C-S-<right>" nil
  "C-S-<down>" nil)



 (:localleader


  (:after protobuf-mode
          (:prefix-map  ("t" . "translate")
           :map protobuf-mode-map
           :desc "Scalaproto cc-region-to-proto" "c" #'scalaproto-cc-in-region-to-proto
           )

          (:prefix-map  ("s" . "search")
           :map protobuf-mode-map
           :desc "Search Entity" "c" #'+proto/search-entity
           )
          )
  (:after lsp-mode
          (:prefix-map  ("=" . "format")
           :map lsp-mode-map
           :desc "Format buffer(lsp)" "b" #'lsp-format-buffer
           :desc "Format region (lsp)" "r" #'lsp-format-region
           )
          (:prefix-map  ("g" . "goto")
           :map lsp-mode-map
           :desc "Go to definition" "d" #'lsp-find-definition
           :desc "Go to declaration" "D" #'lsp-find-declaration
           :desc "Click lens" "l" #'lsp-avy-lens
           :desc "Go to references" "r" #'lsp-find-references
           :desc "Go to implementation" "i" #'lsp-goto-implementation
           )
          (:map lsp-mode-map
           :desc "Lsp Imenu" "C-," #'lsp-ui-imenu
           :desc "Errors" "e" #'lsp-treemacs-errors-list
           :desc "Errors+choose level" "E" #'j/lsp-select-log-level
           :desc "Exec Code Action" "a" #'lsp-execute-code-action
           )
          )

  (:after org
   :map org-mode-map
   :desc "Toggle link display"  :nv "C-u" #'org-toggle-link-display
   :desc "Toggle latex display" :nv "C-l" #'org-toggle-latex-fragment
   :desc "Toggle image display" :nv "C-i" #'org-toggle-inline-images
   :desc "org-toc-show" "C-," #'org-toc-show
   )
  )
 )

(use-package! difftastic-bindings
  :config
  (difftastic-bindings-mode)
  (map!
   :map difftastic-mode-map
   :nvi "q" #'difftastic-quit)
  )
