;;; conf/my-lsp.el -*- lexical-binding: t; -*-
(defun +lsp--update-projectile-after-rename()
  "Save projectile buffers and invalidate cache on lsp rename ARG is ignored."
  (projectile-save-project-buffers)
  (projectile-invalidate-cache nil))

(defun +lsp--update-projectile-after-rename-delayed-a(arg)
  (run-at-time "1 sec" nil #'+lsp--update-projectile-after-rename))

(advice-add 'lsp-rename :after #'+lsp--update-projectile-after-rename-delayed-a)


(after! format
  (defadvice! jformat-lsp-or-aphelia (formatter &optional success-callback &key callback)
    :override #'+format/buffer
    (if (bound-and-true-p lsp-mode)
        (lsp-format-buffer)
      (apheleia-format-buffer formatter success-callback :callback callback))
    ))

(setq lsp-ui-doc-enable nil)
