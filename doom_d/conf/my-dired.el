;;; conf/my-dired.el -*- lexical-binding: t; -*-
(after! dired
  (setq dired-du-bind-human-toggle nil
        dired-du-size-format t
        dired-du-bind-mode t
        dired-du-bind-count-sizes nil)
  (setq dired-omit-files
        (concat "\\`[.]?#\\|\\`[.][.]?\\'"
                "\\|^flycheck_.*"
                "\\|^[.].*$")) ;; ignore all dotfiles
  (set-popup-rule!  "^\\*image-dired\\*"   :ignore t)
  (map! :map dired-mode-map :ng "q" #'quit-window)

  (require 'jdired)

  (setq image-dired-external-viewer "/home/owner/.nix-profile/bin/gthumb")

  (map! :map dirvish-mode-map
        ;; "s" #'dired-omit-mode
        :ng "C-c d" #'j/dired-dragon))

(defun j/run-dired-hydra()
  (interactive)
  (require 'hydra)
  (require 'jhydras)
  (setq j/hydra/dired-goto/other nil)
  (call-interactively 'j/hydra/dired-goto/body))
