;;; conf/my-org-gtd.el -*- lexical-binding: t; -*-


(setq org-gtd-directory "~/next/notes/gtd"
      org-edna-use-inheritance t
      org-gtd-update-ack "3.0.0"
      org-gtd-canceled "KILL"
      )

(use-package org-gtd
  :after org
  :init


  (require 'org-gtd-archive)
  (require 'org-gtd-organize)
  (require 'org-gtd-clarify)
  (require 'org-gtd-process)
  (require 'org-gtd-calendar)
  (require 'org-gtd-incubate)

  ;; TODO Sync GTD timestamp from SCHEDULED
  ;; Add hook that updates gtd timestamp after scheduled is updated

  (defun j/org-gtd--move-timestamp-to-scheduled(&optional entry)
    (setq entry (org-element-at-point entry))
    (if-let ((timestamp (or (and (boundp 'org-gtd-timestamp) (org-entry-get entry org-gtd-timestamp))
                            ;; (org-entry-get entry "TIMESTAMP")
                            )))
        (org-entry-put entry "SCHEDULED" timestamp )))

  (defun j/org-gtd--move-timestamp-to-scheduled-in-buffer()
    (org-ql-select (list (current-buffer))
      '(and (not (scheduled)) (not (done)))
      :action '(j/org-gtd--move-timestamp-to-scheduled nil)))

  ;; https://github.com/alphapapa/org-ql/issues/295
  (defun j/org-archive-done-in-buffer (&optional level)
    "LEVEL: archive only done tasks with this level, defaults to 1."
    (interactive)
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward
              (concat "* " (regexp-opt org-done-keywords) " ") nil t)
        (when (= (org-outline-level) (or level 1))
          (org-archive-subtree)))))

  (defun j/org-gtd-prep-schedule-item(reminder-date)
    "Prepare entry for item scheduled by org-gtd"
    (let ((date (or reminder-date
                    (org-read-date t nil nil "When would you like this item to come up again? "))))
      (org-entry-put (point) org-gtd-timestamp (format "[%s]" date))
      (org-schedule nil date)))

  (defadvice! j/org-gtd-delegate-item-at-point (&optional delegated-to checkin-date)
    "Delegate the item at point to DELEGATED-TO and schedule a checkin for CHECKIN-DATE."
    :override #'org-gtd-delegate-item-at-point
    (let ((delegated-to (or delegated-to
                            (apply org-gtd-delegate-read-func nil)))
          (date (or checkin-date
                    (org-read-date t nil nil "When do you want to check in on this task? ")))
          (org-inhibit-logging 'note))
      (org-set-property org-gtd-delegate-property delegated-to)
      (org-schedule nil date)
      ;; (org-entry-put (point) org-gtd-timestamp (format "<%s>" date))
      ;; (save-excursion
      ;;   (org-end-of-meta-data t)
      ;;   (open-line 1)
      ;;   (insert (format "<%s>" date)))
      (org-todo org-gtd-wait)
      (save-excursion
        (goto-char (org-log-beginning t))
        (insert (format "programmatically delegated to %s\n" delegated-to)))))

  (defun j/org-gtd-show-done-tasks-by-date (date)
  "Show all tasks marked as DONE on specific DATE.
DATE should be in format YYYY-MM-DD"
  (interactive (list (org-read-date nil nil nil "Enter date: ")))
  (let ((files (list (expand-file-name "org-gtd-tasks.org" org-gtd-directory)
                     (expand-file-name "org-gtd-tasks.org_archive" org-gtd-directory)))
        (search-term (format "CLOSED:\s+\\[%s" date))
        (tag (when current-prefix-arg
               (completing-read "Select tag: " '("@work" "@me" "@home" "@config") nil t))))
    (org-ql-search files
      `(and (done)
            (regexp ,search-term)
            ,@(when tag `((tags ,tag))))
      :title (format "Tasks completed on %s%s" date (if tag (format " with tag %s" tag) ""))
      :super-groups '((:auto-ts t)))))




  (setq org-gtd-capture-templates   nil ;; Let's use default org capture
        ;; TODO: could be easier?
        org-gtd-calendar-func (lambda (&optional appointment-date)
                                (j/org-gtd-prep-schedule-item appointment-date)
                                (setq-local org-gtd--organize-type 'calendar)
                                (org-gtd-organize-apply-hooks)
                                (with-org-gtd-refile "Calendar"
                                  (org-refile nil nil '("Calendar" "~/next/notes/gtd/remote-calendar.org" nil nil)))
                                (org-save-all-org-buffers)
                                )

        org-gtd-incubate-func (lambda (&optional reminder-date)
                                (j/org-gtd-prep-schedule-item reminder-date)
                                (setq-local org-gtd--organize-type 'incubated)
                                (org-gtd-organize-apply-hooks)
                                (with-org-gtd-refile "Tickler"
                                  (org-refile nil nil '("tickler.org" "~/next/notes/gtd/tickler.org" nil  nil)))
                                (org-save-all-org-buffers))


        org-gtd-single-action-func (lambda ()
                                     (org-todo org-gtd-next)
                                     (setq-local org-gtd--organize-type 'single-action)
                                     (org-gtd-organize-apply-hooks)
                                     ;; TODO -> make it Y/n
                                     (if (y-or-n-p "Schedule?")
                                         (org-schedule nil))
                                     (when (y-or-n-p "Set effort?")
                                       (org-set-effort))
                                     (org-gtd-refile--do org-gtd-action org-gtd-action-template)
                                     ))

  (add-hook! org-gtd-clarify-mode #'org-save-all-org-buffers)
  ;; (add-hook org-gtd-organize-hooks '(org-gtd-set-area-of-focus org-set-tags-command))
  :config
  (org-edna-mode)
  :bind
  (("C-c d c" . org-capture)
   ("C-c d e" . org-gtd-engage)
   ("C-c d p" . org-gtd-process-inbox)
   :map org-gtd-clarify-map
   ("C-c c" . org-gtd-organize)))

(after! org-gtd-id

  ;; Replace polish signs in org-gtd-id--generate-sanitized-alnum-dash-string
  (defadvice! j/org-gtd-id--generate-sanitized-alnum-dash-string (str)
    :filter-return #'org-gtd-id--generate-sanitized-alnum-dash-string
    "Generate a sanitized version of STRING that is alnum and dash only."
    (let* ((str (replace-regexp-in-string "ą" "a" str))
           (str (replace-regexp-in-string "ć" "c" str))
           (str (replace-regexp-in-string "ę" "e" str))
           (str (replace-regexp-in-string "ł" "l" str))
           (str (replace-regexp-in-string "ń" "n" str))
           (str (replace-regexp-in-string "ó" "o" str))
           (str (replace-regexp-in-string "ś" "s" str))
           (str (replace-regexp-in-string "ź" "z" str))
           (str (replace-regexp-in-string "ż" "z" str)))
      str)))

(after! org-gtd-process
  (defadvice!  j/org-gtd-process-inbox ()
    "Process inbox under top-level Inbox tag"
    :override #'org-gtd-process-inbox
    (let ((buffer (find-file-noselect (org-gtd-inbox-path))))
      (set-buffer buffer)
      (goto-char (point-min))
      (when (org-before-first-heading-p)
        (org-next-visible-heading 1)
        (org-N-empty-lines-before-current 1))

      (when (s-equals? (org-get-title) "Inbox")
        (org-N-empty-lines-before-current 1)
        (org-next-visible-heading 1))


      (if (org-at-heading-p)
          (org-gtd-clarify-inbox-item)
        (message "Inbox is empty. No items to process.")
        (org-gtd-process--stop)))
    ))
