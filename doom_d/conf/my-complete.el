;;; conf/my-complete.el -*- lexical-binding: t; -*-

(after! company
  (map!
   (:after company
           (:map company-active-map
                 "C-f"     #'company-filter-candidates)))
  (setq company-show-quick-access nil ;; i dont use them
        company-idle-delay 0.1
        company-box-enable-icon nil
        company-box-scrollbar nil
        company-box-doc-enable nil))

(after! corfu
  (setq corfu-preselect 'first)
  (map!
   :i "C-x C-f" #'cape-file))


(after! consult ;; vertico
  (setq consult-preview-key 'any)
  (require 'jsearch)

                                        ;TODO:bcm
  ;; (consult-customize
  ;;  j/search-git-root
  ;;  :preview-key (list (kbd "C-SPC") (kbd "C-M-j") (kbd "C-M-k")))
  )

(after! marginalia
  (defun j/marginalia-file-project-name(filename)
    (if-let ((_ filename)
             (proj-root  (doom-project-root (file-name-directory filename))))
        (when proj-root (funcall projectile-project-name-function proj-root))))

  (defadvice! j/marginalia-annotate-bookmark (cand)
    :override 'marginalia-annotate-bookmark
    (when-let ((bm (assoc cand bookmark-alist)))
      (marginalia--fields
       ((marginalia--bookmark-type bm) :width 10 :face 'marginalia-type)

       ((j/marginalia-file-project-name (bookmark-get-filename bm))
        :truncate (- (/ marginalia-field-width 2)) :face 'marginalia-symbol)
       ((bookmark-get-filename bm)
        :truncate (- (/ marginalia-field-width 2)) :face 'marginalia-file-name)
       ((when-let ((front (bookmark-get-front-context-string bm)))
          (-->
           front
           (replace-regexp-in-string "^\n+" ""  it)
           (replace-regexp-in-string "\n+" "⮰"  it)
           (replace-regexp-in-string "[ \t]+" " " it)
           (replace-regexp-in-string "^\*+" "\*" it) ;; remove leading "*" - it is usually org
           (string-trim it)
           (concat it "…")))
        :truncate (/ marginalia-field-width 3) :face 'marginalia-documentation)
       )))

  (defun j/marginalia--buffer-status (buffer)
    "Return the status of BUFFER as a string."
    (format-mode-line '(;;(:propertize "%1*%1+%1@" face marginalia-modified)
                        ;; marginalia--separator
                        (7 (:propertize "%I" face marginalia-size))
                        marginalia--separator
                        ;; InactiveMinibuffer has 18 letters, but there are longer names.
                        ;; For example Org-Agenda produces very long mode names.
                        ;; Therefore we have to truncate.
                        (20 (-20 (:propertize mode-name face marginalia-mode))))
                      nil nil buffer))

  (defadvice! j/marginalia-annotate-buffer (cand)
    :override 'marginalia-annotate-buffer
    (when-let (buffer (get-buffer cand))
      (marginalia--fields
       ((j/marginalia--buffer-status buffer))
       ((unless (s-starts-with? "*" (buffer-name buffer))
          (j/marginalia-file-project-name (marginalia--buffer-file buffer)) )
        :truncate (- (/ marginalia-field-width 2))
        :width 30
        :face 'marginalia-symbol)
       ((marginalia--buffer-file buffer)
        :truncate (- (ceiling (/ marginalia-field-width 3)))
        :face 'marginalia-file-name)
       )))
  )
