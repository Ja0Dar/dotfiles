;;; conf/my-scala.el -*- lexical-binding: t; -*-
(after! scala-mode

  ;; FIXME: upstream or properly load
  (defun lsp-graphql-activate-p (filename &optional _)
    "Check if the GraphQL language server should be enabled based on FILENAME."
    (let ((target-extensions (mapconcat (lambda (ext) (format "\\.%s\\'" ext)) lsp-graphql-target-file-extensions "\\|")))
      (or (string-match-p target-extensions filename)
          (and (derived-mode-p 'js-mode 'js2-mode 'typescript-mode 'typescript-ts-mode)
               (not (derived-mode-p 'json-mode))))))

  (map!
   (:map scala-mode-map
    :localleader
    (:prefix-map  ("i" . "insert implementation")
     :desc "Extends for case class enum" "e" #'jscala-st-extend-cases
     :desc "Add widen to sealed trait" "w" #'jscala-widen)
    (:prefix-map  ("p" . "proto") ;;Or transpose ;;or type
     :desc "Scalaproto proto-to-cc" "c" #'scalaproto-proto-in-region-to-cc)
    (:prefix-map  ("s" . "search")
     :desc "Scala ???" "?" #'+scala/search-???
     :desc "Scala class" "c" #'+scala/search-entity)

    (:prefix-map  ("t" . "translate") ;;Or transpose ;;or type or trim
     :desc "Transpose case statements" "c" #'jscala-swap-case
     :desc "Add type to def" "d" #'jscala-add-def-type
     :desc "Trim typesin arglist" "a" #'jscala/trim-types-in-parens)
    (:prefix-map ("d" . "delete")
     :desc "Del fun implementation" "i" #'jscala-delete-implementation)
    (:prefix-map  ("g" . "goto")
     :desc "Go to implementation" "I" #'jscala-goto-implementation-rg)))

  ;; Treat ??? as a word -
  ;; dont kill ","  when "evil-delte-word" performed on "???,"
  ;; also: jump to beginning on "???" on evil-forward-word
  ;; needs probably jevil-symbol-mode to work
  (modify-syntax-entry ?? "w" scala-syntax:syntax-table)

  (require 'scalaproto)
  (require 'jscala)
  (require 'jmaven)

  (setq lsp-metals-super-method-lenses-enabled t
        lsp-metals-implicit-argument-annotations-enabled nil
        lsp-lens-auto-enable t))


(use-package! ob-scala-cli
  :defer t
  :init
  (add-hook! '+org-babel-load-functions
    (defun +org-babel-load-scala-cli-h (lang)
      (when (string-prefix-p "scala" (symbol-name lang))
        (require 'scala-cli-repl)
        (require 'ob-scala-cli)))))

(set-file-template! "\\.scala$" :trigger "__.scala")

(defun j/scala-mode-init-h()
  "Add rotate text config for scala-mode."
  (require 'rotate-text)
  (setq-local file-name-handler-alist nil)
  (pushnew! rotate-text-local-symbols
            '("Nil" "None")
            '("map" "flatMap")
            '("class" "object" "trait")
            '("def" "val")
            '("&&" "||" )
            '("=" "<-" )
            '("Future" "F" "IO")
            '("List" "Seq" "Option")
            '("with" "extends")
            '("in" "ignore") ;;scalatest
            '("package" "import")
            '("zonedDateTimeToMillis" "millisToZonedDateTime")
            ))

(add-hook! 'scala-mode-hook
           #'j/scala-disable-scala-flycheck-if-script-h
           #'j/scala-mode-init-h
           #'jevil-symbol-mode)
