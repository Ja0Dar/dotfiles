;;; +ox-slack.el --- Description -*- lexical-binding: t; -*-
;;
;; Author: Ja0Dar
;; Maintainer: Ja0Dar
;; Created: January 13, 2022
;; Modified: January 13, 2022
;; Version: 0.0.1
;; Homepage: https://bitbucket.com/Ja0Dar/dotfiles
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(defun +org-slack-underline(_underline contents _info)
  (s-join "" (mapcar (lambda (a)  (concat (list ?̲  a))) contents)))

(defun +org-slack-link (link contents info)
  "Transcode LINK object into Markdown format.
CONTENTS is the link's description.  INFO is a plist used as
a communication channel."
  (let* ((type (org-element-property :type link))
         (raw-path (org-element-property :path link))
         (path
          (cond
           ((member type '("http" "https" "ftp" "mailto"))
            (concat type ":" raw-path))
           ((string= type "file")
            (org-export-file-uri raw-path))
           (t raw-path))))
    (if (not contents) (format "%s" path)
      (format "[%s](%s)" contents path))))

(org-export-define-derived-backend 'slack 'gfm
  :menu-entry
  '(?s "Export to Slack syntax"
       ((?s "To temporary buffer"
            (lambda (a s v b) (org-slack-export-as-slack a s v)))
        (?S "To file" (lambda (a s v b) (org-slack-export-to-slack a s v)))
        (?o "To file and open"
            (lambda (a s v b)
              (if a (org-slack-export-to-slack t s v)
                (org-open-file (org-slack-export-to-slack nil s v)))))))
  :translate-alist
  '(
    (bold . org-slack-bold)
    (code . org-slack-code)
    (underline . +org-slack-underline)
    (headline . org-slack-headline)
    (inner-template . org-slack-inner-template)
    (italic . org-slack-italic)
    (link . +org-slack-link)
    (plain-text . org-slack-plain-text)
    (src-block . org-slack-src-block)
    (strike-through . org-slack-strike-through)
    (timestamp . org-slack-timestamp)))

(defadvice! +org-slack-export-newlines (fn &rest args)  "Preserve newlines when exporting with slack" :around  #'org-slack-export-to-clipboard-as-slack
  (let ((org-export-preserve-breaks t))
    (apply fn args)))

(provide '+ox-slack)
;;; +ox-slack.el ends here
