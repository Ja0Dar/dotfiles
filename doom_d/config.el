;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;; Code:


(pushnew! load-path "~/git/dotfiles/doom_d/lib")
(pushnew! load-path "~/git/dotfiles/doom_d/conf")
(load! "lib/jemacs.el")
(j/load-if-decrypted "~/git/dotfiles/secrets/jsecrets.el")

(load! "conf/my-base.el")

(defadvice! pdf-revert-after-annot (type edges &optional property-alist page)
  :after 'pdf-annot-add-annotation
  (save-buffer)
  (revert-buffer))

;; Save all buffers when switching windows (C-w C-w and all others)

(when (file-exists-p "~/.private/private.el")
  (load "~/.private/private.el"))

(load! "conf/my-bindings.el")
(load! "conf/my-abbrev.el")

(load! "conf/my-ai.el")
(load! "conf/my-complete.el")


(after! calfw
  (require 'cl))

(setq cfw:display-calendar-holidays nil
      ;; calendar-date-style 'european
      cfw:org-capture-template '("c" "Schedule an event" entry
                                 (file "~/next/notes/gtd/remote-calendar.org")

                                 "* %^{Description}\nSCHEDULED: %(cfw:org-capture-day)\n%?")
      calendar-week-start-day 1)


(after! beancount
  (require 'jbeancount))


(setq markdown-fontify-code-blocks-natively t)


(after! evil-ex
  ;;For typos
  (evil-ex-define-cmd "W"        #'save-buffer)
  (evil-ex-define-cmd "Wq"       #'evil-save-and-quit)
  (evil-ex-define-cmd "WQ"       #'evil-save-and-quit)
  (evil-ex-define-cmd "wQ"       #'evil-save-and-quit)
  (evil-ex-define-cmd "Q"        #'evil-quit)
  (evil-ex-define-cmd "Vs"       #'evil-window-vsplit)
  (evil-ex-define-cmd "VS"       #'evil-window-vsplit)
  (evil-ex-define-cmd "vS"       #'evil-window-vsplit)
  (evil-ex-define-cmd "Sp"       #'evil-window-split)
  (evil-ex-define-cmd "SP"       #'evil-window-split)
  (evil-ex-define-cmd "sP"       #'evil-window-split))

(after! elfeed
  (require 'elfeed-goodies)
  (elfeed-goodies/setup)
  (require 'jelfeed)
  (elfeed-org)
  (setq rmh-elfeed-org-files '("~/next/notes/feed.org")
        elfeed-search-date-format  '("%d %b %Y" 11 :left)
        elfeed-search-filter "@3-week-ago +unread ")

  (add-hook! 'elfeed-search-mode-hook 'elfeed-update))

;; NOTE: useful for emacs-everywhere. Allows converting org to github markdown.

(set-popup-rule!
  "^\\*doom:\\(?:v?term\\|e?shell\\)-popup"  ; editing buffers (interaction required)
  :ignore t)

(set-popup-rule!  "^\\*PLANTUML Preview\\*"   :ignore t)

(after! c-mode
  (setq c-mode-hook nil))


(setq
 plantuml-jar-path "/home/owner/.nix-profile/lib/plantuml.jar"
 org-plantuml-jar-path  "/home/owner/.nix-profile/lib/plantuml.jar")


(load! "conf/my-dired.el")


(after! eshell
  (defun eshell/gst (&rest args)
    (magit-status (pop args) nil)
    (eshell/echo))   ;; The echo command suppresses output

  (defun eshell/ga (&rest args)
    (eshell/gst args))

  (defun eshell/ggp (&rest args)
    (if (doom-project-root)
        (eshell/cd (projectile-project-root))
      (eshell/echo "Not in project")))


  (defun eshell/ggh (&rest args)
    (if (vc-git-root  ".")
        (eshell/cd (vc-git-root  "."))
      (eshell/echo "Not in git")))


  (defalias 'open 'find-file-other-window)
  (defalias 'l 'eshell/clear-scrollback)
  (defalias 'll #'(lambda() (eshell/ls "-la"))))

(after! em-term
  (pushnew! eshell-visual-commands "boiler" "fzf" "sk" "pboy" "glclone" "bbclone" "gclone" "dlog" "dexec"))

(setq eww-search-prefix "https://duckduckgo.com/lite?q=")

(setq fish-completion-fallback-on-bash-p nil) ;;Eshell hangs otherwise


(use-package! hl-todo
  :hook (prog-mode . hl-todo-mode)
  :hook (yaml-mode . hl-todo-mode)
  :hook (conf-mode . hl-todo-mode)
  )


(remove-hook 'text-mode-hook #'spell-fu-mode)
(after! spell-fu
  (require 'jspell)
  (when (boundp 'company-mode)
    (require 'company)
    (require 'company-ispell)
    (require 'company-tide)
    (unless company-ispell-available (my-generic-ispell-company-complete-setup))))

(setq flycheck-idle-change-delay 0.2)

(after! evil-org
  (remove-hook 'org-tab-first-hook #'+org-cycle-only-current-subtree-h)

  (defmacro define-and-bind-quoted-text-object (name key start-regex end-regex)
    (let ((inner-name (make-symbol (concat "evil-inner-" name)))
          (outer-name (make-symbol (concat "evil-a-" name))))
      `(progn
         (evil-define-text-object ,inner-name (count &optional beg end type)
           (evil-select-paren ,start-regex ,end-regex beg end type count nil))
         (evil-define-text-object ,outer-name (count &optional beg end type)
           (evil-select-paren ,start-regex ,end-regex beg end type count t))
         (define-key evil-inner-text-objects-map ,key #',inner-name)
         (define-key evil-outer-text-objects-map ,key #',outer-name))))

  ;; TODO - make it only active in org-mode
  (define-and-bind-quoted-text-object "tilde" "~" "~" "~")
  (define-and-bind-quoted-text-object "equals" "=" "=" "="))


(load! "conf/my-org.el")
(load! "conf/my-org-gtd.el")

(after! mu4e
  (require 'mu4e-main)
  (require 'mu4e-obsolete)
  (defun mu4e-clear-caches())
  ;; (defalias 'mu4e--view-gather-mime-parts 'mu4e-view-mime-parts "deprecated")

  ;; NOTE: workaround for read-string output (internal repr of string) being passed from
  ;; mu4e-search-read-query to mu4e~proc-search (?)
  ;; https://github.com/djcb/mu/blame/fdce40af926884c31d6a8a821808061153a53750/mu4e/mu4e-search.el#L341C31-L341C31
  (setq mu4e-query-rewrite-function 'substring-no-properties)

  (setq mu4e-view-show-images nil)
  (add-hook! mu4e-index-updated
             #'mu4e-alert-update-mail-count-modeline))




(use-package! ligature
  :config
  ;; Enable all `+ligatures-prog-mode-list' ligatures in programming modes
  (setq +ligatures-prog-mode-list
        '( "***"
           ":::"   "==="
           ">>>"  "->>" "-->" "---"
           "<*>"  "<==" "<=>"
           "<--"  "<<<" "<+>"
           "..." "+++"  "&&"
           "~>"   "*>"  "||" "|>"
           "::" ":="  "==" "=>" "!="
           ">=" ">>"  "->"  "<~" "<*" "<|"
           "<=" "<>" "<-" "<<" "<+" "</"  "#!"
           "%%" "+>" "++" "?:"
           "?="  ";;" "/*"  "/>" "//"   "(*" "*)"
           "\\\\" "://"))
  (ligature-set-ligatures 'prog-mode +ligatures-prog-mode-list)



  (setq +ligatures-extra-symbols
        '(
          ;; Org
          :name          "»"
          :title         "Ⲧ:"
          ;; Functional
          :lambda "λ")
        +ligatures-extras-in-modes '(python-mode org-mode)
        )
  (set-ligatures! 'org-mode
    :name "#+NAME:"
    :name "#+name:"
    :title "#+title:"
    :title "#+TITLE:")

  )


(after! magit
  (require 'jmagit)

  (map!
   :map magit-status-mode-map
   :prefix "y"
   :nvi "p" #'j/magit-forge-copy-topic-url-at-point
   :nvi "P" #'jmagit-format-and-copy-pr))

(use-package nginx-mode
  :mode (("/nginx/sites-\\(?:available\\|enabled\\)/" . nginx-mode)
         ("nginx.conf" . nginx-mode)))

(after! python

  (defun +python-ligatures-setup-h()
    "Clean doom's defaults."
    (setq +ligatures-extra-alist (assq-delete-all 'python-mode +ligatures-extra-alist))
    (setq python-prettify-symbols-alist '(("lambda" . ?λ)))
    (setq-local prettify-symbols-alist '(("lambda" . ?λ))))
  (add-hook 'python-mode-hook #'+python-ligatures-setup-h)
  (add-hook 'python-ts-mode-hook #'+python-ligatures-setup-h)
  (setq python-shell-interpreter "python"))

(after! mixed-pitch
  (delq! 'org-ref-cite-face mixed-pitch-fixed-pitch-faces))

(add-hook! rainbow-mode
  (hl-line-mode (if rainbow-mode -1 +1)))

(after! diff-hl
  (defadvice! j/diff-hl-after-revert-show-hunk-a ()
    :after #'diff-hl-show-hunk-revert-hunk
    (diff-hl-show-hunk-next))
  )

(load! "conf/my-scala.el")

(after! conf-mode
  (when (and (fboundp 'treesit-available-p) (treesit-available-p))
    (require 'j-tree-sitter))
  (map!
   (:map conf-mode-map
    :localleader
    :desc "Make hocon env" "e" #'jhocon-add-env-for-node-at-point)))

(use-package! typescript-mode
  :config
  (when (and (fboundp 'treesit-available-p) (treesit-available-p))
    (require 'j-tree-sitter)
    (when (treesit-language-available-p 'typescript)

      (add-hook! 'typescript-mode-hook
                 #'typescript-ts-mode))))

(pushnew! auto-mode-alist
          '("\\.sc\\'" . scala-mode)
          '("\\.puml\\'" . plantuml-mode)
          '("\\.rasi\\'" . css-mode)
          '("\\.graphqlconfig\\'" . json-mode)
          '("\\.epub\\'" . nov-mode)
          '("\\.rsc\\'" . tcl-mode))


;; From https://github.com/nix-community/nixd/blob/main/nixd/docs/configuration.md
(use-package! nix-mode
  :after lsp-mode
  :hook   (nix-mode . lsp-deferred) ;; So that envrc mode will work
  :custom (lsp-disabled-clients '((nix-mode . nix-nil))) ;; Disable nil so that nixd will be used as lsp-server
  :config
  (setq lsp-nix-nixd-server-path "nixd"
        lsp-nix-nixd-formatting-command [ "nixpkgs-fmt" ]
        lsp-nix-nixd-nixpkgs-expr "import <nixpkgs> { }"
        lsp-nix-nixd-nixos-options-expr "(builtins.getFlake \"/home/owner/git/dotfiles/sym/common/dot_config/nixpkgs\").nixosConfigurations.t14.options"
        lsp-nix-nixd-home-manager-options-expr "(builtins.getFlake \"/home/owner/git/dotfiles/sym/common/dot_config/nixpkgs\").homeConfigurations.\"owner@t14\".options"))

;; epub support
(use-package! nov
  :config
  (require 'jnov)
  (setq nov-text-width 80)
  (add-hook 'nov-mode-hook #'jnov-mode-setup)
  )



(after! evil
  (setq evil-ex-substitute-global nil ;; Make s/A/B/g be global instad of s/A/B/
        +evil-want-o/O-to-continue-comments nil))

(after! treemacs
  (treemacs-follow-mode 1)
  (setq treemacs-collapse-dirs 5))

(after! apheleia
  (set-formatter! 'nixpkgs-fmt '("nixpkgs-fmt") :modes '(nix-mode))
  (set-formatter! 'pg-format '("pg_format") :modes '(sql-mode))
  (set-formatter! 'shfmt '("shfmt" "-ci" "-i" "4") :modes '(sh-mode)))

(after! sh-script
  (set-formatter! 'shfmt '("shfmt" "-ci" "-i" "4") :modes '(sh-mode)))


(after! projectile
  (defun j/projectile-discover-my-projects-recursive(&rest roots)
    ""
    (interactive)
    (require 'dash)
    (require 's)
    (let* ((real-roots (if roots roots '("~/git" "~/tmp/work")))
           (projects (-flatten-n 1 (mapcar (lambda (root)
                                             (s-split "\n" (cdr (doom-call-process "project_root" "-f" (s-concat "--source=" root)))))
                                           real-roots))))
      (-map #'projectile-add-known-project projects)
      ;; to prevent attempt of freezing emacs by looong output:
      nil))


  (pushnew! projectile-project-root-files-bottom-up "build.sbt" "gradlew" "package.json")
  (pushnew! projectile-globally-ignored-files ".metals" ".bloop" "target" "__pycache__" ".ipynb_checkpoints" "META-INF" "target"))


;; Langtool
;; TODO - disable whitespace only in org tables somehow?
(setq langtool-disabled-rules '("PLUS_MINUS" "EN_QUOTES" "DASH_RULE" "WORD_CONTAINS_UNDERSCORE" "WHITESPACE_RULE")
      langtool-bin nil ;;"/home/owner/.nix-profile/bin/languagetool-commandline"
      langtool-language-tool-server-jar "/home/owner/.nix-profile/share/languagetool-server.jar"
      )


(setq +lookup-provider-url-alist
      '(
        ;; ("Doom Emacs issues" "https://github.com/hlissner/doom-emacs/issues?q=is%%3Aissue+%s")
        ("Startpage"            "https://www.startpage.com/sp/search?query=%s&cat=web&pl=opensearch&language=english")
        ;; ("Google images"     "https://www.google.com/images?q=%s")
        ;; ("Google maps"       "https://maps.google.com/maps?q=%s")
        ("DuckDuckGo"        +lookup--online-backend-duckduckgo "https://duckduckgo.com/?q=%s")
        ;; ("StackOverflow"     "https://stackoverflow.com/search?q=%s")
        ("Github"            "https://github.com/search?ref=simplesearch&q=%s")
        ("Youtube"           "https://yewtu.be/results?aq=f&oq=&search_query=%s")
        ;; ("Wolfram alpha"     "https://wolframalpha.com/input/?i=%s")
        ("Wikipedia"         "https://wikipedia.org/search-redirect.php?language=en&go=Go&search=%s")
        ("WikipediaPL"       "https://wikipedia.org/search-redirect.php?language=pl&go=Go&search=%s")
        ;; ("MDN"               "https://developer.mozilla.org/en-US/search?q=%s")
        ("Diki"              "https://www.diki.pl/slownik-angielskiego?q=%s")
        ))

;;Not sure about colors
(after! lsp-mode

  ;; https://github.com/emacs-lsp/lsp-java/issues/26
  (after! lsp-java

    (setq lombok-library-path (concat doom-data-dir "lombok.jar"))
    (unless (file-exists-p lombok-library-path)
      (url-copy-file "https://projectlombok.org/downloads/lombok.jar" lombok-library-path))

    (setq lsp-java-vmargs '("-XX:+UseParallelGC" "-XX:GCTimeRatio=4" "-XX:AdaptiveSizePolicyWeight=90" "-Dsun.zip.disableMemoryMapping=true" "-Xmx4G" "-Xms100m"))
    (push (concat "-javaagent:" (expand-file-name lombok-library-path)) lsp-java-vmargs))

  (setq lsp-java-jdt-download-url "https://www.eclipse.org/downloads/download.php?file=/jdtls/milestones/1.36.0/jdt-language-server-1.36.0-202405301306.tar.gz"
        lsp-java-configuration-runtimes '[
					  (:name "JavaSE-21"
					   :path "/nix/store/bk3x3ia7gxqic1jgr3dz05gwi8zw671y-openjdk-21+35" ;; FIXME - use non-gced path
					   :default t)])

  (setq-local file-name-handler-alist nil) ;; FIXME - no idea why it fixed go to reference for files is external sources

  (map!
   :map flycheck-mode-map
   :prefix "C-c !"
   "y" #'j/flycheck-yank-error-at-point)

  (require 'jlsp)


  (after! rescript-mode
    (setq lsp-rescript-server-command
          '("node" "/nix/store/61pr617kznr13nhpqq8dkgf0sic201j9-vscode-extension-chenglou92-rescript-vscode-1.16.0/share/vscode/extensions/chenglou92.rescript-vscode/server/out/server.js" "--stdio"))
    ;; Tell `lsp-mode` about the `rescript-vscode` LSP server
    (require 'lsp-rescript)
    ;; Enable `lsp-mode` in rescript-mode buffers
    (add-hook 'rescript-mode-hook 'lsp-deferred)
    ;; Enable display of type information in rescript-mode buffers
    (require 'lsp-ui)
    (add-hook 'rescript-mode-hook 'lsp-ui-doc-mode))


  (setq lsp-response-timeout 180))

(after! lsp-treemacs
  (setq lsp-treemacs-error-list-current-project-only t))

(after! dap-mode
  (setq dap-ui-buffer-configurations
        `((,dap-ui--locals-buffer . ((side . right) (slot . 1) (window-width . 0.28)))
          (,dap-ui--breakpoints-buffer . ((side . left) (slot . 1) (window-width . 0.15)))
          (,dap-ui--sessions-buffer . ((side . left) (slot . 2) (window-width . 0.15)))
          (,dap-ui--expressions-buffer . ((side . left) (slot . 3) (window-width . 0.15) (window-height . 0.15)))
          (,dap-ui--repl-buffer . ((side . bottom) (slot . 1) (window-height . 0.45)))
          (,dap-ui--debug-window-buffer . ((side . bottom) (slot . 3) (window-width . 0.20))))))


(after! graphql-mode
  (require 'lsp)
  (require 'jgraphql)
  (map!
   :map graphql-mode-map
   (:prefix-map  "C-c"
    :desc "Send" "C-c" #'j/graphql-send-query
    :desc "Select endpoint" "C-l" #'j/graphql-select-endpoint
    :desc "Export to curl" "C-e" #'j/graphql-kill-curl
    :desc "Open default variables file" "C-v" #'j/graphql-open-default-variables-file)))

(load! "conf/my-lsp.el")

(defun +protobuf-mode-init-h()
  (require 'scalaproto)
  (require 'jproto)

  (require 'rotate-text)
  (pushnew! rotate-text-local-symbols '("required" "optional" "repeated"))
  (pushnew! rotate-text-local-symbols '("string" "int32" "int64" "bool")))

(add-hook! protobuf-mode
           #'display-line-numbers-mode
           #'+protobuf-mode-init-h
           #'yas-minor-mode-on
           #'hl-line-mode
           ;; #'solaire-mode ;; FIXME - doesn't really work :/
           #'hl-todo-mode)

(after! reason-mode
  (require 'smartparens-ml)
  (require 'smartparens-html)
                                        ;(add-hook 'reason-mode-hook #'lsp)
  )

;; (load "~/git/foss/emacs/rotate-text.el/rotate-text.el")
(use-package! rotate-text
  :commands rotate-text-paren-or-text
  :config
  (require 'rotate-text-paren)
  (require 'rotate-text-paren-smartparens)

  (setq
   rotate-text-paren-get-paren-data-function #'rotate-text-paren-smartparens-get-data
   rotate-text-paren-pairs '(("()" "{}" "[]") ("\"\"" "''"))
   rotate-text-use-defined-casing t)

  (after! hl-todo
    (pushnew! rotate-text-words (mapcar (-compose 'downcase 'car) hl-todo-keyword-faces)))

  (pushnew! rotate-text-symbols
            '("t" "nil")
            '("==" "!=")
            '("<=" "<"))

  (pushnew! rotate-text-words
            '("request" "response")
            '("started" "finished")
            '("seconds" "millis")
            '("true" "false")
            '("error" "warn" "info" "debug")
            '("begin" "end")
            '("open" "close")
            '("opened" "closed")
            '("get" "post" "put" "delete")
            '("yes" "no")))

(after! embark
  ;; Probably could be more vertico - specific and installed in more specific
  ;; map. Not sure how though.
  (defun j/embark-insert-grep-line (string)
    "Insert STRING at point."
    (interactive "sInsert: ")
    (let ((trimmed (->> (s-split ":" string) (-drop 2) (s-join ":"))))
      (cond
       ((not trimmed) (message "Not used in grep/vertico context."))
       (buffer-read-only (with-selected-window (other-window-for-scrolling)
                           (insert trimmed)))
       (t (insert trimmed)))))

  (map! (:map embark-general-map
              "I" #'j/embark-insert-grep-line)

        (:map embark-url-map
              "f" #'j/embark-org-url-with-title)
        (:map embark-link-map
              "f" #'j/embark-link-with-tilte)
        ))

(after! rustic
  (require 'smartparens-rust)
  (defun j/rustic--disable-copilot()
    (setq-local copilot-mode nil))

  (add-hook 'rustic-mode-hook #'j/rustic--disable-copilot))

(use-package! yaml
  :init
  (when (and (fboundp 'treesit-available-p) (treesit-available-p))
    (require 'j-tree-sitter)
    (when (treesit-language-available-p 'yaml)
      (add-hook! yaml-mode #'yaml-pro-ts-mode #'eldoc-mode))))

(use-package! yaml-pro
  :after yaml
  :commands yaml-pro-ts-mode)


;;Smartparens
(after! smartparens
  (add-to-list 'sp--html-modes 'rescript-mode t)
  (let ((+sp-ignored-binddings '("C-<right>")))
    (setq sp-smartparens-bindings (seq-filter
                                   (lambda (entry)  (not (-contains? +sp-ignored-binddings (nth 0 entry))) ) sp-smartparens-bindings)))
  (require 'jsmartparens)
  (smartparens-global-mode 1)
  (sp-use-smartparens-bindings)
  (map!
   (:map smartparens-mode-map
    :nvi  "C-<right>" #'sp-slurp-hybrid-sexp
    :nvi "C-k" #'sp-kill-hybrid-sexp
    :nvi "C-y" #'j/sp-copy-hybrid-sexp
    :nvi "C-M-d" #'sp-down-sexp
    :nvi "C-S-k" #'kill-comment
    :prefix "M-s"
    :nvi "v" #'sp-split-sexp
    :nvi "u" #'sp-splice-sexp
    :nvi "M-n" #'sp-html-next-tag
    :nvi "M-p" #'sp-html-previous-tag)))


;;YASNIPPET
(after! yasnippet
  (require 'jasnippet)

  (map!
   (:map yas-minor-mode-map
    :nvi "M-s-t" #'jasnippet-surround
    :prefix "M-s"
    :nvi  "M-i" #'yas-insert-snippet
    :nvi  "M-s" #'jasnippet-surround
    :nvi  "M-u"   #'+jemacs/unwrap-killing-word-before)))


(add-hook! sh-mode :append (if (s-ends-with? ".env" (buffer-name)) (flycheck-mode 0)))

(setq +zen-text-scale 1.5)


(setq safe-local-variable-values
      '(
        (org-element-use-cache)
        (org-indent-mode)))


(use-package! code-compass

  :config

  (setq code-compass-path-to-code-compass "~/.emacs.d/.local/straight/repos/code-compass/"
        code-compass-download-directory (code-compass--expand-file-name "dependencies")

        code-compass-code-maat-command
        (format "java -jar %s/code-maat-1.0.1-standalone.jar" (code-compass--expand-file-name "dependencies"))
        code-compass-preferred-browser "brave"
        code-compass-exclude-directories  '("node_modules" "bower_components" "vendor" "tmp" "target" ".metals" ".bloop")

        )

  ;; (code-compass-install)
  )
