;;; cc.elcase class mapper -*- lexical-binding: t; -*-

;;; Code:
(defun scalaproto-current-line()
  "."
  (buffer-substring-no-properties
   (line-beginning-position)
   (line-end-position)))

(defun scalaproto--exec(arg)
  (kill-region (region-beginning) (region-end))
  (insert ( cdr (doom-exec-process "scalaproto" arg (pop kill-ring)))))

(defun scalaproto-convert(&optional from to)
  (interactive)
  (kill-region (region-beginning) (region-end))
  (let ((destination (or to (completing-read "Destination lang is?" (list "scala" "json" "proto2")))))
    (insert ( cdr (doom-exec-process "scalaproto" (format "auto-to-%s" destination) (pop kill-ring)))) (insert "\n")))


(defun scalaproto-increasing-numbers()
  "."
  (interactive)
  (scalaproto--exec "fix-proto-numbers")
  )

(provide 'scalaproto)
;;; scalaproto.el ends here
