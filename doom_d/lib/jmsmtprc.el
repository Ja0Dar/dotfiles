;;; jmstprc.el --- Description -*- lexical-binding: t; -*-
;;
;;; Code:

(defun j/msmtp-format-mu4e-context(ctx)
  (let* (         (vars (mu4e-context-vars ctx))
                  (account (mu4e-context-name ctx))
                  (host (alist-get 'smtpmail-smtp-server vars))
                  (port (alist-get 'smtpmail-smtp-service vars))
                  (from (alist-get 'user-mail-address vars))
                  (user (alist-get 'smtpmail-smtp-user vars))
                  )

    (format
     "
account        %s
host           %s
port           %d
from           %s
user           %s
passwordeval   \"gpg2 -q -d ~/.authinfo.gpg 2>/dev/null | awk '/machine %s login %s/{print $NF; exit 0}' | tr -d '\\\"'\"
" account host port from user host user)))


(defun j/mstmprc-file-contet-from-mu4e()
  (let ((prefix "
# Set default values for all following accounts.
defaults
auth           on
tls            on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
tls_fingerprint  1D:34:B2:BF:15:00:B7:9C:7F:39:46:60:D2:E8:F9:5D:88:85:96:83:0F:99:D8:C6:57:C7:BD:79:98:30:92:07
logfile        ~/.msmtp.log
")
        (account-bodies (seq-map #'j/msmtp-format-mu4e-context mu4e-contexts)))

    (s-concat
     prefix
     (s-join "\n" account-bodies))))



(defun j/mstprc-from-mbsyncrc ()
  (interactive)
  (with-temp-file "~/.msmtprc"
    (insert (j/mstmprc-file-contet-from-mu4e))
    (write-file "~/.msmtprc")))

(provide 'jmstprc)
;;; jmstprc.el ends here
