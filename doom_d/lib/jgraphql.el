;;; jgraphql.el --- description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021
;;
;; Author:  JakDar
;; Maintainer:  JakDar
;; Created: January 17, 2021
;; Modified: January 17, 2021
;; Version: 0.0.1
;; Keywords: graphql ide
;; Homepage: https://github.com/owner/jgraphql
;; Package-Requires: ((emacs "27.1") (cl-lib "0.5") (dash "2.17.0") (graphql-mode "1.1"))
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  description
;;
;;; Code:

(defun j/graphql-open-default-variables-file()
  "Open default variables file corresponding to current."
  (interactive)
  (let* ((operation (graphql-current-operation))
         (default-variables-filename (format ".%s.variables.json" operation)))
    (if operation (find-file-other-window default-variables-filename)
      (message "You have to have cursor inside query to recognise it."))))

(defun j/graphql--exists-in-config? (current-url)
  "Return CURRENT-URL if it exists in project graphql config. else - nil."
  (interactive)
  (let ((config (json-read-file (graphql-locate-config "."))))
    (let-alist config
      (->>
       .extensions.endpoints
       (-map (lambda (e) (alist-get 'url e)))

       (-find (lambda (url) (s-equals? url current-url)))))))


(defun j/graphql-send-query ()
  "Send the current GraphQL query/mutation/subscription to server."
  (interactive)

  (if (and graphql-url (not (j/graphql--exists-in-config? graphql-url)))
      (call-interactively #'j/graphql-select-endpoint) )

  (let* (
         ;; FIXME - maybe trim query? (like in curl)
         (operation (graphql-current-operation))
         (default-variables-file (format ".%s.variables.json" operation))
         (graphql-variables-file (if (and
                                      (not current-prefix-arg)
                                      (file-exists-p default-variables-file))
                                     default-variables-file)))
    (if operation
        (call-interactively #'graphql-send-query)
      (message "You have to have cursor inside query to recognise it."))))



(defun j/graphql-kill-curl ()
  "Send the current GraphQL query/mutation/subscription to server."
  (interactive)

  (if (and graphql-url (not (j/graphql--exists-in-config? graphql-url)))
      (call-interactively #'j/graphql-select-endpoint) )

  (let* ((url (or graphql-url (read-string "GraphQL URL: " )))
         (operation (graphql-current-operation))
         (default-variables-file (format ".%s.variables.json" operation))
         (var (if (and
                   (not current-prefix-arg)
                   (file-exists-p default-variables-file))
                  default-variables-file
                (read-file-name "GraphQL Variables: "))))

    (if operation
        (let ((graphql-url url)
              (graphql-variables-file var))

          (let* ((query (s-collapse-whitespace (j/graphql--keep-important-lines (buffer-substring-no-properties (point-min) (point-max)))))
                 (variables (graphql-current-variables var))
                 (body (graphql-encode-json query operation variables))
                 (headers (append '(("Content-Type" . "application/json")) graphql-extra-headers))
                 (header-curl-list (-flatten  (mapcar (lambda(x) (list "-H" (format "'%s: %s'" (car x) (cdr x)))) graphql-extra-headers)))
                 (curl-command-list  (append
                                      (list "curl" "-X" "POST" graphql-url )
                                      header-curl-list
                                      (list "--data-raw" (concat "'" body "'"))
                                      ))
                 (curl-command (string-join curl-command-list " "))
                 ;; (curl-command (combine-and-quote-strings curl-command-list " "))
                 )
            (kill-new curl-command)
            (message "Killed curl command for operaton '%s'." operation)))
      (message "You have to have cursor inside query to recognise it."))))

(defun j/graphql-stubgen ()
  "Generate stub for graphql method."
  (interactive)
  (let* ((operation-type (completing-read "What stub do you want?" '("mutations" "queries")))
         (queries (json-read-file (concat  (projectile-project-root) "/."  operation-type ".json")))
         (names (-map (lambda (el)(alist-get 'name  el)) queries))
         (selected-name  (completing-read "Which query select?" names))
         (idx  (-find-index (lambda (el) (s-equals? el selected-name) ) names))
         (entry (elt queries idx)))


    (find-file (j/graphql--save-entry-stub entry))))


(defun j/graphql--save-entry-stub(entry)
  "Generate files in (project)/queries for ENTRY - alist of name, variables, query."
  (let* (
         (name (alist-get 'name entry))
         (gql-file (concat (projectile-project-root) "queries/" name  ".graphql"))
         (variables-file (concat (projectile-project-root) "queries/." name ".variables.json")))

    (with-temp-file gql-file (insert (alist-get 'query entry)))
    (with-temp-file variables-file (insert (alist-get 'variables entry)))
    (message "Saved graphql and variable files for %s" name )
    gql-file))

(defun j/graphql--is-comment-line(s)
  "Return t if S is a comment line."
  (or
   (s-prefix? "#" s)

   (let ((split (s-split "#" (s-collapse-whitespace s))))
     (and
      (< 1 (length split))
      (s-equals? (car split) " ")))))

(defun j/graphql--is-important-line(s)
  "Return t if S is not a comment line."
  (not
   (or
    (string-empty-p s)
    (s-equals? (s-collapse-whitespace s) " ")
    (j/graphql--is-comment-line s))))


(defun j/graphql--keep-important-lines(s)
  "Remove comments from graphql string S."
  (->>
   s
   (s-split "\n" )
   (-filter #'j/graphql--is-important-line)
   (s-join "\n")))

(defun j/graphql-select-endpoint ()
  "Set parameters based off of the endpoints listed in a .graphqlconfig file. Modified version of graphql-select-endpoint."
  (interactive)
  (let ((config (json-read-file (graphql-locate-config "."))))
    (let-alist config
      (if-let ((endpoints .extensions.endpoints)
               (endpoint (cdr (assq (intern (graphql--completing-read-endpoint endpoints)) endpoints))))
          (let-alist endpoint

            (let* (
                   (computed-headers (-map (lambda (header) (cons (car header) (j/graphql--exec-script (cdr header))) ) .script-headers))
                   (duplicated-header-names (->>
                                             (append computed-headers .headers)
                                             (-group-by 'car )
                                             (-map (lambda (h) (cons (car h) (length (cdr h)))))
                                             (-filter (lambda (h) (< 1 (cdr h))))
                                             (-map 'car))))

              (if duplicated-header-names
                  (warn "Found duplicated headers %S" duplicated-header-names))

              (setq graphql-url .url
                    graphql-extra-headers (append computed-headers .headers))))
        (error "No endpoint configurations in .graphqlconfig")))))


;; NOTE: depends on doom - if published - we'll have to copy code 🤷‍♂️
(defun j/graphql--exec-script(filename)
  "Execute sh script (FILENAME) in project root."
  (cdr (doom-exec-process "sh" (concat (projectile-project-root) filename ))))

;; Curl import

(defun j/graphql-import-curl--endpoint(&optional cmd)
  (message "Processed with %S"
           (doom-exec-process "python"
                              "/home/owner/git/dotfiles/lib/jgraphql-cli"
                              (projectile-project-root)
                              (or cmd (pop kill-ring))
                              "upsert_endpoint"
                              (read-string "Name of the endpoint: ")
                              ))
  (save-window-excursion
    (find-file (concat (projectile-project-root) "/.graphqlconfig"))
    (+format/region-or-buffer)
    (save-buffer)))

(defun j/graphql-import-curl--request(&optional cmd)
  (message "Processed with %S"
           (doom-exec-process "python"
                              "/home/owner/git/dotfiles/lib/jgraphql-cli"
                              (projectile-project-root)
                              (or cmd (pop kill-ring)) ;; NOTE: good or not?
                              "upsert_request"
                              )))


(defun j/graphql-import-curl()
  (interactive)
  (let ((option (completing-read "What to import?" (list "request" "endpoint" "both")))
        (cmd (read-string "Curl: ")))

    (pcase option
      ("request" (j/graphql-import-curl--request cmd))
      ("endpoint" (j/graphql-import-curl--endpoint cmd))
      ("both"
       (j/graphql-import-curl--endpoint cmd)
       (j/graphql-import-curl--request cmd))
      )))


(provide 'jgraphql)
;;; jgraphql.el ends here
