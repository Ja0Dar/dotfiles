

(defvar j/inbox-capture-file "/home/owner/next/notes/gtd/inbox.org")
(defvar j/inbox-mobile-capture-file "/home/owner/next/notes/gtd/mobile-inbox.org")

;; From https://mollermara.com/blog/Fast-refiling-in-org-mode-with-hydras/
(defun j/inbox-org-refile (file headline &optional arg)
  (let ((pos (save-excursion
               (find-file file)
               (org-find-exact-headline-in-buffer headline))))
    (org-refile arg nil (list headline file nil pos)))
  (switch-to-buffer (current-buffer)))


(defun j/inbox-capture-refile-whole-mobile ()
  "Refile all headings from mobile inbox to main inbox."
  (interactive)
  (save-excursion
    (with-current-buffer (find-file-noselect j/inbox-mobile-capture-file)
      (org-mode)
      (goto-char (point-min))
      ;; Check if there are any headings in the file
      (if (not (re-search-forward "^\\*" nil t))
          (message "No headings found in mobile inbox")
        ;; Go back to beginning and prepare to refile
        (goto-char (point-min))
        (when (not (org-at-heading-p))
          (org-next-visible-heading 1))
        ;; Only proceed if we found a heading
        (when (org-at-heading-p)
          (push-mark (point-max) nil t) ;; Activate the mark
          (j/inbox-org-refile j/inbox-capture-file "Inbox")
          (org-save-all-org-buffers)
          (message "Mobile inbox refiled successfully"))))))



(provide 'j-inbox)
