;;; jhydras.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021
;;
;; Author:  <https://github.com/owner>
;; Maintainer:  <owner@hpek>
;; Created: September 18, 2021
;; Modified: September 18, 2021
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/owner/jhydras
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:


(defhydradio j/hydra/dired-goto ()
  (other "Open thing in other buffer" ))

(defun j/hydra--split-horizontally-if-single-window()
  (if (eq 1 (length (window-list)))
      (split-window-horizontally)))

;; TODO - move to workspace




(defun j/hydra--diredfun(arg)
  (if j/hydra/dired-goto/other
      (progn
        (j/hydra--split-horizontally-if-single-window)
        (dired-other-window arg))
    (dired arg)))


(defun j/hydra--find-filefun(arg)
  (if j/hydra/dired-goto/other
      (progn
        (j/hydra--split-horizontally-if-single-window)
        (find-file-other-window arg))
    (find-file arg)))

(defhydra j/hydra/dired-goto (:color blue)
  "
    Directories             Notes                          Configs
    ────────────────────────────────────────────────────────────────────────
    _n_ ~/next                _G_ org-gtd-tasks.org         _f_ system flake.nix
    _m_ ~/next/memes          _I_ inbox.org
    _w_ ~/tmp/work            _M_ mobile.org
    _z_ ~/git/zowie           _C_ recurring.org
    _d_ ~/Downloads           _R_ remote-calendar.org
    _h_ $HOME                 _S_ someday.org
    _r_ /                     _F_ feed.org
    _g_ gtd split
    _y_ Completed yesterday


    ^^
    [_q_] cancel                [_C-o_] Open in other buffer:    % -15`j/hydra/dired-goto/other^^^
    "
  ;; Directories
  ("n" (j/hydra--diredfun "~/next/"))
  ("m" (j/hydra--diredfun "~/next/memes/"))
  ("w" (j/hydra--diredfun "~/tmp/work/"))
  ("z" (j/hydra--diredfun "~/git/zowie/"))
  ("d" (j/hydra--diredfun "~/Downloads/"))
  ("h" (j/hydra--diredfun "~/"))
  ("r" (j/hydra--diredfun "/"))
  ("g" (j/workspaces-gtd))
  ("y" (call-interactively 'j/org-gtd-show-done-tasks-by-date))

  ;; Notes
  ("G" (j/hydra--find-filefun "~/next/notes/gtd/org-gtd-tasks.org"))
  ("I" (j/hydra--find-filefun "~/next/notes/gtd/inbox.org"))
  ("M" (j/hydra--find-filefun "~/next/notes/gtd/mobile.org"))
  ("C" (j/hydra--find-filefun "~/next/notes/gtd/recurring.org"))
  ("R" (j/hydra--find-filefun "~/next/notes/gtd/remote-calendar.org"))
  ("S" (j/hydra--find-filefun "~/next/notes/gtd/defer/someday.org"))
  ("F" (j/hydra--find-filefun "~/next/notes/feed.org"))

  ;; Configs
  ("f" (j/hydra--find-filefun "~/git/dotfiles/sym/common/dot_config/nixpkgs/flake.nix"))

  ;; Footer
  ("q" nil "cancel")
  ("C-o" (j/hydra/dired-goto/other) nil :exit nil))

(provide 'jhydras)
;;; jhydras.el ends here
