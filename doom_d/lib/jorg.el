;;; jorg.el --- Description -*- lexical-binding: t; -*-

;; Package-Requires: ((emacs "26.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:


;; from some stack overflow

(defun j/org-remove-overlays()
  "In jorg as it is used for org-num"
  (interactive)
  (remove-overlays))

(defun j/org-sort-multi (&rest sort-types)
  "Sort successively by a list of criteria, in descending order of importance. For example, sort first by TODO status, then by priority, then by date, then alphabetically, case-sensitive. Each criterion is either a character or a cons pair (BOOL . CHAR), where BOOL is whether or not to sort case-sensitively, and CHAR is one of the characters defined in ``org-sort-entries-or-items''.

So, the example above could be accomplished with:
 (org-sort-multi ?o ?p ?t (t . ?a))"
  (interactive)
  (mapc #'(lambda (sort-type)

            (cond ((functionp sort-type)
                   (org-sort-entries nil ?f sort-type))

                  (t
                   (when (characterp sort-type) (setq sort-type (cons nil sort-type)))
                   (org-sort-entries (car sort-type) (cdr sort-type)))))

        (reverse sort-types)))


(defun j/nov--find-file-other-window (file index point)
  "Open FILE in nov-mode and go to the specified INDEX and POSITION.
If FILE is nil, the current buffer is used."
  (when file
    (find-file-other-window file))
  (unless (eq major-mode 'nov-mode)
    (nov-mode))
  (when (not (nov--index-valid-p nov-documents index))
    (error "Invalid documents index"))
  (setq nov-documents-index index)
  (nov-render-document)
  (goto-char point))


(defun j/org-sort-custom--todo-sort ()
  (cl-position
   (nth 2 (org-heading-components))
   '(nil "STRT" "NEXT" "TODO" "YES" "LOOP" "WAIT" "HOLD" "PROJ" "DONE" "KILL" "YES" "NO")
   :test 'equal))


(defun j/org-sort-custom--today-first ()
  (let ((tag-str (nth 5 (org-heading-components)))
        (todo-keyword (nth 2 (org-heading-components))))

    (if (and (-contains-p (list "TODO" "STRT" "NEXT") todo-keyword)
             tag-str
             (s-contains? ":today:" tag-str t))
        -1
      1)))

(defun j/org-sort-custom ()
  "Sort children of node by:
1. with today tag
2. By todo - custom order
3. By priority."


  (interactive)
  (j/org-sort-multi 'j/org-sort-custom--today-first 'j/org-sort-custom--todo-sort ?p)
  (org-update-statistics-cookies nil)
  (j/org-show-just-me))


(defun j/org-sort-custom-parent()
  (interactive)
  (save-excursion
    (org-up-heading-safe)
    (j/org-sort-custom)))

;; from https://emacs.stackexchange.com/a/59045
(defun j/org-show-just-me (&rest _)
  "Fold all other trees, then show direct children of current org-heading."
  (interactive)
  (org-cycle-set-startup-visibility)
  (org-reveal)
  (org-show-children))


(defun j/org-refile-local ()
  (interactive)
  (let ((org-refile-targets '((nil :level . 1))))
    (call-interactively  #'org-refile)))

(defun j/org-sort-partent-if-gtd-h ()
  (if (s-ends-with-p "/gtd/" (file-name-directory (buffer-file-name)))
      (j/org-sort-custom-parent)))


(defun j/count-subtree-items (point)
  "Count items in subtree at POINT."
  (save-excursion
    (goto-char point)
    (let ((count (length (org-map-entries t nil 'tree))))
      (1- count))))  ; Subtract 1 to exclude the parent heading

(provide 'jorg)
;;; jorg.el ends here
