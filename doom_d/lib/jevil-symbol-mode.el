;;; jevil-symbol-mode.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021
;;
;; Created: October 07, 2021
;; Modified: October 07, 2021
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.4"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:


(defun jevil-symbol-forward-evil-word (&optional count)
  "Move forward COUNT words.
Moves point COUNT words forward or (- COUNT) words backward if
COUNT is negative. Point is placed after the end of the word (if
forward) or at the first character of the word (if backward). A
word is a sequence of word characters matching
\[[:word:]] (recognized by `forward-word'), a sequence of
non-whitespace non-word characters '[^[:word:]\\n\\r\\t\\f ]', or
an empty line matching ^$."
  (evil-forward-nearest
   count
   #'(lambda (&optional cnt)
       (let ((word-separating-categories evil-cjk-word-separating-categories)
             (word-combining-categories evil-cjk-word-combining-categories)
             (pnt (point)))
         (forward-word cnt)
         (if (= pnt (point)) cnt 0)))
   #'(lambda (&optional cnt)
       (evil-forward-chars "^[:word:]\n\r\t\f " cnt))
   #'forward-evil-empty-line))



(defun jevil-symbol-mode--turn-on()
  "Turn on forward-symbol-mode."
  (message "Jevil symbol mode ON.")
  (defadvice! j/forward-symbol-not-word (&optional count)
    :override 'forward-evil-word
    (if jevil-symbol-mode  (forward-evil-symbol count) (jevil-symbol-forward-evil-word count))))

(defun jevil-symbol-mode--turn-off()
  "Turn on forward-symbol-mode."
  (message "Jevil symbol mode OFF."))

(define-minor-mode jevil-symbol-mode
  "Forward symbol instead of forwarding word."
  :lighter " 🕉"
  :global nil
  (if jevil-symbol-mode
      (jevil-symbol-mode--turn-on)
    (jevil-symbol-mode--turn-off)))


(provide 'jevil-symbol-mode)
;;; jevil-symbol-mode.el ends here
