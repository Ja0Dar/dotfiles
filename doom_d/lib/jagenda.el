;;; jagenda.el --- description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2020
;;
;; Created: September 24, 2020
;; Modified: September 24, 2020
;; Version: 0.0.1
;; Keywords:
;; Package-Requires: ((emacs 27.1.50) (cl-lib "0.5"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  description
;;
;;; Code:

;; TODO - allign tags independently of keyword

(defvar elegant-agenda-transforms nil)

(defvar elegant-agenda-face-remappings (let ((face-height (face-attribute 'default :height)))
                                         (list
                                          (list 'default (list :family "Roboto Mono"
                                                               :height (ceiling (* face-height 1.3)) :weight 'normal))
                                          (list 'header-line (list :family "Hack" ;; Make em-dashes stick together
                                                                   :height (ceiling(* face-height 1.5)) :weight 'normal
                                                                   :underline nil  :overline nil :box nil))
                                          (list 'bold (list :height (ceiling (* face-height 1.1)) :weight 'normal))
                                          '(italic (:foreground "orange"))
                                          '(org-link (:foreground "white"))))
  "A list of faces and the associated specs that will be remapped when elegant-agenda-mode is enabled")

(defun jagenda-format-header(prefix)
  "Formats PREFIX with m-dashes so that it occupies whole line."
  (format "%s%s" prefix (make-string (- (window-width) (length prefix)) ?— t)))

(defun elegant-agenda--enable ()
  (let ((title "—  A G E N D A  "))
    ;; (setq-local line-spacing 5)
    (setq-local org-agenda-use-time-grid nil
                tab-width 8
                org-agenda-block-separator "xx"
                org-agenda-block-separator nil
                mode-line-format nil
                header-line-format (jagenda-format-header title))
    (display-line-numbers-mode 0)
    (setq elegant-agenda-transforms
          (mapcar (lambda (face-&-spec)
                    (face-remap-add-relative (car face-&-spec) (cadr face-&-spec)))
                  elegant-agenda-face-remappings))))

(defun elegant-agenda--disable ()
  (setq-local line-spacing (default-value 'line-spacing)
              org-agenda-use-time-grid (default-value 'line-spacing)
              org-agenda-block-separator (default-value 'org-agenda-block-separator))

  (mapc #'face-remap-remove-relative
        elegant-agenda-transforms)
  (setq-local elegant-agenda-transforms nil)
  (setq-local mode-line-format (default-value 'mode-line-format))
  (setq-local header-line-format nil))

(define-minor-mode elegant-agenda-mode
  "Provides a more elegant view into your agenda"
  :init-value nil :lighter " elegant-agenda" :keymap nil
  (if elegant-agenda-mode
      (elegant-agenda--enable)
    (elegant-agenda--disable))
  (force-window-update (current-buffer)))


(defun my-org-agenda-override-header (orig-fun &rest args)
  "Change the face of the overriden header string if needed.
The propertized header text is taken from `org-agenda-overriding-header'.
The face is only changed if the overriding header is propertized with a face."
  (let ((pt (point))
        (header org-agenda-overriding-header))
    (apply orig-fun args)
    ;; Only replace if there is an overriding header and not an empty string.
    ;; And only if the header text has a face property.
    (when (and header (> (length header) 0)
               (get-text-property 0 'face header))
      (save-excursion
        (goto-char pt)
        ;; Search for the header text.
        (search-forward header)
        (unwind-protect
            (progn
              (read-only-mode -1)
              ;; Replace it with the propertized text.
              (replace-match header))
          (read-only-mode 1))))))

(defun my-org-agenda-override-header-add-advices ()
  "Add advices to make changing work in all agenda commands."
  (interactive)
  (dolist (fun '(org-agenda-list org-todo-list org-search-view org-tags-view))
    (advice-add fun :around #'my-org-agenda-override-header)))




(defvar jagenda--super-agenda-groups
  '(
    (:name "👷 In progress" :todo "STRT" :order 0)
    (:name "🕰 Today" :tag "today" :order 1)
    (:name "🛑 Important" :priority "A" :order 2)
    (:name "🟦 Next" :todo "NEXT" :order 3)
    (:name "🟩 Todo" :todo "TODO" :order 4) ;; Shouldn't occur?
    (:name "🟨 Waiting" :todo "WAIT" :order 8)))


;; week plan agenda
(defun jagenda-skip()
  (or (outline-next-heading)
      (goto-char (point-max))))


(defun jagenda-skip-fun-keep-tags(&optional tags)
  (let ((has-relevant-tags (-intersection (org-get-tags) tags))
        (is-done (-contains? org-done-keywords-for-agenda (org-get-todo-state)))
        (tags-empty (seq-empty-p (org-get-tags))))

    (when
        (or
         is-done
         (and
          ;; We query any tags
          (not (seq-empty-p tags))
          ;; But they don't match
          (and (not tags-empty) (not has-relevant-tags))
          ))
      (jagenda-skip))))

(defun jagenda-skip-f-keep-only-unscheduled-for-tags(&optional tags)
  "skip when not in tags or scheduled"
  (when
      (and (not (seq-empty-p tags))
           (or (not (-intersection (org-get-tags) tags))
               (org-get-scheduled-time (point))))
    (jagenda-skip))
  )





;; FIXME - make no tags equal all tags
(defmacro jagenda-add-weekly-tag-jagenda(binding header tags)
  "Create agenda command for weekly agenda with TAGS.
BINDING is the keybinding, HEADER is the header string and TAGS is the list of tags to keep.
If TAGS is nil, all tags are kept."

  `(org-add-agenda-custom-command
    '(,binding ,header
      ((agenda "" ((org-agenda-span 'week)
                   (org-agenda-skip-function '(lambda () (jagenda-skip-fun-keep-tags ,tags)))
                   (org-super-agenda-groups jagenda--super-agenda-groups
                                            )))
       (alltodo ""
                ;; TODO - add line above
                ((org-agenda-overriding-header
                  (propertize
                   (jagenda-format-header  "— B A C K L O G")
                   'face '(:inherit 'header-line :foreground "#0000fafa9a9a")))
                 (org-agenda-skip-function '(lambda () (jagenda-skip-f-keep-only-unscheduled-for-tags ,tags)))
                 (org-super-agenda-groups jagenda--super-agenda-groups))
                )))))

;; Config
(my-org-agenda-override-header-add-advices)
(jagenda-add-weekly-tag-jagenda "w" "Work" (list "@work"))
(jagenda-add-weekly-tag-jagenda "m" "Me" (list "@me"))
(jagenda-add-weekly-tag-jagenda "c" "Config" (list "@config"))
(jagenda-add-weekly-tag-jagenda "h" "Home" (list "@home"))
(jagenda-add-weekly-tag-jagenda "p" "Private" (list "@home" "@me" "@config"))
(jagenda-add-weekly-tag-jagenda "a" "All" nil)


;; TODO: add week 9 to 5 if week and 9 to 5
;; TODO: hide next actions ( hide todo? )


(provide 'jagenda)
;;; jagenda.el ends here
