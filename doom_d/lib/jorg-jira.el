;;; +org-jira.el --- Description -*- lexical-binding: t; -*-

;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(defun j/org-jira-get-epic-issues()
  "Get all issues for the current epic."
  (interactive)
  (org-jira-get-issues-headonly
   (jiralib-do-jql-search (format "\"Epic Link\" = %s" (org-jira-parse-issue-id)))))

(provide 'jorg-jira)
;;; +org-jira.el ends here
