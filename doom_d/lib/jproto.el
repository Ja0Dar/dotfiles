;;; jproto.el --- description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2020
;;
;; Created: May 25, 2020
;; Modified: May 25, 2020
;; Version: 0.0.1
;; Keywords:
;; Package-Requires: ((emacs 27.0.90) (cl-lib "0.5"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  description
;;
;;; Code:


(defun +proto/search-entity()
  "Search class/trait/object using `consult-grep'."
  (interactive)
  (j/default/search-symbol  "(message|enum) "))


(provide 'jproto)
;;; jproto.el ends here
