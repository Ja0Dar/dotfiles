;;; jmaven.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021
;;
;; Author: Ja0Dar
;; Maintainer: Ja0Dar
;; Created: October 20, 2021
;; Modified: October 20, 2021
;; Version: 0.0.1
;; Homepage: https://bitbucket.com/Ja0Dar/dotfiles
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; Next steps
;; - TODO Migrate (at least partially) to scaladex to be able to package verisions and not use latest by version (fallbacking to maven for java?) https://github.com/Baccata/vscode-scaladex-search/blob/main/src/main/scala/scaladex.scala
;; - filter by scala version
;; - sort by version count
;; - add narrowing?
;; - add add pretty annotation?
;; - Add embark methods instead of those ad-hoc?

(defun jmaven-search(query &optional rows)
  (interactive)
  (require 'request)
  (require 'dash)
  (let* (
         ;; (query-split (s-split " " query t))
         ;; (query (if (eq 2 (length query-split)) (format "g:%s AND a:%s" (nth 0 query-split) (nth 1 query-split)) query))
         (rows (or rows 200))
         (response (request "https://search.maven.org/solrsearch/select"
                     :params (list (cons "q"  query) (cons "rows"  rows))
                     :sync t
                     :parser 'json-read ))
         (status (request-response-status-code response))
         (body (request-response-data response))
         (docs (let-alist body .response.docs)))

    (thread-last
      docs
      (seq-filter (lambda (d) (let-alist d (> .versionCount (if (< (length docs) rows) 3 10)))))
      (seq-filter (lambda (d) (let-alist d (not (-some (lambda (suffix) (s-contains-p suffix .a)) '("_sjs" "_2.10" "_2.11")))))) ;; remove old scala versions & scalajs
      (seq-map (lambda (d) (cons (jmaven--format-entry-to-see d) d))))))

(defun jmaven--format-millis(millis)
  (format-time-string "%b %d %Y" (round (/ millis 1000))  t))

(defun jmaven--format-entry-to-see (json-item)
  "Format JSON-ITEM as sbt entry."
  (let-alist json-item
    ;; .timestamp - for sorting
    (format "%S" json-item)))
;; (format "%s/%s/%s  vc:%s time:%s" .g  .a  .latestVersion .versionCount (jmaven--format-millis .timestamp))))

(defun jmaven--format-entry (json-item tpe)
  "Format JSON-ITEM in various formats."
  (let-alist json-item
    (let* ((a-trimmed (->> .a (replace-regexp-in-string "_2\.1[0-9]$" "" ) (replace-regexp-in-string "_3$" "" )))
           (is-java-dep (s-equals? a-trimmed .a)))
      (pcase `(,tpe  ,is-java-dep)
        ('(ivy t) (format "import $ivy.`%s:%s:%s`" .g  .a .latestVersion))
        ('(ivy nil) (format "import $ivy.`%s::%s:%s`" .g  a-trimmed .latestVersion))
        ('(sbt t) (format "\"%s\" %s \"%s\" %s \"%s\"" .g "%"  .a "%" .latestVersion))
        ('(sbt nil) (format "\"%s\" %s \"%s\" %s \"%s\"" .g "%%"  a-trimmed "%" .latestVersion))
        ((pred (lambda (l) (eq (car l) 'web))) (format "https://mvnrepository.com/artifact/%s/%s" .g  .a))
        ('(scala-cli t) (format "//> using lib \"%s:%s:%s\"" .g  .a .latestVersion))
        ('(scala-cli nil) (format "//> using lib \"%s::%s:%s\"" .g  a-trimmed .latestVersion))
        (_ (error "Unknown type %s" tpe))))))

(defun jmaven--consult-search (sync)
  (lambda (action)
    (cond
     ((and (stringp action) (not (s-equals-p action "")))
      (when-let (cands (jmaven-search action))
        (funcall sync 'flush)
        (funcall sync cands)))
     (t (funcall sync action)))))

(defun jmaven--copy-and-say(text)
  (kill-new text)
  (message "Copied %s." text))

(defun jmaven-insert-dependency()
  (interactive)
  (let* ((data
          (consult--read
           (thread-first (consult--async-sink)
                         ;; (consult--async-refresh-immediate)
                         (consult--async-refresh-timer)
                         (jmaven--consult-search)
                         ;; (consult--async-transform #'jmaven--format-entry-sbt)
                         (consult--async-throttle 0.1 0.5)
                         )
           :sort t
           :lookup #'consult--lookup-cdr ;; use cdr as result of matching when grepping on car
           ;; :annotate
           :prompt "Entry: "
           :category 'scala))
         (action (consult--read '("sbt" "ivy" "web" "scala-cli"))))

    (pcase action
      ("sbt" (jmaven--copy-and-say (jmaven--format-entry data 'sbt)))
      ("ivy" (jmaven--copy-and-say (jmaven--format-entry data 'ivy)))
      ("scala-cli" (jmaven--copy-and-say (jmaven--format-entry data 'scala-cli)))
      ("web" (browse-url (jmaven--format-entry data 'web))))))


(provide 'jmaven)
;;; jmaven.el ends here
