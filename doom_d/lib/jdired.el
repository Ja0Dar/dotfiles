;;; jdired.el --- Description -*- lexical-binding: t; -*-
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
(defun j/dired-dragon(&optional arg)
  "Run dragon-drag-and-drop (dragon) on ARG - selected files in dired/ranger."
  (interactive "P")
  (require 'dash)
  (let* ((filenames (-map 'car (dired-map-over-marks (cons (dired-get-filename) (point)) arg)))
         (commandline (-concat '("dragon" "-x" "-a") filenames)))
    (apply #'doom-exec-process  commandline)))



(provide 'jdired)
;;; jdired.el ends here
