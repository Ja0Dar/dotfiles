;;; emacs jbeancount.el --- Description -*- lexical-binding: t; -*-

;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(after! language-id
  (pushnew! language-id--definitions
            '("Beancount"
              (beancount-mode
               (language-id--file-name-extension ".bean"))
              (beancount-mode
               (language-id--file-name-extension ".beancount")))))


(provide 'jbeancount)
;;; emacs jbeancount.el ends here
