;;; jworkspaces.el --- description -*- lexical-binding: t; -*-

;;; Code:

(defun jworkspace--is-eshell-buffer(name)
  "Whether NAME string is a name of eshell buffer."
  (and
   (s-starts-with? "*" name)
   (s-ends-with? "*" name)
   (s-contains? "eshell" name)))

(defun j/workspaces-rename-to-current-project()
  "."
  (interactive)
  (let* ((current-name (+workspace-current-name))
         (project-name (projectile-project-name))

         (new-name (cond
                    ((jworkspace--is-eshell-buffer (buffer-name)) "eshell")
                    ((not (s-equals? current-name project-name ))  project-name)
                    (t nil)
                    )))

    (if new-name
        (progn
          (+workspace-rename current-name new-name)
          (+workspace/display)
          )
      (message "Name already set."))))

(defun j/workspaces-kill-other-workspaces()
  (interactive)
  (mapcar #'+workspace-kill
          (seq-filter (lambda (x) (not (s-equals-p x (+workspace-current-name))))
                      (+workspace-list-names)))
  (let (message-log-max)
    (message "%s %s" (+workspace--tabline) "                Killed other workspaces.")))


(defvar j/workspaces-gtd-name "*gtd*")

(defun j/workspaces--move-current-to-first()
  (let ((current-ws-idx  (seq-position (+workspace-list-names) (+workspace-current-name))))
    (if (not (eq current-ws-idx 0))
        (+workspace/swap-left current-ws-idx)))
  )


(defun j/workspaces--gtd-setup-buffers()

  (doom/window-maximize-buffer)
  (find-file "~/next/notes/gtd/org-gtd-tasks.org")
  ;; HACK fix for org-alert waking up things to early
  (unless (bound-and-true-p org-modern-mode) (org-mode))
  (split-window-horizontally)
  (find-file-other-window "~/next/notes/gtd/mobile-inbox.org")
  (when visual-line-mode (visual-line-mode))
  (split-window-below)
  (enlarge-window (truncate (* (window-height) 0.4)))
  (find-file "~/next/notes/gtd/inbox.org")
  (when visual-line-mode (visual-line-mode))
  (select-window (get-buffer-window "org-gtd-tasks.org" 'visible))

  ;; FIXME - move cursor to gtd, prevent artifacts when running twice etc
  ;; (enlarge-window (- (truncate (* (window-width) 0.1))) t)
  )

(defun j/workspaces-gtd()
  (interactive)
  (unless (+workspace-exists-p j/workspaces-gtd-name) (persp-add-new j/workspaces-gtd-name))
  (+workspace-switch j/workspaces-gtd-name)
  (j/workspaces--gtd-setup-buffers)
  (j/workspaces--move-current-to-first))

(provide 'jworkspaces)
;;; jworkspaces.el ends here
