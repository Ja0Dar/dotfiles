;;; ~/git/dotfiles/emacs/jsearch.el -*- lexical-binding: t; -*-

;;; Code:
(defun j/search-git-root (&optional arg)
  (interactive "P")
  (let* ((projectile-project-root nil)
         (disabled-command-function nil)
         (current-prefix-arg (unless (eq arg 'other) arg))
         (default-directory (vc-git-root ".")))
    (call-interactively
     (cond ((modulep! :completion ivy)     #'+ivy/project-search)
           ((modulep! :completion helm)    #'+helm/project-search)
           ((modulep! :completion vertico) #'+vertico/project-search)
           (#'projectile-ripgrep)))))

(defun j/search-find-file-git-root (&optional arg)
  "Run `+vertico-file-search' in git root returned by `magit-toplevel'."
  (interactive "P")
  (require 'magit)
  (let (
        (default-directory (vc-git-root "."))
        (disabled-command-function nil) ;TODO:bcm  make it not jump or disable lsp loading on preview
        )
    (call-interactively #'+vertico/find-file-in)))



(defun j/default/search-symbol (prefix)
  "PREFIX - escaped thing to prefix in `counsel-projectile-rg'."
  (cond
   ((modulep! :completion vertico)
    (+vertico-file-search :query prefix :in projectile-project-root :all-files nil))
   ((modulep! :completion ivy)
    (let ((counsel-projectile-rg-initial-input (format "%s " prefix))
          (ivy-more-chars-alist   '((t . 0)))
          )
      (counsel-projectile-rg)))
   (t (message "Completion method not supported"))))

(provide 'jsearch)
