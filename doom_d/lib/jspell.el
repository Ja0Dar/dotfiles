;;; jspell.el --- Description -*- lexical-binding: t; -*-
;;; Code:

(define-minor-mode jspell-mode
  "Minor mode for spell-checking in Emacs.
jspell-mode is enabledspell-fu mode and provides additional functionality for spell-checking."
  :lighter " jspell"
  (if jspell-mode
      (progn
        (spell-fu-mode 1)
        (j/spell-fu-setup)
        (j/ispell-dict-setup)
        (pushnew! completion-at-point-functions 'ispell-completion-at-point)
        )
    (progn
      (spell-fu-mode -1)
      (setq completion-at-point-functions
            (delq 'ispell-completion-at-point completion-at-point-functions))
      )))

(defun j/ispell--personal-dictionary-filename (dict)
  (format "/home/owner/.aspell.%s.pws" dict))

(defun j/ispell-set-dict (&optional dict)
  (interactive
   (list (completing-read "Which dictionary? " '("en" "pl"))))
  (setq ispell-dictionary (or dict
                              (completing-read "Which dictionary? " '("en" "pl")))
        ispell-personal-dictionary (j/ispell--personal-dictionary-filename ispell-dictionary)))

(defun j/spell-fu-setup()
  (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "en"))
  (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "pl"))
  (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "en-computers"))
  (spell-fu-dictionary-add
   (spell-fu-get-personal-dictionary "pl-personal" "/home/owner/.aspell.pl.pws"))
  (spell-fu-dictionary-add
   (spell-fu-get-personal-dictionary "en-personal" "/home/owner/.aspell.en.pws")))


(defun j/ispell-dict-setup ()
  "Update the ispell word dictionary if needed."
  (when ispell-complete-word-dict
    (let* ((has-dict-complete (and ispell-complete-word-dict
                                   (file-exists-p ispell-complete-word-dict)))
           (has-dict-personal (and ispell-personal-dictionary
                                   (file-exists-p ispell-personal-dictionary)))
           (is-dict-outdated
            (and has-dict-complete has-dict-personal
                 (time-less-p
                  (nth 5 (file-attributes ispell-complete-word-dict))
                  (nth 5 (file-attributes ispell-personal-dictionary))))))


      (when (or (not has-dict-complete) is-dict-outdated)
        (with-temp-buffer
          ;; Insert personal dictionary content if available

          (dolist (lang '("en" "pl"))
            (when (insert-file-contents (j/ispell--personal-dictionary-filename lang))
              (goto-char (point-min))
              (when (looking-at "personal_ws\-")
                (delete-region (line-beginning-position) (1+ (line-end-position))))
              (goto-char (point-max))
              (unless (eq ?\n (char-after))
                (insert "\n"))))

          ;; Add aspell master dictionary content
          (call-process "aspell" nil t nil "-d" "en_US" "dump" "master")
          (call-process "sh" nil t nil "-c" "aspell -d pl dump master | aspell -l pl expand | awk '{for(i=1;i<=NF;i++) print $i}'")

          ;; Sort case-insensitively
          (sort-lines nil (point-min) (point-max))
          ;; Write the combined dictionary
          (write-region nil nil ispell-complete-word-dict))))))

(provide 'jspell)
;;; jspell.el ends here
