;;; jasnippet.el --- description -*- lexical-binding: t; -*-

(defun jasnippet-templates-from-group (group)
  "."
  (seq-filter (lambda (template)  (s-equals-p group (car (yas--template-group template))))
              (yas--all-templates (yas--get-snippet-tables))))


(defun jas-insert-snippet-from-group (group &optional no-condition)
  "Choose a snippet to expand, pop-up a list of choices according
to `yas-prompt-functions'.

With prefix argument NO-CONDITION, bypass filtering of snippets
by condition."
  (interactive "P")
  (setq yas--condition-cache-timestamp (current-time))
  (let* ((yas-buffer-local-condition (or (and no-condition
                                              'always)
                                         yas-buffer-local-condition))
         (templates (jasnippet-templates-from-group group))
         (yas--current-template (and templates
                                     (or (and (cl-rest templates) ;; more than one template for same key
                                              (yas--prompt-for-template templates))
                                         (car templates))))
         (where (if (region-active-p)
                    (cons (region-beginning) (region-end))
                  (cons (point) (point)))))
    (if yas--current-template
        (yas-expand-snippet yas--current-template (car where) (cdr where))
      (yas--message 1 "No snippets can be inserted here!"))))



(defun jasnippet-surround(arg)
  "."
  (interactive "p")
                                        ;TODO:bcm  make it load yasnippet ONCE
  ;; https://github.com/hlissner/doom-emacs/commit/60b595321eaf4f698ae129a7f138b7545f7127be
  ;; (yas-global-mode 1)
  (progn
    (if (not (region-active-p))
        (er/expand-region arg) nil )
    (jas-insert-snippet-from-group "surround")
    (keyboard-quit)))

;;Helpers based on doom-emacs:

(defvar +scala-project-package-roots (list "scala/" "test/" "main/" "src/" 1)
  "A list of relative directories (strings) or depths (integer) used by
`+scala-current-package' to delimit the namespace from the current buffer's full
file path. Each root is tried in sequence until one is found.

If a directory is encountered in the file path, everything before it (including
it) will be ignored when converting the path into a namespace.

An integer depth is how many directories to pop off the start of the relative
file path (relative to the project root). e.g.

Say the absolute path is ~/some/project/src/scala/net/lissner/game/MyClass.scala
The project root is ~/some/project
If the depth is 1, the first directory in src/scala/net/lissner/game/MyClass.scala
  is removed: scala.net.lissner.game.
If the depth is 2, the first two directories are removed: net.lissner.game.")
;;
;;;###autoload
(defun +scala-current-package ()

  "From  +java-current-package from doom-emacs

For example: ~/some/project/src/net/lissner/game/MyClass.scala
Is converted to: net.lissner.game

It does this by ignoring everything before the nearest package root (see
`+scala-project-package-roots' to control what this function considers a package
root)."
  (unless (eq major-mode 'scala-mode)
    (user-error "Not in a scala-mode buffer"))
  (let* ((project-root (file-truename (doom-project-root)))
         (file-path (file-name-sans-extension
                     (file-truename (or buffer-file-name
                                        default-directory))))
         (src-root (cl-loop for root in +scala-project-package-roots
                            if (and (stringp root)
                                    (locate-dominating-file file-path root))
                            return (file-name-directory (file-relative-name file-path (expand-file-name root it)))
                            if (and (integerp root)
                                    (> root 0)
                                    (let* ((parts (split-string (file-relative-name file-path project-root) "/"))
                                           (fixed-parts (reverse (nbutlast (reverse parts) root))))
                                      (when fixed-parts
                                        (string-join fixed-parts "/"))))
                            return it)))
    (when src-root
      (string-remove-suffix "." (replace-regexp-in-string "/" "." src-root)))))

;;;###autoload
(defun +scala-current-class ()
  "Get the class name for the current file."
  (unless (eq major-mode 'scala-mode)
    (user-error "Not in a scala-mode buffer"))
  (unless buffer-file-name
    (user-error "This buffer has no filepath; cannot guess its class name"))
  (or (file-name-sans-extension (file-name-base (buffer-file-name)))
      "ClassName"))

(provide 'jasnippet)
;;; jasnippet.el ends here
