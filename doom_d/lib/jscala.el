;;; jscala.el --- description -*- lexical-binding: t; -*-
;;; Commentary:

;;; Code:


;;;;;;; Go to implementation
(defun jscala-ivy-dependencies-from-sbt ()
  "Try to import ivy dependencies from sbt file.
Currently only form like
libraryDependencies += \"com.typesafe.akka\" %% \"akka-actor\" % \"2.5.21\"
is available."
  (interactive)

  (kill-region (region-beginning) (region-end))
  (with-temp-buffer
    (insert (pop kill-ring))
    (goto-char (point-min))
    (let ((regex "\"\\(.+?\\)\"[ ]+%\\{1,2\\}[ ]+\"\\(.+?\\)\"[ ]+%\\{1,2\\}[ ]+\"\\(.+?\\)\"[ ]*,?")
          (res))
      (while (re-search-forward regex nil t)
        (add-to-list
         'res
         (format "import $ivy.`%s::%s:%s`" (match-string 1) (match-string 2) (match-string 3))
         t))
      (message "Copied %S" (s-join "\n" res))
      (kill-new (s-join "\n" res))) ))

(defun jscala--get-method-class-name(arg)
  "."
  (interactive "p")
  (save-mark-and-excursion
    (let ((expand-region-fast-keys-enabled nil))
      (sp-up-sexp)
      (evil-jump-item)
      (re-search-backward "trait\\|class")
      (evil-forward-word-begin)
      (er/expand-region arg)
      (copy-region-as-kill (region-beginning) (region-end))
      (pop kill-ring)
      )
    )
  )


(defun jscala-goto-implementation-rg(arg)
  "."
  (interactive "p")
  (let
      ((parent-name (jscala--get-method-class-name arg) )
       (expand-region-fast-keys-enabled nil))

    (if (not (region-active-p))
        (er/expand-region arg) nil )
    (copy-region-as-kill (region-beginning) (region-end))
    (+vertico-file-search :query (format "override def %s#%s" (pop kill-ring) parent-name) :in ( projectile-project-root ) :all-files arg)))


;;;;;;; Mior macros
;;TODO:bcm - make function / macro for it
(defun jscala-swap-case()
  (interactive)

  (save-excursion
    (if (not (region-active-p))
        (jscala--selcect-in-sexp)
      nil)
    (evil-ex "'<,'>s/case\\s+\\(.*?\\)\\s+=>\\s+\\(.*\\)/case \\2 => \\1" )))


(fset 'jscala-add-def-type
      [?K ?$ ?F ?: ?y ?$ ?q  ?/ ?= ?\[ ?^ ?> ?\] return ?F ?\) right ?? ?\) return ?p ?? ?d ?e ?f ?  return ?w ?w])

(fset 'jscala--widen-macro
      [?? ?s ?e ?a ?l ?e ?d return ?w ?w ?v ?e ?y ?\C-o ?a tab ?d ?e ?f ?  ?w ?i ?d ?e ?n ?: escape ?p ?a ?  ?= ?  ?t ?h ?i ?s escape])

;;TODO rewrite to tree sitter?
(defun jscala-widen()
  "."
  (interactive)
  (if (eq evil-state 'insert)
      (evil-escape))
  (execute-kbd-macro 'jscala--widen-macro))



(defun jscala--get-line-pos(line-number)
  "Get point at LINE-NUMBER without moving cursor."
  (save-excursion
    (goto-line line-number)
    (point)))

;; TODO - make it work with multiline case classes (tree-sitter rework)
(defun jscala-st-extend-cases()
  ""
  (interactive)
  (let*(
        (ala (save-excursion

               (sp-backward-up-sexp)
               (re-search-backward "object +\\([a-zA-Z0-9]+\\)" nil t)
               (match-string 1)))

        (end-line (save-excursion (sp-up-sexp) (line-number-at-pos (point))))
        (replace-str (format "case \\1 extends %s" ala) ))

    (save-excursion
      (sp-backward-up-sexp)
      (while (re-search-forward "case \\(.*\\)" (jscala--get-line-pos end-line) t)
        (if (not  (cl-search "extends" (match-string 1)))
            (replace-match
             replace-str
             'fixedcase
             nil))))))


;; Searches

(defun +scala/search-entity()
  "Search class/trait/object using `counsel-projectile-rg'."
  (interactive)
  ;; HACK for ivy add \\ before "(" and ")"
  (j/default/search-symbol "\\(class\\|trait\\|class\\)\\ "))

(defun +scala/search-???()
  "Search ??? using `counsel-projectile-rg'."
  (interactive)
  (j/default/search-symbol "\\?\\?\\?"))


;;Minor
(defun j/scala-hocon-env->yaml()
  (interactive)
  (evil-ex "'<,'>s/^.*?$\\{\\(\\??\\)\\([A-Z_]+\\)\\}/\\2: \"\\1\"" ))

(defun jscala/field-to-slick-column()
  (interactive)
  (if (not (region-active-p))
      (evil-visual-line) nil )
  (evil-ex "'<,'>s/\\(\\w+\\)\\s*:\\s*\\([^,]+\\),?/def \\1 = column[\\2](\"\\1\")   "))

(defun jscala--selcect-in-sexp()
  "."
  (interactive)
  (let ((beg (save-excursion (sp-backward-up-sexp) (point)))
        (end (save-excursion (sp-up-sexp) (backward-char) (point))))
    (goto-char beg)
    (push-mark end)
    (setq mark-active t)))


(defun jscala/comment-inline(beg end)
  (goto-char end)
  (insert "*/")
  (goto-char beg)
  (insert "/*")
  (goto-char (+ 4 end)))

(defun jscala/comment-dwim(arg) ;; I could integrate it with evil somehow
  (interactive "*P")
  (if (region-active-p)
      (jscala/comment-inline (region-beginning) (region-end))
    (comment-dwim arg)))

(defun +evil--replace-in-selection-expanding(regex)
  "."
  (if (not (region-active-p))
      (jscala--selcect-in-sexp)
    nil)
  (evil-ex regex))


(defun jscala/trim-types-in-parens()
  "."
  (interactive)
  (+evil--replace-in-selection-expanding "'<,'>s/: *[].0-9[a-z]*//g" ))

(defun jscala/remove-default-args-in-definition()
  "."
  (interactive)
  (+evil--replace-in-selection-expanding "'<,'>s/=[^,\)]*//g" ))

(defun jscala/trim-proto-cc-definition()
  "Trims case class definition generated by sclalapb."
  (interactive)
  (+evil--replace-in-selection-expanding "'<,'>s/\\(_root_[^A-Z]*\\(Predef\.\\)?\\)//g" ))


(defun j/scala-disable-scala-flycheck-if-script-h()
  "Disable flycheck scala checkers for scala scripts."
  (when (and flycheck-mode
             (or (s-ends-with? ".sc" buffer-file-name) (s-ends-with? ".sbt" buffer-file-name)))
    (flycheck-disable-checker 'scala)))


(defun jscala-goto-other ()
  "Open corresponding Scala test file in the /test/ directory, creating it if it doesn't exist."
  (interactive)
  (let* ((filename (buffer-file-name))
         (basename (file-name-nondirectory filename))
         (is-test (string-match-p "Test.scala" basename))
         (new-filename (if is-test
                           (s-replace "Test.scala" ".scala" basename)
                         (concat (file-name-sans-extension basename) "Test.scala")))
         (old-path (file-name-directory filename))
         (new-path (if is-test
                       (replace-regexp-in-string "/test/" "/main/" old-path )
                     (replace-regexp-in-string "/main/" "/test/" old-path ))))

    (jscala--goto-open-file new-path new-filename)))

(defun jscala--goto-open-file(path filename)
  (unless (file-exists-p path)
    (make-directory path t))
  (find-file (concat path filename))
  (if (not (file-exists-p (concat path filename)))
      (write-region "" nil (concat path filename) nil)))


(defmacro jscala-refresh-after-toggle (symbol)
  "Add refreshing of metals overlays after invocation of SYMBOL."
  (let ((advice-name (intern (concat (symbol-name symbol) "-refresh-adv"))))
    `(defadvice! ,advice-name ()
       :after ',symbol
       (lsp--remove-overlays 'metals-decoration)
       (lsp-notify "metals/didFocusTextDocument" (lsp--buffer-uri))
       )
    )
  )

;; (jscala-refresh-after-toggle lsp-metals-toggle-show-inferred-type)
;; (jscala-refresh-after-toggle lsp-metals-toggle-show-implicit-arguments)
;; (jscala-refresh-after-toggle lsp-metals-toggle-show-implicit-conversions)
;; (jscala-refresh-after-toggle lsp-metals-toggle-show-super-method-lenses)



(provide 'jscala)
;;; jscala.el ends here
