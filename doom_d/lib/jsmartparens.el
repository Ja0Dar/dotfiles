;;; jsmartparens.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Jakub Darul
;;
;; Author: Jakub Darul <https://github.com/JakDar>
;; Created: February 18, 2022
;; Modified: February 18, 2022
;; Version: 0.0.1
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(defun j/sp-copy-hybrid-sexp()
  (interactive)
  (let((beg (point))
       (end (sp--get-hybrid-sexp-end)))
    (evil-goggles--show-blocking-hint beg end)
    (kill-ring-save beg end)))

(provide 'jsmartparens)
;;; jsmartparens.el ends here
