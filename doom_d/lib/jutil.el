;;; jutil.el --- Description -*- lexical-binding: t; -*-

;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(defun j/util-open-or-create-bug ()
  "Choose a directory in '~/tmp/work/bug' or create one with a file having the same name and '.org' suffix, then open the file."
  (interactive)
  (let* ((parent-dir (expand-file-name "~/tmp/work/bug"))

         (dirs (directory-files parent-dir nil "^[^.]"))
         (name (completing-read "Enter the bug directory name: " dirs ))
         (dir-path (concat parent-dir "/" name))
         (file-path (concat dir-path "/" name ".org")))
    (unless (file-exists-p dir-path)
      (make-directory dir-path t)
      (with-temp-buffer
        (write-region "" nil file-path)
        (find-file file-path)
        (yas-expand-snippet (yas-lookup-snippet "bug" 'org-mode) (point) (point))
        (save-buffer)))
    (find-file file-path)))


(provide 'jutil)
;;; jutil.el ends here
