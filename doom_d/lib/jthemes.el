;;; jthemes.el --- description -*- lexical-binding: t; -*-



(provide 'jthemes)
;;; jthemes.el ends here

(defvar jthemes-chosen-themes '(doom-one modus-operandi) "List of themes we want to cycle between")

(defun jthemes-cycle()
  "Simpler version of spacemacs/cycle-spacemacs-theme."
  (interactive)
  (setq jthemes--cycle-themes
        (or (cdr (memq doom-theme jthemes-chosen-themes))
            jthemes-chosen-themes))

  (disable-theme doom-theme)
  (let ((new-theme (pop jthemes--cycle-themes)))
    (load-theme new-theme t nil)
    (setq doom-theme new-theme)
    (if (eq major-mode 'org-mode)
        (++custom-set-faces)
      )
    ;; TODO  - deduplicate with .config probably
    (when (member "Noto Color Emoji" (font-family-list))
      (set-fontset-font t 'unicode "Noto Color Emoji" nil 'prepend))
    (message "%S" new-theme))
  )
