;;; jemacs.el --- description -*- lexical-binding: t; -*-
;;; Code:

(defun j/load-if-decrypted (filename)
  "Load the file specified by FILENAME if it is decrypted."
  (with-temp-buffer
    (condition-case nil
        (progn
          (insert-file-contents filename)
          (if (string-match-p "[\0-\x08\x0b-\x0c\x0e-\x1f\x80-\xff]" (buffer-string))
              (message "File is encrypted, not loading.")
            (progn
              (load filename)
              (message "File loaded successfully."))))
      (file-error (message "File not found, not loading.")))))

(defun set-region-writeable (begin end)
  "Removes the read-only text property from the marked region."
  ;; See http://stackoverflow.com/questions/7410125
  (interactive "r")
  (let ((modified (buffer-modified-p))
        (inhibit-read-only t))
    (remove-text-properties begin end '(read-only t))
    (set-buffer-modified-p modified)))


(defun j/org-delete-url-from-point ()
  (interactive)
  (when-let  (link-info (assoc :link (org-context)))
    ;; org-context seems to return nil if the current element
    ;; starts at buffer-start or ends at buffer-end
    (delete-region (or (cadr link-info) (point-min))
                   (or (caddr link-info) (point-max)))))

(defun j/embark-org-url-with-title (url &rest args)
  (let ((name (cdr (doom-exec-process "puptitle" url ))))
    (j/org-delete-url-from-point)
    (insert (format "[[%s][%s]]" url name))
    (message "Inserted %s link." name)))


;; TODO - merge with the http one?
(defun j/embark-link-with-tilte()
  (interactive)
  (embark-org-copy-link-target)

  (let* ((link (pop kill-ring))
         (_ (save-window-excursion
              (org-open-link-from-string link)
              (kill-ring-save (line-beginning-position) (line-end-position))))
         (name (pop kill-ring)))

    (j/org-delete-url-from-point)
    (insert (format "[[%s][%s]]" link name))
    (message "Inserted %s link." name)))

(defun j/emacs--replace-in-buf (regex replace)
  (goto-char (point-min))
  (while (re-search-forward regex nil t)
    (replace-match replace nil nil))
  )

(defun j/emacs-trim-in-sexp()
  "Trim newlines in sexp. With prefix arg skips trimming spaces.
Remove last comm in case of scala-mode and '('."
  (interactive)

  (let* (
         (only-newlines current-prefix-arg)
         (beg (save-excursion (sp-backward-up-sexp) (point)))
         (end (save-excursion (sp-up-sexp) (point)))
         (my-sexp (buffer-substring beg end))
         (trimmed (with-temp-buffer
                    (insert my-sexp)
                    ;; Replace newlines
                    (j/emacs--replace-in-buf "\n+" " ")

                    ;;Replace spaces
                    (unless only-newlines
                      (j/emacs--replace-in-buf " +" " ")
                      (j/emacs--replace-in-buf " \\([\),]\\)" "\\1")
                      (j/emacs--replace-in-buf "\\([\(]\\) "  "\\1")
                      (j/emacs--replace-in-buf "\\(\)?\\) \)" "\\1")
                      (j/emacs--replace-in-buf "\( \(" "((")

                      ;; Remove trailing comma
                      (j/emacs--replace-in-buf ",\)" ")")
                      )


                    (buffer-string)
                    ))
         )

    (delete-region beg end)
    (insert trimmed)
    )
  )

(defun +jemacs/single-to-muliline()
  (interactive)
  (if (not (region-active-p))
      (evil-visual-line) nil )
  (evil-ex "'<,'>s/\\((\\|,\\|)\\)/\\1\\n/g" )
  )

(defun +jemacs/unwrap-killing-word-before()
  "Unwrap sexp and kill first Word before it."
  (interactive)
  ;; (save-excursion
  (sp-backward-up-sexp)
  ;; (sp-backward-kill-symbol 1)
  (let ((end (point)))
    (search-backward-regexp "[^a-zA-Z0-9_\.]")
    (forward-char)
    (kill-region (point) end)
    (sp-unwrap-sexp))
  )

(defun +jemacs/passgen()
  (interactive)
  (let* ((output (doom-call-process "keepassxc-cli" "generate" "-s" "-l" "-U" "-n" "-x" "'`\""))
         (pass (cdr output)))
    (insert pass)))


(defun xah-insert-random-string (NUM)
  "Insert a random alphanumerics string of length NUM (defaulting to 20).
Call `universal-argument' before for different count.
URL `http://ergoemacs.org/emacs/elisp_insert_random_number_string.html'."
  (interactive "P")
  (xah--insert-random-string
   NUM
   "@#$%^&*(.)[]<>-_+{};:ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"))


(defun xah-insert-random-hex (NUM)
  "Insert a random alphanumerics string of length NUM (defaulting to 20).
Call `universal-argument' before for different count.
URL `http://ergoemacs.org/emacs/elisp_insert_random_number_string.html'."
  (interactive "P")
  (xah--insert-random-string
   NUM
   "ABCDEF0123456789"))

(defun xah-insert-random-alphanumeric (NUM)
  "Insert a random alphanumerics string of length NUM (defaulting to 20).
Uses only alphanumeric chars..
Call `universal-argument' before for different count.
URL `http://ergoemacs.org/emacs/elisp_insert_random_number_string.html'."
  (interactive "P")
  (xah--insert-random-string
   NUM
   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"))


(defun xah--insert-random-string (NUM charset)
  (let ((baseCount (length charset)))
    (dotimes (_ (if (numberp NUM) (abs NUM) 20))
      (insert (elt charset (random baseCount))))))


(defun +jemacs-format-time-seconds(seconds utc)
  "Format date from epoch `SECONDS'. If `UTC' is t use UTC time zone."
  (format-time-string "%Y-%m-%dT%T"  seconds utc))

(defun +jemacs-format-time-millis(millis utc)
  "Format date from epoch `MILLIS'. If `UTC' is t use UTC time zone."
  (format-time-string "%Y-%m-%dT%T" (round (/ millis 1000))  utc))

(defun +jemacs-format-time(arg &optional time-number utc)
  "Format time string assuming millis.
It fallbacks to seconds if TIME-NUMBER is way in the future uses
active region if no TIME-NUMBER provided."
  (interactive "p")

  (let* (
         (timezone-msg (if utc "UTC" "Local Time Zone"))
         (time  (or time-number
                    (progn (if (not (region-active-p)) (er/expand-region arg))
                           (kill-region (region-beginning) (region-end))
                           (-> kill-ring pop substring-no-properties string-to-number))))
         (year-millis (string-to-number (format-time-string "%Y" time utc)))
         (result (if (< 3000 year-millis )
                     (progn
                       (message "Millis %s" timezone-msg)
                       (+jemacs-format-time-millis time utc))
                   (progn
                     (message "Seconds %s" timezone-msg)
                     (+jemacs-format-time-seconds time utc)))))

    (if time-number result (insert result))))

(defun +jemacs-format-time-utc(arg &optional time-number)
  "Format time string assume millis.
It fallbacks to seconds if TIME-NUMBER is way in the future uses
active region if no `TIME-NUMBER' provided."
  (interactive "p")
  (+jemacs-format-time arg time-number t))

;; https://discordapp.com/channels/406534637242810369/406627025030348820/780891252266500106
(defun j/screenshot-svg ()
  "Make SVG screenshot of Emacs."
  (interactive)
  (let* ((filename (make-temp-file "Emacs" nil ".svg"))
         (data (x-export-frames nil 'svg)))
    (with-temp-file filename
      (insert data))
    (kill-new filename)
    (message filename)))


(defun j/current-file-link(link-type)
  "Get link to file in dotfiles. Quick and dirty.
`LINK-TYPE' is one of strings bellow. Depends on
`doom-call-process' and git_web script."
  (interactive (list (completing-read "Link type: " '(
                                                      "Branch"
                                                      "Branch + Raw"
                                                      "Commit"
                                                      "Commit + Line"
                                                      "Commit + Raw"
                                                      ))))
  (message "%S" (line-number-at-pos))
  (cl-destructuring-bind (branch raw line)
      (pcase link-type
        ("Branch" (list t nil nil))
        ("Branch + Raw" (list t t nil))
        ("Commit" (list nil nil nil))
        ("Commit + Line" (list nil nil t))
        ("Commit + Raw" (list nil t nil))
        )
    (require 's)
    (let* (
           (baseurl ( cdr (doom-exec-process "git_web" "--print")))
           (filename (file-relative-name (buffer-file-name) (vc-root-dir)))
           (revision (cl-destructuring-bind (_ . output)
                         (if branch
                             (doom-call-process "git" "symbolic-ref" "-q" "--short" "HEAD")
                           (doom-call-process "git" "rev-parse" "HEAD")
                           )
                       output ;TODO: use status and make it safe?
                       ))
           (url (cond
                 ((s-starts-with? "https://bitbucket" baseurl)
                  (concat
                   baseurl
                   (if raw "/raw" "/src")
                   "/" revision "/" filename
                   (if line (format "#lines-%s" (line-number-at-pos)) "")))
                 ((s-starts-with? "https://github.com" baseurl)
                  (concat
                   (if raw (s-replace "https://github.com/" "https://raw.githubusercontent.com/" baseurl) (concat baseurl "/blob"))
                   "/" revision "/" filename
                   (if line (format "#L%S" (line-number-at-pos)) "")))
                 ((s-contains? "gitlab" baseurl)
                  (concat
                   (concat  baseurl (if raw "/-/raw" "/-/blob"))
                   "/" revision "/" filename
                   (if line (format "#L%S" (line-number-at-pos)) "")))
                 (t nil)
                 ))
           )

      (if url
          (progn
            (message "Copied file link: %s" url)
            (kill-new url))
        (message "Origins other than github & bitbucket not supported."))
      )))


(defun j/save-all-if-not-popup ()
  "Save unsaved buffers.
Added exception for *Calendar* as otherwise
I would get file saves before `org-time-stamp' which
would (because something) remove whitespace which would make
inserting timestamp delete a whitespace before it."
  (interactive)
  ;; Save all modified buffers with no prompts

  (require 's)
  (unless (or
           (and (boundp 'popup-p) (popup-p (current-buffer)))
           (s-equals-p (buffer-name) "*Calendar*"))
    (save-some-buffers t)))


(defun j/robomongo-to-json-buffer()
  "Convert robomongo output to json in a buffer."
  (save-excursion
    (j/emacs--replace-in-buf "/\\* 1 \\*/" "")
    (j/emacs--replace-in-buf "/\\*.*?\\*/" ",")
    (j/emacs--replace-in-buf "^}$" "},")
    (j/emacs--replace-in-buf "ISODate(\"\\([^\"]+\\)\")" "\"\\1\"")
    (j/emacs--replace-in-buf "ObjectId(\"\\([^\"]+\\)\")" "\"\\1\"")
    (j/emacs--replace-in-buf "NumberLong(\\([0-9]+\\))" "\\1")
    (j/emacs--replace-in-buf "NumberInt(\\([0-9]+\\))" "\\1")
    (j/emacs--replace-in-buf "NumberDecimal(\"\\([^\"]+\\)\")" "\"\\1\"")
    (j/emacs--replace-in-buf "BinData(0,\"\\([^\"]+\\)\")" "\"\\1\"")
    ;; Didn't test those dates
    (j/emacs--replace-in-buf "new Date(\\([0-9]+\\))" "\"\\1\"")
    (j/emacs--replace-in-buf "new Date(\"\\([^\"]+\\)\")" "\"\\1\"")
    (j/emacs--replace-in-buf "new Date(\\([0-9]+\\))" "\"\\1\"")
    (goto-char (point-min))
    (insert "[")
    (goto-char (point-max))
    (insert "]"))
  (unless (eq major-mode 'json-mode)
    (json-mode))
  (+format/region-or-buffer))


(defun j/robomongo-to-json-region()
  "Like `j/robomongo-to-json-in-buffer' but for region."
  (let ((region (buffer-substring-no-properties (region-beginning) (region-end))))
    (with-temp-buffer
      (insert region)
      (j/robomongo-to-json-buffer)
      (setq region (buffer-string)))
    (delete-region (region-beginning) (region-end))
    (insert region)))

(defun j/robomongo-to-json-dwim()
  "Convert robomongo output to json.
If region is active, convert region, otherwise convert whole buffer."
  (interactive)
  (if (use-region-p)
      (j/robomongo-to-json-region)
    (j/robomongo-to-json-buffer)))

(defun j/kill-current-filename()
  (interactive)
  (let ((filename (buffer-file-name)))
    (if filename
        (progn
          (kill-new filename)
          (message "Filename copied to kill-ring: %s" filename))
      (message "Buffer is not visiting a file."))))

(provide 'jemacs)
;;; jemacs.el ends here
