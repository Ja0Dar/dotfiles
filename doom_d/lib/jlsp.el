;;; jlsp.el --- description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021
;;
;; Created: January 15, 2021
;; Modified: January 15, 2021
;; Version: 0.0.1
;; Package-Requires: ((emacs "29.1") (dash "2.17.0"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Code:



(defun j/lsp-select-log-level()
  (interactive)
  (let* ((level-string   (completing-read "Lsp log level" '("ERROR" "WARN" "INFO" "DEBUG")))
         (severity  (pcase level-string
                      (`"ERROR" 1)
                      (`"WARN" 2)
                      (`"INFO" 3)
                      (`"DEBUG" 5))))
    (when severity
      (setq lsp-treemacs-error-list-severity severity))
    (call-interactively 'lsp-treemacs-errors-list)))

(defun j/flycheck-yank-error-at-point ()
  "Yank explaination to error at point."

  (interactive)
  (-when-let* ((first-error
                ;; Get the first error at point that has an `error-explainer'.
                (seq-find (lambda (error)
                            (flycheck-checker-get
                             (flycheck-error-checker error) 'error-explainer))
                          (flycheck-overlay-errors-at (point))))
               (explainer
                (flycheck-checker-get (flycheck-error-checker first-error)
                                      'error-explainer))
               (explanation (funcall explainer first-error)))
    (kill-new explanation)
    (message "Copied explaination.")))


(defun j/lsp-remove-other-sessions ()
  "Stolen from porcupine :)."
  (interactive)
  (-each
      (-remove-item
       (lsp-find-session-folder (lsp-session) default-directory)
       (lsp-session-folders (lsp-session)))
    #'lsp-workspace-folders-remove))



(defun j/treesit-node-string (node)
  "Return string for NODE."
  (buffer-substring-no-properties (treesit-node-start node) (treesit-node-end node)))

;; thanks to Porcupine96
(defun +lsp/fill-signature--clean ()
  "TODO: tree-sitter it "
  (let* ((beg (save-excursion (backward-up-list) (right-char) (point)))
         (end (save-excursion (up-list) (left-char) (point))))
    (kill-region beg end)))

(defun +lsp/fill-signature--param-value (name lookup)
  (s-concat
   name
   " = "
   (or (nth 1 (assoc name lookup)) "???")
   ","))


(defun j/scala-ts--current-params-lookup()
  (interactive)


  (let* (
         (call-node (treesit-node-at (point) 'scala 'call_expression))
         (_ (setq test-call call-node))
         (current-node (treesit-node-child call-node 0)) ;; arguments
         (children nil))


    (while (treesit-node-next-sibling current-node)
      (setq current-node (treesit-node-next-sibling current-node))

      (if-let ((_ (equal "assignment_expression" (treesit-node-type current-node)))
               ;; assignment_expression is "key = value" when "=" is a node (1) we skip
               (key (treesit-node-child current-node 0))
               (value (treesit-node-child current-node 2)))

          (setq children (append (list (list (j/treesit-node-string key) (j/treesit-node-string value) )) children))))

    children))


(defun +lsp/fill-signature--handle (signature)
  (let* ((signatures (gethash "signatures" signature))
         (signature (elt signatures 0))
         (params (gethash "parameters" signature))
         (labels (mapcar (lambda (p) (gethash "label" p)) params))
         (label-names (mapcar (lambda (l) (s-replace "<" "" (car (s-split ":" l)))) labels))
         (lookup (j/scala-ts--current-params-lookup)))

    (+lsp/fill-signature--clean)

    (insert "\n")
                                        ;REVIEW: could be better indented
    (mapcar (lambda (n) (progn (scala-indent:indent-line) (insert  (+lsp/fill-signature--param-value n lookup) "\n"))) label-names)))


                                        ;FIXME:parse lsp error message to get missing names?
(defun j/scala-fill-signature ()
  (interactive)
  (if (and lsp--signature-last-buffer
           (not (equal (current-buffer) lsp--signature-last-buffer)))
      (lsp-signature-stop)
    (lsp-request-async "textDocument/signatureHelp"
                       (lsp--text-document-position-params)
                       #'+lsp/fill-signature--handle
                       :cancel-token :signature)))

(provide 'jlsp)
;;; jlsp.el ends here
