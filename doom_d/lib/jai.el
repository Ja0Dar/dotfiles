;;; jai.el --- Description -*- lexical-binding: t; -*-

;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(defun j/ai--get-auth-secret (host)
  "Get secret from auth-source for specified HOST."
  (let* ((authinfo (auth-source-search :host host))
         (raw-secret (plist-get (car authinfo) :secret)))
    (if (functionp raw-secret)
        (funcall raw-secret)
      raw-secret)))

(defun j/ai-read-ai-keys-from-authinfo()
  "Read the OpenAI API key from authinfo."
  (interactive)
  (require 'auth-source)

  (setq gptel-api-key (j/ai--get-auth-secret "api.openai.com"))
  (setq robby-openai-api-key gptel-api-key)
  (message "OpenAI API key set")


  (setq brave-search-api-key (j/ai--get-auth-secret "api.search.brave.com/res/v1/"))
  (setq robby-openai-api-key gptel-api-key)
  (message "OpenAI API key set")

  (setq gptel-anthropic-key (j/ai--get-auth-secret "anthropic"))
  (setq gptel-gemini-key (j/ai--get-auth-secret "gemini"))
  (message "Anthropic API key set"))

(defun j/ai-ensure-api-key()
  (unless (bound-and-true-p gptel-api-key)
    (j/ai-read-ai-keys-from-authinfo)))

(provide 'jai)
;;; jai.el ends here
