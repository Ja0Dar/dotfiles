;;; jelfeed.el --- Elfeed improvements -*- lexical-binding: t; -*-

;;; Commentary:
;; Taken from https://github.com/algernon/elfeed-goodies/issues/15

;;; Code:
(defun search-header/draw-wide (separator-left separator-right search-filter stats db-time)
  (let* ((update (format-time-string "%Y-%m-%d %H:%M:%S %z" db-time))
         (lhs (list
               (powerline-raw (-pad-string-to "Feed" (- elfeed-goodies/feed-source-column-width 4)) 'powerline-active1 'l)
               (funcall separator-left 'powerline-active1 'powerline-active2)
               (powerline-raw (-pad-string-to "Date" 5) 'powerline-active2 'l) ;; NOTE Eyeballing it
               (funcall separator-left 'powerline-active2 'powerline-active0)
               (powerline-raw (-pad-string-to "Tags" (- elfeed-goodies/tag-column-width 6)) 'powerline-active0 'l)
               (funcall separator-left 'powerline-active0 'mode-line)
               (powerline-raw "Subject" 'mode-line 'l)))
         (rhs (search-header/rhs separator-left separator-right search-filter stats update)))

    (concat (powerline-render lhs)
            (powerline-fill 'mode-line (powerline-width rhs))
            (powerline-render rhs))))

(defun elfeed-goodies/entry-line-draw (entry)
  "Print ENTRY to the buffer."
  (let* ((title (or (elfeed-meta entry :title) (elfeed-entry-title entry) ""))
         (date (elfeed-search-format-date (elfeed-entry-date entry)))
         (title-faces (elfeed-search--faces (elfeed-entry-tags entry)))
         (feed (elfeed-entry-feed entry))
         (feed-title
          (when feed
            (or (elfeed-meta feed :title) (elfeed-feed-title feed))))
         (tags (mapcar #'symbol-name (elfeed-entry-tags entry)))
         (tags-str (concat "[" (mapconcat 'identity tags ",") "]"))
         (title-width (- (window-width) elfeed-goodies/feed-source-column-width
                         elfeed-goodies/tag-column-width 4))
         (tag-column (elfeed-format-column
                      tags-str (elfeed-clamp (length tags-str)
                                             elfeed-goodies/tag-column-width
                                             elfeed-goodies/tag-column-width)
                      :left))
         (feed-column (elfeed-format-column
                       feed-title (elfeed-clamp elfeed-goodies/feed-source-column-width
                                                elfeed-goodies/feed-source-column-width
                                                elfeed-goodies/feed-source-column-width)
                       :left)))

    (if (>= (window-width) (* (frame-width) elfeed-goodies/wide-threshold))
        (progn
          (insert (propertize feed-column 'face 'elfeed-search-feed-face) " ")
          (insert (propertize date 'face 'elfeed-search-date-face) " ")
          (insert (propertize tag-column 'face 'elfeed-search-tag-face) " ")
          (insert (propertize title 'face title-faces 'kbd-help title))
          )
      (insert (propertize title 'face title-faces 'kbd-help title)))))


(defun j/elfeed-org-export-opml-to-file (output-file)
  "Export Org feeds under `rmh-elfeed-org-files' to an OPML file.
Argument OUTPUT-FILE is the path where the OPML file will be saved."
  (interactive "FOutput OPML file: ")
  (let ((opml-body (cl-loop for org-file in rmh-elfeed-org-files
                            concat
                            (with-temp-buffer
                              (insert-file-contents
                               (expand-file-name org-file org-directory))
                              (rmh-elfeed-org-convert-org-to-opml
                               (current-buffer))))))
    (with-temp-file output-file
      (insert "<?xml version=\"1.0\"?>\n")
      (insert "<opml version=\"1.0\">\n")
      (insert "  <head>\n")
      (insert "    <title>Elfeed-Org Export</title>\n")
      (insert "  </head>\n")
      (insert "  <body>\n")
      (insert opml-body)
      (insert "  </body>\n")
      (insert "</opml>\n"))
    (message "OPML exported to %s" output-file)))



(provide 'jelfeed)
;;; jelfeed.el ends here
