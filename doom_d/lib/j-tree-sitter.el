;;; +ts-scala.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022
;;
;; Author:  <https://github.com/JakDar>
;; Created: February 13, 2022
;; Modified: February 13, 2022
;; Package-Requires: ((emacs "26.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;; init

(require 'treesit)
(defconst j-tree-sitter-initialised nil "Is this initialised?")

(defun j-tree-sitter-init()
  (require 'treesit-langs)
  (setq treesit-extra-load-path (list "/home/owner/.nix-profile" treesit-langs-grammar-dir)
        treesit-load-name-override-list nil ;;'((yaml "yaml" nil)) ;TODO:bcm
        j-tree-sitter-initialised t))

;; HACK FIXME CLEANUP and everything
(unless j-tree-sitter-initialised
  (j-tree-sitter-init))

(defun j/treesit-parent-node-of-type (node type)
  (while (and node (not (s-equals? (treesit-node-type node) type)))
    (setq node (treesit-node-parent node)))
  node)

(defun jscala-delete-implementation()
  "Function -declaration into function definition"
  (interactive)

  (if-let* ((random_node (treesit-node-at (point) 'scala))
            (fun-node (j/treesit-parent-node-of-type random_node "function_definition")))
      (let* (
             (name-node (treesit-node-child-by-field-name fun-node "name"))
             (return-type-node (treesit-node-child-by-field-name fun-node "return_type"))
             ;; FIXME it wont work for multiple argument lists :/
             (parameters-node (treesit-node-child-by-field-name fun-node "parameters"))
             (body-node (treesit-node-child-by-field-name fun-node "body"))
             (fun-end-node (or return-type-node parameters-node name-node)))



        ;; Have to insert newline to mitigate stupid bug (killing too much newlines)
        ;; I dont have time to handle
        (let ((s (treesit-node-end fun-end-node))
              (e (treesit-node-end body-node)))
          (save-excursion (goto-char (treesit-node-end body-node))
                          (insert "\n"))
          (kill-region s e )))
    (message "Cannot find function_definition at point.")))


;; (j/treesit-hocon-ts-node-is-kv-key (treesit-node-at (point) 'hocon 'unquoted_path))
(defun j/treesit-hocon-ts-node-is-kv-key (node)
  (if-let* ((path (treesit-node-parent node))
            (pair (treesit-node-parent path))
            (_ (s-equals? (treesit-node-type pair) "pair"))
            (value (treesit-node-child pair 1))
            (_ (not (s-equals? (treesit-node-type (treesit-node-child value 0)) "object"))))
      t))

(defun jhocon-add-env-for-node-at-point()
  "Function -declaration into function definition"
  (interactive)

  (require 'dash)
  (require 's)
  (require 'string-inflection)

  (let ((acc nil))
    (if-let* ((current-node (treesit-node-at (point) 'hocon 'unquoted_path))
              (_ (j/treesit-hocon-ts-node-is-kv-key current-node))
              (field-name (s-trim
                           (buffer-substring-no-properties
                            (treesit-node-start current-node)
                            (treesit-node-end current-node)))))


        (progn
          (while current-node
            (if-let ((_ (s-equals? (treesit-node-type current-node) "pair"))
                     (path (treesit-node-child current-node 0))
                     (beg (treesit-node-start path))
                     (end (treesit-node-end path)))
                (setq acc (append (list (s-trim (buffer-substring-no-properties beg end))) acc)))

            (setq current-node (treesit-node-parent current-node)))

          (let ((env-name (->> acc
                               (-map #'string-inflection-upcase-function)
                               (s-join "_")
                               (s-replace "." "_")))
                (indentation-str (make-string (current-indentation) ? )))

            (save-excursion
              (end-of-line)
              (insert (format "\n%s%s = ${?%s}" indentation-str field-name env-name)))
            (next-line)))
      (message "Not a hocon key."))))



;; FIXME - rebind for scala to "f"
;; (define-key evil-inner-text-objects-map "z" (evil-textobj-tree-sitter-get-textobj "function"
;;                                               '((scala-mode . [(function_definition) @function]))))

(provide 'j-tree-sitter)
;;; +ts-scala.el ends here
