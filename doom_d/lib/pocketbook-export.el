;;; pocketbook-export.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022
;;
;; Created: June 12, 2022
;; Modified: June 12, 2022
;; Version: 0.0.1
;; Package-Requires: ((emacs "25.1") (dash "2.17.0") (org "9.6"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description

;; To export run calibre and go to /run/media/$USER/*/system/config/books.db (at least for me)
;;
;;; Code:


(defvar pocketbook-export-sql
  "SELECT
    Title,
    Authors,
    Pos,
    Highlight,
    Note
FROM
    Books
    INNER JOIN (
        SELECT
            OID AS BookID,
            Pos,
            Highlight,
            Note
        FROM
            Items
            INNER JOIN (
                SELECT
                    ParentID,
                    Highlight,
                    Note,
                    Pos
                FROM
                    Items
                    INNER JOIN (
                        SELECT
                            tags_hl.ItemID,
                            tags_hl.Val ->> \'text\' AS Highlight,
                            tags_notes.Val ->> \'text\' AS Note,
                            tags_hl.Val ->> \'begin\'  AS Pos
                        FROM
                            Tags tags_hl
                            LEFT JOIN Tags tags_notes ON tags_hl.ItemID = tags_notes.ItemID
                                AND tags_hl.TagId = 104
                                AND tags_notes.TagId = 105
                        WHERE
                            json_valid (tags_hl.Val)
                            AND tags_hl.Val ->> \'text\' <> \'Bookmark\') AS Highlights ON Highlights.ItemID = OID) AS Highlights ON Highlights.ParentID = OID) AS Highlights ON BookID = OID
      WHERE (Highlight is not null or Note is not null)
")


;; It is kinda broken - works only for some cases
(defun pocketbook-export--pbr-to-nov-page (pbr)
  (string-match "epubcfi(/./\\([0-9]+\\)" pbr)
  (/
   (string-to-number
    (match-string  1 pbr)) 2))

(defun pocketbook-export--nov-link(book-file page title)
  (format "[[nov:%s::%d:1][%s]]" book-file page title))




(defun pocketbook-export (&optional db-name destination-file epub-name)
  "."
  (interactive)
  (require 'dired-x)
  (let* ((db (sqlite-open (or db-name (dired-x-read-filename-at-point "Database file "))))
         (result (sqlite-select db pocketbook-export-sql))
         (_ (sqlite-close db))
         (annot-by-book (-group-by (-partial 'nth 0) result))
         (selected-book (completing-read "Which book? " (-map (-partial 'nth 0) annot-by-book)))
         (epub-file (or epub-name (dired-x-read-filename-at-point "Epub file ")))
         (notes (alist-get selected-book annot-by-book nil nil 's-equals?))
         (author (nth 1 (nth 0 notes)))
         (file-name (or destination-file (dired-x-read-filename-at-point "Export org file ")))
         (export-buffer (or (find-buffer-visiting file-name)
                            (find-file file-name)))
         (i 0))


    (with-current-buffer export-buffer
      (delete-region (point-min) (point-max))


      (insert "#+TITLE: " selected-book "\n")
      (insert "#+Author: " author "\n")

      (org-insert-heading nil)
      (insert selected-book "\n")

      (cl-loop for notes-line in notes do
               (let ((pbr (nth 2 notes-line))
                     (highlight (nth 3 notes-line))
                     (note (nth 4 notes-line)))

                 (setq i (+ i 1))
                 (org-insert-heading nil nil t)
                 (org-do-demote)
                 (insert (format "TODO Insight %i\n" i))
                 (if-let* ((_ pbr)
                           (page (pocketbook-export--pbr-to-nov-page pbr))
                           (link (pocketbook-export--nov-link epub-file page "Link")))
                     (insert link "\n"))
                 (org-insert-subheading nil)
                 (insert "Highlight\n")
                 (insert "\n" highlight "\n")
                 (when note
                   (org-insert-heading nil)
                   (insert "Note\n")
                   (insert "\n" note "\n")))))))

(provide 'pocketbook-export)
;;; pocketbook-export.el ends here
