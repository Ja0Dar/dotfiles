;;; jmagit.el --- description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2020
;;
;; Created: July 28, 2020
;; Modified: July 28, 2020
;; Version: 0.0.1
;; Keywords:
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  description
;;
;;; Code:

(defun j/magit-forge-copy-topic-url-at-point()
  (interactive)
  (let ((url (forge-get-url (forge-topic-at-point))))
    (kill-new url)
    (message "Copied %s" url)))

(defun jmagit-forge--format-user-from-field(pr field-symbol)
  (s-join ", " (mapcar (lambda (n) (nth 1 n)) (eieio-oref pr field-symbol))))

(defun jamgit-forge-extract-task(text)
  (string-match  "\\([A-Z][A-Z][A-Z][A-Z]?-[0-9]+\\)" text)
  (match-string 1 text))



(defun jmagit-forge-extract-name-from-pr-url(url)
  (if-let* ((split-url (reverse (s-split "/" url)))
            (pr-id (nth  0 split-url))
            (repo (nth 2 split-url)))
      (format "%s #%s" repo pr-id)))

;; TODO get name of author if no assignee
(defun jmagit-format-pr (pr)
  "Format PR and return string for it."
  (with-slots (id author title head-ref) pr

    ;; TODO: fix -> reviewws not showing, why?
    (let* ((reviewers (jmagit-forge--format-user-from-field pr 'review-requests))
           (assignees (jmagit-forge--format-user-from-field pr 'assignees))
           (assignees (if (s-equals? assignees "") author assignees))
           (qa "???")
           (pr-link (forge-get-url pr))
           (pr-name (jmagit-forge-extract-name-from-pr-url pr-link))
           (jira-id (jamgit-forge-extract-task (or head-ref title)))
           (jira-url (or (and (boundp 'jiralib2-url) jiralib2-url) (and (boundp 'jiralib-url) jiralib-url)))
           (jira-link (s-concat jira-url "/browse/" jira-id))
           (jira-md-link (if jira-id (format "[%s](%s)" jira-id jira-link)
                           "-")))

      (format "
*Deployment*: _%s_
author:      %s
review:      %s
QA:            %s
PR:             [%s](%s)
JIRA:          %s
User facing:   NO
services:      ()
" title assignees reviewers qa pr-name pr-link jira-md-link))))


(defun jmagit-format-and-copy-pr()
  (interactive)
  (message "Copied PR summary.")
  (kill-new (jmagit-format-pr (forge-pullreq-at-point))))



(provide 'jmagit)
;;; jmagit.el ends here
