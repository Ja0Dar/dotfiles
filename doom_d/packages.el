;;; ~/.doom.d/packages.el

;;; Code:

;; Grammar
(package! writegood-mode :disable t)


;;docker
(package! docker :disable t)

;;web
(package! haml-mode :disable t)
(package! pug-mode :disable t)
(package! slim-mode :disable t)
(package! sass-mode :disable t)
(package! sws-mode :disable t)
(package! sws-mode :disable t)

(package! nginx-mode)

;; cc
(package! cuda-mode :disable t)
(package! opencl-mode :disable t)
(package! glsl-mode :disable t)

;;magit
(package! magit-gitflow :disable t)

;; Org
(package! org-appear :recipe (:host github :repo "awth13/org-appear") :pin "32ee50f8fdfa449bbc235617549c1bccb503cb09")
(package! org-modern :pin "ab52b6d728a5123b4a5aa55fe3340cf42981a362")
(package! org-alert :disable j/doom-lite)
(package! org-gtd :recipe (:host github :repo "Trevoke/org-gtd.el")  :pin "f82eb971db0008b773a57c207120751f913bde6b")
(package! org-ql :pin "98c62ab0a6c084ae4132110e24d9fe1ace91d363")

(package! nov :pin "bbb5c60bfd6b09cffe0406a56930105335f07887" :disable j/doom-lite)

(package! org-src-emph :recipe (:host github :repo "TobiasZawada/org-src-emph" ) :pin "27f1a1133e5cf770e70d90abe43a1f92f148d762")
(package! org-super-agenda)
(package! websocket)
(package! org-roam-ui :recipe (:host github :repo "org-roam/org-roam-ui" :files ("*.el" "out")) :pin "5ac74960231db0bf7783c2ba7a19a60f582e91ab")

;; (package! org-pandoc-import
;;   :recipe (:host github
;;            :repo "tecosaur/org-pandoc-import"
;;            :files ("*.el" "filters" "preprocessors")) :disable j/doom-lite)

(package! org-excalidraw :recipe (:host github :repo "wdavew/org-excalidraw" :build (:not compile)) :pin "9750463dfda28b9ca70df048761c131aa94d6c12")

;; graphql bloat 🤷‍♂️
(package! ob-graphql :disable t)
(package! graphql-doc :disable t)

;; Neo4j
;; (package! ob-cypher)
;; (package! cypher-mode)

(package! rescript-mode :pin "b9dda43cc52cd4a9b384c59fb0cc7a11f0bad230")
(package! lsp-rescript :pin "7baf9adf10234cf964feefae99050268e9bc5681")

;;Data
;; NOTE: protobuf-mode doesn't change much. The repo itself - does a lot, we don't need to upgrade it always.
(package! protobuf-mode :pin "65a9ffe93b75c17f2cd7e3adb26fac6130c8f8c4")
(package! yaml-pro :disable j/doom-lite)

;; Misc
(package! evil-string-inflection)
(package! fish-mode)
(package! drag-stuff)
(package! rotate-text :recipe (:host github :repo "JakDar/rotate-text.el" :branch "feat/rotate-parens"))

;;Calendar
;; NOTE Original repo - kiwanami/emacs-calfw is inactive since 2018. This fork fixes a bug with displaying multiday events.
(package! calfw :recipe (:host github :repo "MaximeWack/emacs-calfw" :branch "fix_org-get-timerange" :files ("calfw.el")) :pin "6c75fd3acb47f634d0b2c3c591e659c5bae448cc")
(package! calfw-org :recipe (:host github :repo "MaximeWack/emacs-calfw" :branch "fix_org-get-timerange" :files ("calfw-org.el")) :pin "6c75fd3acb47f634d0b2c3c591e659c5bae448cc")
(package! calfw-cal  :disable t)
(package! calfw-ical  :disable t)

(package! org-gcal :disable t)
(package! org-caldav)
(package! org-contacts)

;; Dired -related
(package! dired-du)

(package! ox-gfm) ;; for emacs-everywhere
(package! ox-slack)

;;JIRA
;; (package! ox-jira :recipe (:host github :repo "stig/ox-jira.el" :branch "trunk") :pin "00184f8fdef02a3a359a253712e8769cbfbea3ba")
;; (package! jiralib2 :recipe (:host github :repo "JakDar/jiralib2" :branch "master") :disable j/doom-lite)
;; (package! ejira :recipe (:host github :repo "JakDar/ejira" :branch "main" :build (:not compile)) :disable j/doom-lite)
;; (package! org-jira :recipe (:host github :repo "ahungry/org-jira") :pin "bd573584a9d93b320d5adbfc09dee30e83e5120d" :disable j/doom-lite)

(package! langtool :pin "6605f26f56b7da1499a32885236c02a40b0b0e31")

(package! kbd-mode :recipe (:host github :repo "kmonad/kbd-mode" :branch "master") :pin "f8951b2efc5c29954b0105a9e57e973515125b0d" :disable j/doom-lite)
(package! nushell-mode :recipe (:host github :repo "azzamsa/emacs-nushell") :pin "c179c3cf573b2cc9421dc08bf47e2d619c7791ee" :disable j/doom-lite)

;; Let's try to use treesit-langs as they follow naming scheme required by treesit.
;; As of Apr 16 2023, install (`treesit-langs-install-grammars') version 0.12.40 to get all the languages.
(package! treesit-langs :recipe (:repo "kiennq/treesit-langs" :host github ) :pin "f0b58f01a924bc498e1f6ee0c87c13116bbfdc87" :disable (not (fboundp 'treesit-available-p)))

(package! mermaid-mode :pin "e74d4da7612c7a88e07f9dd3369e3b9fd36f396c")
(package! ob-mermaid :disable j/doom-lite)

(package! ox-ipynb :recipe (:host github :repo "jkitchin/ox-ipynb") :pin "ef9ba544230f9a30c17b172b566d9756cb74b608")

;; AI
(package! copilot :recipe (:host github :repo "copilot-emacs/copilot.el" :files ("*.el" "dist")) :pin "7d105d708a23d16cdfd5240500be8bb02f95a46e" :disable j/doom-lite)
(package! copilot-chat :recipe (:host github :repo "chep/copilot-chat.el" :files ("*.el")) :pin "8a00deeeea9fdd897d3e31840c03a778eb775c7f")
(package! aider :recipe (:host github :repo "tninja/aider.el" :files ("aider.el" "aider-core.el" "aider-file.el" "aider-code-change.el" "aider-discussion.el" "aider-prompt-mode.el" "aider-doom.el")))
(package! gptel :recipe (:host github :repo "karthink/gptel" ) :pin "2a6f714d307c403684e81a1c4c0e521d7c4d12d8")
(package! org-ai :recipe (:host github :repo "rksm/org-ai") :pin "5a906fd4ecc4ff4d8ad561da14346a9d8b1d17db")
;; (package! robby :recipe (:host github :repo "stevemolitor/robby" :files ("*.el" "README.org") :build (:not compile)) :pin "34cc41331f628685df0bb48983bbfd8531e1127f")


(package! lsp-metals :recipe (:host github :repo "emacs-lsp/lsp-metals" ) :pin "345b4fa80e31c58fd14e4c0cf9b88eb2aededcb0")
(package! difftastic :pin "b7836169fadf22a2a32f055884dfdd1062e4cb51")

(package! code-compass :recipe (:host github :repo "ag91/code-compass") :pin "6b741978c83f0359c7e555ab78708eed6ced8486")
;; (package! scala-cli-repl :recipe (:host github :repo "ag91/scala-cli-repl") :pin "a6e91851d8617ab340b5cc8bdbd7b93fd89a2316")
;; (package! keycast :pin "c44618d2867fc2410e5061fef2a805e974198cf2")

;;; packages.el ends here
