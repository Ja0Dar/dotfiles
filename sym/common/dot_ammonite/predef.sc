//Scala things
import $ivy.`org.typelevel::cats-core:2.6.1`
import $ivy.`org.typelevel::cats-effect:2.5.4`
import $ivy.`co.fs2::fs2-core:2.5.10`
import cats._
import cats.effect._

// Import $ivy.`org.typelevel::mouse:0.26`,mouse.all._ # pipe already in AmmoniteOps pipeable
// https://www.lihaoyi.com/upickle/#uJson
// import $ivy.`com.lihaoyi::upickle:0.9.5`
// import $ivy.`com.lihaoyi::ujson:0.9.5`
// https://www.lihaoyi.com/PPrint/
// import $ivy.`com.lihaoyi::pprint:0.5.6`,pprint.pprintln
// import $ivy.`org.http4s::http4s-circe:1.0.0-M4`
// import $ivy.`org.http4s::http4s-blaze-client:1.0.0-M4`

import language.postfixOps
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

implicit val cs:ContextShift[IO] = IO.contextShift(global)
implicit val timer: Timer[IO] = IO.timer(global)

//Time
import java.time._
import java.util.Date

object TimeUtils {
  var zoneId = ZoneId.systemDefault()

  def dateToZoned: Date => ZonedDateTime = date => ZonedDateTime.ofInstant(date.toInstant, zoneId)

  def zonedToDate: ZonedDateTime => Date = zonedDateTime => Date.from(zonedDateTime.toInstant)

  def zonedToMillis: ZonedDateTime => Long = zonedDateTime => zonedDateTime.toInstant.toEpochMilli

  def millisToZoned: Long => ZonedDateTime = millis => ZonedDateTime.ofInstant(Instant.ofEpochMilli(millis), zoneId)

  def zonedToSeconds: ZonedDateTime => Long = zonedDateTime => zonedDateTime.toInstant.getEpochSecond

  def secondsToZoned: Long => ZonedDateTime = seconds => ZonedDateTime.ofInstant(Instant.ofEpochSecond(seconds), zoneId)

  def now() = ZonedDateTime.now()
}

val Tu = TimeUtils



//Hasher
object DefaultVersions{
  val Hasher = "1.2.2"
  val BCrypt = "0.4" //TODO:bcm - undo?
}

def loadCirce() = {


  val circeVersion = "0.13.0"
  interp.load.ivy(
    "io.circe" %% "circe-generic" % circeVersion,
    "io.circe" %% "circe-parser"  % circeVersion
  )
}

def hasherLib(version: String) =  "com.outr" %% "hasher" % version
def bcryptLib(version:String) = "org.mindrot" % "jbcrypt" % version
def jsoupLib(version:String) = "org.jsoup" % "jsoup" % version

def printLoadInfo(name: String, version: String, methodName: String): Unit =
  println(
    s"""=============================
       |Loading $name - $version
       |-----------------------------
       |NOTE:
       |To specify the version, run
       |  $methodName("YOUR_VERSION")
       |
       |e.g.)
       |$methodName("$version")
       |""".stripMargin
  )

def loadHasher(version: String = DefaultVersions.Hasher,bcryptVersion:String = DefaultVersions.BCrypt): Unit = {
  printLoadInfo("Hasher", version, "loadHasher")
  interp.load.ivy(hasherLib(version))
  interp.load.ivy(bcryptLib(bcryptVersion))
  println(
    """---
      |import com.roundeights.hasher.Implicits._
      |""".stripMargin
  )
}

def loadJsoup(version:String = "1.13.1") = {
  printLoadInfo("Jsoup", version, "loadJsoup")
  interp.load.ivy(jsoupLib(version))
}
