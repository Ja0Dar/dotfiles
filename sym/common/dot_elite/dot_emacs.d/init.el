;; -*- lexical-binding: t; -*-

(setq comp-deferred-compilation nil
      comp-async-report-warnings-errors nil)

(add-hook 'window-setup-hook
          (lambda ()
            (message "Elite loaded %d packages in %s with %d gcs"
                     (length load-path)
                     (format "%.03fs" (float-time (time-subtract
                                                   (current-time)
                                                   before-init-time)))
                     gcs-done)))

(setq gc-cons-threshold 64000000)

;; TODO: make it non- HOME specific, make it easily deployable

(setq user-emacs-directory "/home/owner/.elite"
      menu-bar-mode nil
      user-init-file (concat user-emacs-directory "/.emacs.d/init.el")
      package-user-dir "/home/owner/.elite/elpa"
      package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/") ;; 50-70ms
                         ("org" . "https://orgmode.org/elpa/")
                         ))

(setenv "HOME" "/home/owner")

(load (concat user-emacs-directory
              (if (bound-and-true-p byte-compile-current-file)
                  "/.emacs.d/elegance.elc"
                "/.emacs.d/elegance.el")))


;;;Code
;;Begiining from https://github.com/nilcons/emacs-use-package-fast/blob/master/errge-dot-emacs.el

;; (package-initialize)
;; (unless (package-installed-p 'use-package)
;;           (package-refresh-contents)
;;           (package-install 'use-package))

;; Disable package initialize after us.  We either initialize it
;; anyway in case of interpreted .emacs, or we don't want slow
;; initizlization in case of byte-compiled .emacs.elc.


;; file-name-handler-alist shaves startup time:
;; https://www.reddit.com/r/emacs/comments/3kqt6e/2_easy_little_known_steps_to_speed_up_emacs_start/
(let ((file-name-handler-alist nil))

  (setq package-enable-at-startup nil)
  ;; Ask package.el to not add (package-initialize) to .emacs.
  (setq package--init-file-ensured t)
  ;; set use-package-verbose to t for interpreted .emacs,
  ;; and to nil for byte-compiled .emacs.elc
  (eval-and-compile
    (setq use-package-verbose (not (bound-and-true-p byte-compile-current-file))))
  ;; Add the macro generated list of package.el loadpaths to load-path.
  (mapc #'(lambda (add) (add-to-list 'load-path add))
        (eval-when-compile
          ;; (require 'package)
          (package-initialize)
          ;; Install use-package if not installed yet.
          (unless (package-installed-p 'use-package)
            (package-refresh-contents)
            (package-install 'use-package))
          (require 'use-package)
          (setq use-package-always-ensure t)
          (let ((package-user-dir-real (file-truename package-user-dir)))
            ;; The reverse is necessary, because outside we mapc
            ;; add-to-list element-by-element, which reverses.
            (nreverse (apply #'nconc
                             ;; Only keep package.el provided loadpaths.
                             (mapcar #'(lambda (path)
                                         (if (string-prefix-p package-user-dir-real path)
                                             (list path)
                                           nil))
                                     load-path))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; real .emacs starts here



  ;; so we can (require 'use-package) even in compiled emacs to e.g. read docs
  (use-package use-package :commands use-package-autoload-keymap :defer 1)

  (defvar-local hidden-mode-line-mode nil)
  (defvar hide-mode-line nil)



  (define-minor-mode hidden-mode-line-mode
    "Minor mode to hide the mode-line in the current buffer."
    :init-value nil
    :global t
    :variable hidden-mode-line-mode
    :group 'editing-basics
    (if hidden-mode-line-mode
        (setq hide-mode-line mode-line-format
              mode-line-format nil)
      (setq mode-line-format hide-mode-line
            hide-mode-line nil))
    (force-mode-line-update)
    ;; Apparently force-mode-line-update is not always enough to
    ;; redisplay the mode-line
    (redraw-display))

  (setq select-enable-primary t
        select-enable-clipboard t)
  ;; HAndled in elegance

  (require 'package)

  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

  (add-to-list 'package-archives
               '("melpa-stable" . "https://stable.melpa.org/packages/") t)

  (setq-default use-package-always-defer t
                use-package-always-ensure t
                indent-tabs-mode nil
                tab-width 2
                css-indent-offset 2)

  (fset 'yes-or-no-p 'y-or-n-p)

  (setq x-select-enable-clipboard-manager nil)

  (setq make-backup-files nil
        auto-save-default nil
        inhibit-splash-screen t
        confirm-kill-emacs 'yes-or-no-p
        visible-bell nil
        vc-follow-symlinks t

        vc-ignore-dir-regexp
        (format "%s\\|%s"
                vc-ignore-dir-regexp
                tramp-file-name-regexp)
        tramp-verbose 1
        ;; tramp-completion-reread-directory-timeout nil
        ;; tramp-use-ssh-controlmaster-options nil
        ;; docker-tramp-use-names t
        remote-file-name-inhibit-cache nil

        )

  (setq exec-path (append exec-path '("/usr/local/bin")))

  (show-paren-mode 1)
  (global-display-line-numbers-mode 1)

  (use-package s
    :commands s-split
    :defer 1
    )

  (use-package dash
    :commands -contains?
    )

  ;;  (use-package org
  ;;   :config (add-hook 'org-first-tab-hook #'org-indent-mode))


  (if (< 26 (string-to-number (car (s-split "\\." emacs-version))))
      (menu-bar--display-line-numbers-mode-relative)
    (setq display-line-numbers-type 'relative)
    )

  (global-hl-line-mode 1)


  (use-package comment-dwim-2
    :defer nil
    :commands comment-dwim-2
    :config (global-set-key (kbd "M-;") 'comment-dwim-2)
    )

  ;; (use-package smartparens
  ;;   :config
  ;;   (progn
  ;;     (require 'smartparens-config)
  ;;     (setq sp-highlight-pair-overlay nil
  ;;           sp-highlight-wrap-overlay nil
  ;;           sp-highlight-wrap-tag-overlay nil
  ;;           sp-max-prefix-length 50
  ;;           sp-max-pair-length 4
  ;;           sp-escape-quotes-after-insert nil
  ;;           )

  ;;     (let ((+sp-ignored-binddings '("C-M-a" "C-<right>")))
  ;;       (setq sp-smartparens-bindings (seq-filter
  ;;                                      (lambda (entry)  (not (-contains? +sp-ignored-binddings (car entry))) ) sp-smartparens-bindings
  ;;                                      )))
  ;;     (smartparens-global-mode 1)
  ;;     (sp-use-smartparens-bindings)

  ;;     (global-set-key (kbd "C-<right>") #'sp-slurp-hybrid-sexp)
  ;;     (global-set-key (kbd "C-k") #'sp-kill-hybrid-sexp )
  ;;     (global-set-key (kbd "M-s v") #'sp-split-sexp )
  ;;     (global-set-key (kbd "M-s u") #'sp-splice-sexp )
  ;;     (global-set-key (kbd "M-s M-n") #'sp-html-next-tag )
  ;;     (global-set-key (kbd "M-s M-p") #'sp-html-previous-tag )
  ;;     )
  ;;   :commands (sp-slurp-hybrid-sexp
  ;;              sp-kill-hybrid-sexp
  ;;              sp-split-sexp
  ;;              sp-splice-sexp
  ;;              sp-html-next-tag
  ;;              sp-html-previous-tag
  ;;              )
  ;;   :defer 1
  ;;   )




  (eval-after-load 'esh-mode
    '(progn


       (defalias 'l 'eshell/clear-scrollback)
       (defalias 'open 'find-file)

       (setq eshell-prompt-function (lambda () (concat
                                           (car (cdr (reverse (s-split "/" default-directory))))
                                           (if (= (user-uid) 0) " # " " λ "))))
       (setq eshell-prompt-regexp "^.* λ ")
       ))
  (add-hook 'eshell-mode-hook #'hidden-mode-line-mode)

  (setq eshell-last-dir-ring-size 500)

  (use-package projectile
    :defer 1
    :config
    (projectile-mode 1)
    (setq projectile-enable-caching nil)
    (setq projectile-completion-system 'ivy)
    )

  ;; (use-package esup
  ;;   :ensure t
  ;;   :pin melpa
  ;;   :commands (esup))

  (use-package evil
    :config
    (defun evil-set-cursor-including-terminal (orig-fn specs)
      (if (display-graphic-p)
          (funcall orig-fn specs)
        (if (evil-insert-state-p)
            (send-string-to-terminal "\e[5 q")
          (send-string-to-terminal "\e[1 q"))))

    (advice-add #'evil-set-cursor :around #'evil-set-cursor-including-terminal)
    (setq evil-emacs-state-cursor 'bar)

    (evil-ex-define-cmd "W"        #'save-buffer)
    (evil-ex-define-cmd "Wq"       #'evil-save-and-quit)
    (evil-ex-define-cmd "WQ"       #'evil-save-and-quit)
    (evil-ex-define-cmd "wQ"       #'evil-save-and-quit)
    (evil-ex-define-cmd "Q"        #'evil-quit)
    (evil-ex-define-cmd "Vs"       #'evil-window-vsplit)
    (evil-ex-define-cmd "VS"       #'evil-window-vsplit)
    (evil-ex-define-cmd "Sp"       #'evil-window-vsplit)
    (evil-ex-define-cmd "SP"       #'evil-window-vsplit)
    (define-key evil-insert-state-map (kbd "C-x C-f") 'company-files)
    (define-key evil-insert-state-map (kbd "C-SPC") 'company-complete)
    :defer 1
    :commands (evil-mode
               evil-insert-state-p
               evil-set-cursor
               evil-ex-define-cmd
               evil-save-and-quit
               evil-quit
               evil-window-vsplit
               evil-delay
               evil-normalize-keymaps
               )
    )


  (defun +toggle-evil-mode()
    (interactive)

    (if
        (bound-and-true-p evil-mode)
        (progn
          (unless (display-graphic-p)
            (send-string-to-terminal "\e[5 q"))
          (message "Disabling evil-mode"))
      (progn
        (message "Enabling evil-mode")))

    (call-interactively #'evil-mode))
  (global-set-key (kbd "<f1>") #'+toggle-evil-mode)

  (use-package evil-mc
    :after evil
    :commands (evil-mc-make-cursor-here
               evil-mc-make-all-cursors
               evil-mc-undo-all-cursors
               evil-mc-pause-cursors
               evil-mc-resume-cursors
               evil-mc-make-and-goto-first-cursor
               evil-mc-make-and-goto-last-cursor
               evil-mc-make-cursor-in-visual-selection-beg
               evil-mc-make-cursor-in-visual-selection-end
               evil-mc-make-cursor-move-next-line
               evil-mc-make-cursor-move-prev-line
               evil-mc-make-cursor-at-pos
               evil-mc-has-cursors-p
               evil-mc-make-and-goto-next-cursor
               evil-mc-skip-and-goto-next-cursor
               evil-mc-make-and-goto-prev-cursor
               evil-mc-skip-and-goto-prev-cursor
               evil-mc-make-and-goto-next-match
               evil-mc-skip-and-goto-next-match
               evil-mc-skip-and-goto-next-match
               evil-mc-make-and-goto-prev-match
               evil-mc-skip-and-goto-prev-match
               )
    :init
    ;; The included keybindings are too imposing and are likely to cause
    ;; conflicts, so we'll set them ourselves.
    (defvar evil-mc-key-map (make-sparse-keymap))

    :config
    (global-evil-mc-mode +1)

    (evil-define-key '(normal visual) global-map
      "gzj" #'evil-mc-make-cursor-move-next-line
      "gzd" #'evil-mc-make-and-goto-next-match
      "gzD" #'evil-mc-make-and-goto-prev-match
      "gzj" #'evil-mc-make-cursor-move-next-line
      "gzk" #'evil-mc-make-cursor-move-prev-line
      "gzm" #'evil-mc-make-all-cursors
      "gzn" #'evil-mc-make-and-goto-next-cursor
      "gzN" #'evil-mc-make-and-goto-last-cursor
      "gzp" #'evil-mc-make-and-goto-prev-cursor
      "gzP" #'evil-mc-make-and-goto-first-cursor
      "gzq" #'evil-mc-undo-all-cursors
      ;; "gzt" #'+multiple-cursors/evil-mc-toggle-cursors
      ;; "gzu" #'+multiple-cursors/evil-mc-undo-cursor ;TODO:bcm
      ;; "gzz" #'+multiple-cursors/evil-mc-toggle-cursor-here
      "gzI" #'evil-mc-make-cursor-in-visual-selection-beg
      "gzA" #'evil-mc-make-cursor-in-visual-selection-end)



                                        ;TODO:bcm
    ;; REVIEW This is tremendously slow on macos and windows for some reason.
    ;; (setq evil-mc-enable-bar-cursor t)
    (add-hook 'evil-insert-state-entry-hook #'evil-mc-resume-cursors)
    )


  (use-package anzu
    :after isearch-mode)

  (use-package evil-anzu
    :after evil-ex
    :config (global-anzu-mode 1)
    )

  (with-eval-after-load 'evil
    (require 'evil-anzu))

  (use-package evil-surround
    :after evil
    :commands (global-evil-surround-mode
               evil-surround-edit
               evil-Surround-edit
               evil-surround-region)
    :config (global-evil-surround-mode 1)
    :init (evil-define-key 'visual global-map "S" 'evil-surround-region)
    )

  (use-package which-key
    :defer 1
    :config (which-key-mode))

  (use-package magit
    :pin melpa-stable
    :config (progn
              (put 'magit-clean 'disabled nil)
              (add-hook 'magit-status-sections-hook 'magit-insert-worktrees)
              (setq magit-commit-show-diff nil))
    :commands magit-status)

  (use-package markdown-mode
    :mode (("\\.md\\'" . markdown-mode))
    :config (progn
              (add-hook 'markdown-mode-hook 'visual-line-mode)))

  (use-package yaml-mode
    :mode (("\\.yml" . yaml-mode)
           ("\\.yaml" . yaml-mode)))

  (use-package rjsx-mode
    :defer 1
    :interpreter (("node" . rjsx-mode))
    :mode (("\\.js?\\'" . rjsx-mode)
           ("\\.jsx?\\'" . rjsx-mode))
    :config (progn
              (electric-indent-mode -1)
              (setq js2-basic-offset 2
                    js2-highlight-level 3
                    js2-bounce-indent-p t
                    js2-mode-show-strict-warnings nil)))

  (use-package scala-mode
    :defer 1
    :mode (("\\.scala\\'" . scala-mode)
           ("\\.sc\\'" . scala-mode)
           ("\\.sbt\\'" . scala-mode))
    )

  ;; (use-package lsp-metals) TODO: - add metals

  (use-package fish-mode
    :mode (("\\.fish\\'" . fish-mode))
    :config (setq fish-enable-auto-indent t)
                                        ;TODO:bcm  make it autoindent
    )

  (use-package nix-mode
    :mode (("\\.nix\\'" . nix-mode))
    )

  (use-package protobuf-mode
    :defer 1
    :mode (("\\.proto\\'" . protobuf-mode))
    )

  (setq dired-dwim-target t
        dired-recursive-deletes t
        dired-use-ls-dired nil
        delete-by-moving-to-trash t)

  (use-package ivy
    :config (progn
              (ivy-mode)
              (setq ivy-use-virtual-buffers t)
              (setq ivy-count-format "")
              (setq ivy-use-selectable-prompt t)))

  (use-package swiper
    :defer nil
    :commands swiper
    :config
    (global-set-key (kbd "C-s") #'swiper))


  (use-package counsel
    :defer nil
    :commands counsel-M-x
    :config (global-set-key (kbd "M-x") #'counsel-M-x)
    )

  (use-package company
    :commands company-complete-common company-complete company-files company-manual-begin company-grab-line
    :defer t
    ;; :hook (doom-first-input . global-company-mode)
    :init
    (setq company-idle-delay 0.1
          company-minimum-prefix-length 2
          company-tooltip-limit 14
          company-tooltip-align-annotations t
          company-require-match 'never
          company-global-modes '(not erc-mode message-mode help-mode gud-mode)
          company-frontends '(company-pseudo-tooltip-frontend
                              company-echo-metadata-frontend)

          ;; Buffer-local backends will be computed when loading a major mode, so
          ;; only specify a global default here.
          company-backends '(company-capf)
          company-auto-complete nil
          company-auto-complete-chars nil
          company-dabbrev-other-buffers nil
          company-dabbrev-ignore-case nil
          company-dabbrev-downcase nil)

    :config
    (progn
      (global-company-mode)
      (require 'company-files)
      (add-to-list 'company-files--regexps "file:\\(\\(?:\\.\\{1,2\\}/\\|~/\\|/\\)[^\]\n]*\\)")
      (add-hook 'company-mode-hook #'evil-normalize-keymaps)

      ;; (define-key evil-insert-state-map (kbd "C-x C-f") 'company-files)
      ;; (define-key evil-insert-state-map (kbd "C-SPC") 'company-complete)
      )

    )

  (use-package counsel-projectile
    :after 'counsel-mode
    :defer 1)

  (use-package vlf
    :commands vlf)

  (global-set-key (kbd "C-x t l") #'display-line-numbers-mode)

  ;; NOTE - nice, but costs startup time
  ;; (load-theme 'tsdh-dark)

  ;;Restore after everything loaded, bcs after-init-hook is not loaded

  (setq gc-cons-threshold 16777216); 16mb
  )

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(which-key anzu evil-surround evil use-package smartparens projectile comment-dwim-2)))
