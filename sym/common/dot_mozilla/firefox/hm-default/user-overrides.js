/* override recipe: enable session restore ***/
user_pref("browser.startup.page", 3); // 0102


// NOTE: using this instead of cookie auto delete due to
// https://github.com/Cookie-AutoDelete/Cookie-AutoDelete/issues/807

user_pref("privacy.clearOnShutdown.history", false); // 2811


// needed to keep history since FF128+? https://github.com/arkenfox/user.js/issues/1859#issuecomment-2243460299
user_pref("privacy.clearOnShutdown_v2.historyFormDataAndDownloads", false);
  // user_pref("privacy.clearOnShutdown.cookies", false); // 2811 optional: default false arkenfox v94
  // user_pref("privacy.clearOnShutdown.formdata", false); // 2811 optional
// user_pref("privacy.cpd.history", false); // 2812 to match when you use Ctrl-Shift-Del
  // user_pref("privacy.cpd.cookies", false); // 2812 optional: default false arkenfox v94
  // user_pref("privacy.cpd.formdata", false); // 2812 optional


// REVIEW: enable?
/* override recipe: RFP is not for me ***/
user_pref("privacy.resistFingerprinting", false); // 4501
user_pref("privacy.resistFingerprinting.letterboxing", false); // 4504 [pointless if not using RFP]
user_pref("webgl.disabled", false); // 4520 [mostly pointless if not using RFP]


// TODO: enable?
// Disable cookie deletion (in favor of cookie autodelete)
// user_pref("privacy.sanitize.sanitizeOnShutdown", false);

// REFERER policy TODO: check if can undo
user_pref("network.http.referer.XOriginPolicy",0); // 1601


// UI TWEAKS
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
user_pref("devtools.toolbox.host", "window");
user_pref("devtools.inspector.show_pseudo_elements", true);

// user_pref("layout.css.backdrop-filter.enabled", true);
// user_pref("browser.compactmode.show", true);
// user_pref("svg.context-properties.content.enabled", true);

///////// Usability/preference

// keyword.enabled enables search from ulbar
user_pref("keyword.enabled", false);

// Remove pocket
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("extensions.pocket.enabled", true);

// Newtab
user_pref("browser.newtabpage.enabled", true);
// Disable saving passwords
user_pref("signon.rememberSignons", false);

user_pref("joverrides.status", "Success");
