#!/usr/bin/env bash

#Git
alias gti=git #omit
alias gir=git #omit
alias gsu="git submodule update --remote --init"

alias rulz="git push"                        #omit
alias rerulz="git pull --rebase && git push" #omit
alias bringz="git pull"                      #omit

alias gbb="git branch | fzy | sed 's/*//g' | xargs -I _ git checkout _"

#TODO - fix <,,,> -> orign/HEAD etc
alias gbbr="git branch -r | fzy | sed 's/*//g' | awk -F'/' '{for (i=2; i<NF; i++) printf \$i \"/\"; print \$NF}' | xargs -I _ git checkout _"

#Removing
alias rmm="ls | sk -m | xargs -I _ rm -r _"
alias rmmf="ls | sk -m | xargs -I _ rm -rf _"

alias p1="ping 1.1"
alias ga="git add"

#Misc
alias falias="alias | fzy | cut -d\"'\" -f2 | xargs -I _ sh -c  _ "
