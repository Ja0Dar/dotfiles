-- init.lua


-- BASICS
vim.g.mapleader = " "
vim.wo.wrap = false
vim.wo.number = true
vim.wo.relativenumber = true

-- Enable system clipboard
vim.opt.clipboard = 'unnamedplus'

-- Theme
require('onedark').setup {
    style = 'warmer'
}
require('onedark').load()

-- Link concealing
vim.opt.conceallevel = 2
vim.opt.concealcursor = 'nc'

-- ORG MODE

-- Treesitter configuration
-- require('nvim-treesitter.configs').setup {
--   -- If TS highlights are not enabled at all, or disabled via `disable` prop,
--   -- highlighting will fallback to default Vim syntax highlighting
--   highlight = {
--     enable = true,
--     -- Required for spellcheck, some LaTex highlights and
--     -- code block highlights that do not have ts grammar
--     additional_vim_regex_highlighting = {'org'},
--   },
--   -- ensure_installed = {'org'}, -- Or run :TSUpdate org
-- }


-- require('orgmode').setup({
--   -- org_agenda_files = {'~/Dropbox/org/*', '~/my-orgs/**/*'},
--   org_default_notes_file = '~/next/notes/gtd/inbox.org',
-- })