alias gti = git
alias gir = git #omit

alias gsu = git submodule update --remote --init
alias rulz = git push
def rerulz [] {git pull --rebase; git push}
alias bringz = git pull #omit
def gpsup [] { git push --set-upstream origin (git branch| grep '*'| sed 's/* //') }
def gbb [] { git branch | fzy | sed 's/*//g' | xargs -I _ git checkout _ }

alias gst = git status
alias gste = with-env [TERM "xterm-direct"] {emacsclient -create-frame --tty -a ""  --eval "(progn (magit-status) (delete-other-windows))"}

alias ga = git add

alias gc = git commit
alias gco = git checkout
alias gcb = git checkout -b
alias gcm = git checkout master

alias gdca = git diff --cached
alias grh = git reset
alias grhh = git reset --hard

def gbbr [] { git branch -r | fzy | sed 's/*//g' | awk -F'/' '{for (i=2; i<NF; i++) printf \$i \"/\"; print \$NF}' | xargs -I _ git checkout _ }
def gbda [] { git branch --no-color --merged | command grep -vE '^(\*|\s*(master|develop|dev|release)\s*\$)' | command xargs -n 1 git branch -d }
alias glod = git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset"

alias ggh = cd (git rev-parse --show-toplevel)
alias ggp = cd (project_root -g)

alias currentMillis = sh -c "date +'%s%N'  | head -c -7"
def dateymd [] { ^date -u +"%Y_%m_%d" }

alias zf = zathura --fork
alias p1 = ping 1.1

alias unfreeze_emacs = pkill -SIGUSR2 emacs # https://emacs.stackexchange.com/a/21645

alias hmem = htop --sort-key PERCENT_MEM
alias hcpu = htop --sort-key PERCENT_CPU


alias ns = nix-shell --run nu
alias vim = nvim #omit
