{ config, lib, pkgs, ... }:

{
  imports = [ ./modules ];
  programs.man.enable = false;
  home.extraOutputsToInstall = [ "man" ];

  # Let Home Manager install and manage itself.

  programs.home-manager.enable = true;

  services.lorri.enable = true;


  # Hyprland
  services.avizo.enable = true;
  services.hyprpaper = {
    enable = true;
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  home.packages = with pkgs; [
    cached-nix-shell
    topgrade
  ];

  fonts.fontconfig.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "owner";
  home.homeDirectory = "/home/owner";

  systemd.user.services = {

    checkcpu = {
      Service = {
        Type = "oneshot";
        ExecStart = "/home/owner/git/dotfiles/scripts/check_cpu";
        Environment = "PATH=/home/owner/.local/bin:/home/owner/.nix-profile/bin:/run/current-system/sw/bin:/usr/bin:/bin";
      };
      Install.WantedBy = [ "default.target" ];
    };

    cronbat = {
      Service = {
        Type = "oneshot";
        ExecStart = "/home/owner/.local/bin/cron/cronbat";
        Environment = "PATH=/home/owner/.local/bin:/home/owner/.nix-profile/bin:/run/current-system/sw/bin:/usr/bin:/bin";
      };
      Install.WantedBy = [ "default.target" ];
    };

    checkup = {
      Service = {
        Type = "oneshot";
        ExecStart = "/home/owner/.local/bin/cron/checkup";
        Environment = "PATH=/home/owner/.local/bin:/home/owner/git/dotfiles/scripts:/home/owner/.nix-profile/bin:/run/current-system/sw/bin:/usr/bin:/bin";
      };

      Install.WantedBy = [ "default.target" ];
    };


    gtdmonitor = {
      Service = {
        Type = "oneshot";
        ExecStart = "/home/owner/.local/bin/cron/gtdmonitor";
        Environment = "PATH=/home/owner/.local/bin:/home/owner/.nix-profile/bin:/run/current-system/sw/bin:/usr/bin:/bin";
      };

      Install.WantedBy = [ "default.target" ];
    };

    # mbsync = {
    #   Service = {
    #     Type = "oneshot";
    #     ExecStart = "/home/owner/.nix-profile/bin/mbsync -Va";
    #     ExecStartPost = "/run/current-system/sw/bin/sh -c \"/home/owner/.nix-profile/bin/mu index || echo Locked\"";
    #   };

    #   Install.WantedBy = [ "default.target" ];
    # };

  };

  systemd.user.timers = {
    # https://www.freedesktop.org/software/systemd/man/systemd.time.html

    cronbat = {
      Timer = {
        OnCalendar = "minutely";
        Persistent = true;
      };
      Install.WantedBy = [ "timers.target" ];
    };

    checkup = {
      Timer = {
        OnCalendar = "*-*-* 0/2:00:00"; # every 2 hours
        Persistent = true;
      };
      Install.WantedBy = [ "timers.target" ];
    };


    checkcpu = {
      Timer = {
        OnCalendar = "*-*-* *:0/1:00"; # every 1 minute
        Persistent = true;
      };
      Install.WantedBy = [ "timers.target" ];
    };

    gtdmonitor = {
      Timer = {
        OnCalendar = "*-*-* *:*:0/15"; # every 15 sec
        Persistent = true;
      };
      Install.WantedBy = [ "timers.target" ];
    };

    # FIXME: bring back when I fix annoying gpg monit
    # mbsync = {
    #   Timer = {
    #     OnCalendar = "*-*-* *:0/5:00"; # every 5 minutes
    #     Persistent = true;
    #   };
    #   Install.WantedBy = [ "timers.target" ];
    # };


  };
}
