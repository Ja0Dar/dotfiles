{ pkgs, lib }:
let
  mach-nix = import
    (builtins.fetchGit {
      url = "https://github.com/DavHau/mach-nix";
      ref = "refs/tags/3.5.0";
    })
    {
      pypiDataRev = "17f523a15e819fae41ab8a55b5f2851872e4c20b";
      # pypiDataSha256 = "sha256:1c5gv76hnx3cpi9zlcbfb92qb1nl66005485npq5l9b0a29hip1c";
      python = "python39";
    };
  pyEnv = mach-nix.mkPython rec {

    requirements = ''
      easyocr
      setuptools
    '';
  };
in
pkgs.stdenv.mkDerivation {
  name = "easy-ocr-cli";
  pname = "easy-ocr-cli";
  phases = [ "installPhase" ];
  buildInputs = [ pyEnv ];
  # TODO - system-wide easyocr or shell + script.. not sure
  installPhase = ''
    mkdir -p "$out/bin"

    cat >> "$out/bin/easy-ocr-cli" << EOF
    #!${pyEnv}/bin/python

    import easyocr
    import sys

    # Originally from
    # https://github.com/metafy-social/python-scripts/blob/master/scripts/Extract_TEXT_FROM_IMAGE/main.py


    if len(sys.argv) < 3:
      print("easy-ocr-cli <lang1+lang2> <path>")
      sys.exit(1)

    languages = sys.argv[1].split("+")
    # languages = ["en"]  # refer https://www.jaided.ai/easyocr/ for supporting langs
    IMG_PATH = sys.argv[2]


    reader = easyocr.Reader(languages, gpu=False)

    result = reader.readtext(IMG_PATH)

    lines = []
    for tup in result:
        lines.append(tup[1])

    print("\n".join(lines))


    EOF
    chmod u+x  "$out/bin/easy-ocr-cli"
  '';
}
