{ pkgs ? <nixpkgs>, lib ? pkgs.lib }:

with pkgs.python3Packages;
let
  curlify =
    buildPythonPackage rec {
      pname = "curlify";
      version = "2.2.1";
      disabled = ! pythonAtLeast "3.8";

      src = fetchPypi {
        inherit pname version;
        sha256 = "sha256-DT8C5yNfr5Ut6O9F70aYRRltMGMtWDi81a7iF3Jt3W0=";
      };

      propagatedBuildInputs = [
        requests
      ];

      meta = with lib; {
        homepage = "https://github.com/ofw/curlify";
        description = "";
        license = licenses.mit;
        maintainers = with maintainers; [ ];
      };
    };
in
buildPythonPackage rec {
  pname = "pricehist";
  version = "1.4.4";
  disabled = ! pythonAtLeast "3.8";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-q2AFYxdQZ8mDFxwoTD9kYp6GBekXDrhzRdm91oAIZ50=";
  };

  propagatedBuildInputs = [
    requests
    lxml
    cssselect
    curlify
  ];

  meta = with lib; {
    homepage = "https://gitlab.com/chrisberkhout/pricehist/";
    description = "";
    license = licenses.mit;
    maintainers = with maintainers; [ ];
  };
}
