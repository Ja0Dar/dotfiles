{ config, lib, pkgs, ... }:
let
  cfg = config.fonts;
in
{
  options.fonts = {
    enable = lib.mkEnableOption "Enable fonts module.";
  };

  config = lib.mkIf cfg.enable {

    home.packages = with pkgs; [
      nerd-fonts.roboto-mono
      nerd-fonts.fira-code
      source-han-mono # Japanese
      # fira-code
      # fira-code-symbols
      hack-font
      merriweather
      font-awesome_4
      font-awesome_5
      overpass
      iosevka-comfy.comfy-duo
      # hasklig
    ];
  };
}
