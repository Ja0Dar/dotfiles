{ config, lib, pkgs, ... }:
let
  cfg = config.dev;
in
{
  options.dev = {
    enable = lib.mkEnableOption "Enable dev module.";
    burp = lib.mkEnableOption "Enable burpsuite.";
    zap = lib.mkEnableOption "Enable OWASP zap.";
    zw = lib.mkEnableOption "Enable Zowier.";
  };

  config = lib.mkIf cfg.enable {

    programs.vscode = {
      enable = true;
      # extensions = with pkgs.vscode-extensions; [
      #   dracula-theme.theme-dracula
      #   vscodevim.vim
      #   # yzhang.markdown-all-in-one
      # ];
    };

    home.packages = with pkgs; [
      solaar # FIXME - move somewhere

      # Misc editors to try-out (& skim for ideas to add to emacs?)
      helix
      zed-editor

      git-crypt
      distrobox
      bubblewrap

      (lib.mkIf cfg.zap
        (jlib.javaFontWrap pkgs { bin = "zap"; }))

      (lib.mkIf cfg.zap
        (makeDesktopItem {
          desktopName = "OWASP Zap (zaproxy)";
          name = "zap";
          exec = "zap";
          categories = [ "Development" ];
          icon = /home/owner/git/dotfiles/img/zap.png;
        }))


      (lib.mkIf cfg.burp
        (jlib.javaFontWrap pkgs { bin = "burpsuite"; }))

      caido
      # (lib.mkIf cfg.burp jython)

      (lib.mkIf cfg.burp
        (makeDesktopItem {
          desktopName = "BurpSuite";
          name = "burpsuite";
          exec = "burpsuite";
          categories = [ "Development" ];
          icon = /home/owner/git/dotfiles/img/burp.png;
        }))

      (lib.mkIf cfg.zw zowier)
      # DB
      # robo3t
      redli

      grpcurl

      # Cloud
      k9s
      kubectl
      kubectx
      sops
      argocd
      google-cloud-sdk
      awscli
      ansible
      kubernetes-helm

      # FIXME - as of https://github.com/adrienverge/openfortivpn/issues/1076#issuecomment-1648126967 manual intervention:
      # echo ipcp-accept-remote > /etc/ppp/options
      # is needed to make openfortivpn work
      openfortivpn
      openvpn
      wireguard-tools
      github-cli
      glab
      pre-commit

    ];
  };
}
