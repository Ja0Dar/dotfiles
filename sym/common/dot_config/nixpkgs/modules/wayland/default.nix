{ config, lib, pkgs, ... }:
let
  cfg = config.wayland;
in
{
  options.wayland = { enable = lib.mkEnableOption "Enable wayland module."; };
  config = lib.mkIf cfg.enable {

    # xdg.configFile."electron-flags.conf".text = ''
    # --enable-features=UseOzonePlatform
    # --ozone-platform=wayland
    # '';

    home.sessionVariables = {
      MOZ_ENABLE_WAYLAND = 1;
      XDG_CURRENT_DESKTOP = "sway";
      NIXOS_OZONE_WL = "1";
    };

    home.packages = with pkgs; [
      # TODO: feature-gate Hyprland
      hyprlock
      hypridle
      pyprland

      (lib.mkIf (!config.for-nixos.enable) (sway.override {
        withGtkWrapper = true;
        withBaseWrapper = true;
      })) # doesnt work on arch

      swayidle
      swayr
      wl-clipboard
      wl-clipboard-x11
      waybar
      wob # wayland overlay bar
      xwayland
      wev # wayland event viewer

      # Randring
      (jlib.glWrap pkgs { bin = "wdisplays"; })
      wlr-layout-ui
      kanshi # autorandr..
      wlr-randr

      # screenshoting trio
      grim
      slurp
      swappy
      flameshot
      kooha

      # Menus
      rofi-wayland
      bemenu

      wtype
    ];


  };
}
