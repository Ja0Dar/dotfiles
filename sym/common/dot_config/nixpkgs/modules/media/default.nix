{ config, lib, pkgs, inputs, ... }:
let
  cfg = config.media;
  waylandFreetube = pkgs.jlib.wrapPassingShare pkgs {
    bin = "freetube";
    script = ''
      #!/usr/bin/env bash
      exec ${pkgs.freetube}/bin/freetube --enable-features=UseOzonePlatform --ozone-platform=wayland "$@"
    '';
  };
in
{
  options.media = {
    enable = lib.mkEnableOption "Enable media module.";
    bloat = lib.mkEnableOption "Enable media bloat apps.";
    editors = lib.mkEnableOption "Enable media editors.";
  };

  config = lib.mkIf cfg.enable {

    programs.obs-studio = {
      enable = true;
      package = (pkgs.jlib.glWrap pkgs { bin = "obs"; package = pkgs.obs-studio; });
      plugins = with pkgs.obs-studio-plugins;[
        obs-pipewire-audio-capture
        obs-scale-to-sound
      ];
    };

    home.packages = with pkgs; [
      # FFmpeg
      (lib.mkIf config.for-nixos.enable ffmpeg-full)
      ffmpegthumbnailer

      # Audio playback
      pavucontrol
      pulsemixer
      playerctl
      qt5.qttools # for qdbus

      # Images
      gthumb
      (jlib.glWrap pkgs { bin = "imv"; })
      imagemagick
      exiv2
      # gifsicle # gif optimisation

      # Videos
      (jlib.glWrap pkgs { bin = "mpv"; })
      # (jlib.glWrap pkgs { bin = "ani-cli"; })
      # python39Packages.subliminal # get subtitles

      (lib.mkIf cfg.bloat (if config.wayland.enable then waylandFreetube else pkgs.freetube))
      (lib.mkIf cfg.editors (jlib.glWrap pkgs { bin = "kdenlive"; }))
      (lib.mkIf cfg.editors audacity)

      # (lib.mkIf cfg.bloat mypaint)
      # (lib.mkIf cfg.bloat mypaint-brushes)
      (lib.mkIf cfg.bloat gimp)
      (lib.mkIf cfg.bloat xournalpp)

      # Requirement for xournalpp as of https://github.com/NixOS/nixpkgs/issues/163107#issuecomment-1100566253
      (lib.mkIf cfg.bloat adwaita-icon-theme)
      # (lib.mkIf cfg.bloat (jlib.glWrap pkgs { bin = "krita"; }))

      yt-dlp # youtube-dl fork, faster and more features

      # steam
    ];


  };
}
