{ config, lib, pkgs, ... }:
let
  cfg = config.gui;
in
{
  options.gui = {
    enable = lib.mkEnableOption "Enable gui module.";
    office = lib.mkEnableOption "Enable Onlyoffice";
    redundant = lib.mkEnableOption "Enable Redundant apps";
    minecraft = lib.mkEnableOption "Enable minecraft";
    calibre = lib.mkEnableOption "Calibre";
    non-essential = lib.mkEnableOption "Non-essential";
  };

  config = lib.mkIf cfg.enable {
    home.packages = with pkgs; [
      kmonad
      (lib.mkIf (!config.for-nixos.enable) nixGLIntel)

      # Menus
      copyq

      # used in copyq:
      (lib.mkIf cfg.non-essential (tesseract5.override {
        enableLanguages = [ "eng" "pol" "osd" ];
      }))
      # easy-ocr-cli

      qrencode # for copyq qrcode support

      # Terminals
      (jlib.glWrap pkgs { bin = "alacritty"; })
      foot
      theme-sh # for theme switching


      (jlib.glWrap pkgs {
        package = nextcloud-client;
        bin = "nextcloud";
      })


      # Micro utils
      nautilus

      # Xorg toys
      xdragon # dragon-drag-and-drop
      xorg.xkill
      libinput-gestures

      # Misc
      keepassxc
      bitwarden-cli
      bitwarden
      veracrypt
      (lib.mkIf cfg.non-essential transmission_3-qt)
      (lib.mkIf cfg.office onlyoffice-bin)

      # wire-desktop # looks like flatpack updates it more quilckly :/
      signal-desktop
      protonmail-bridge
      zathura # PDF

      # at least on arch calibre needs "udisks2" to discover
      (lib.mkIf cfg.calibre (jlib.glWrap pkgs {
        bin = "calibre";
        package = (calibre.overrideAttrs (oldAttrs: rec {
          buildInputs = oldAttrs.buildInputs ++ [ python3Packages.pycrypto ];
        })
        );
      }))


      (lib.mkIf cfg.minecraft (jlib.glWrap pkgs {
        bin = "multimc";
        # bin = "minecraft-launcher";
        # package = pkgs.minecraft;
      }))
    ];
  };
}
