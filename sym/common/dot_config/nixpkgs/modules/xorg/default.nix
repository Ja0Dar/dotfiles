{ config, lib, pkgs, ... }:
let
  enableXorg = ! config.wayland.enable;
in
{
  config = lib.mkIf (enableXorg) {

    home.packages = with pkgs; [
      flameshot
      xwallpaper
      rofi
      (jlib.glWrap pkgs { bin = "picom"; })
      redshift

      xorg.xev
      (lib.mkIf cfg.non-essential pick-colour-picker)
      xclip
      xdotool
      xdo
      xidlehook

      dmenu
      slop # for dmenurecord
      lemonbar-xft
    ];

  };
}
