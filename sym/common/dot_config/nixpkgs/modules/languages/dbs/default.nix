{ config, lib, pkgs, ... }:
let cfg = config.languages.dbs;
in
{
  options.languages.dbs = {
    enable = lib.mkEnableOption "Enable dbs module.";
  };

  config = lib.mkIf cfg.enable {


    home.packages = with pkgs; [
      pgcli
      postgresql # for emacs /sql support / pg_format
      # sqlfluff

      mongosh
    ];

  };
}
