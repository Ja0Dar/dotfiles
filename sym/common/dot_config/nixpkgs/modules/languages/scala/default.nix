{ config, lib, pkgs, ... }:
let cfg = config.languages.scala;
in
{
  options.languages.scala = {
    enable = lib.mkEnableOption "Enable scala module.";
  };

  config = lib.mkIf cfg.enable {

    home.file.".config/java_home".text = "export JAVA_HOME=${pkgs.jdk}";

    home.packages = with pkgs; [
      jdk
      sbt
      coursier
      bloop
      scala-cli
      metals
      scalafmt
      scalafix
      (jlib.javaFontWrap pkgs { bin = "visualvm"; })

      # scalafix # TODO:bcm  update it?

      # Well... and Protobuf
      scalaproto
      protobuf # TODO:bcm  move
      clang-tools
    ];

  };
}
