{ config, lib, pkgs, ... }:
let cfg = config.languages.rust;
in
{
  options.languages.rust = {
    enable = lib.mkEnableOption "Enable rust module.";
  };

  config = lib.mkIf cfg.enable {


    home.packages = with pkgs; [
      rustup
    ];

  };
}
