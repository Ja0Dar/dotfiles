{ config, lib, ... }: {
  imports = [ ./frontend ./latex ./nix ./python ./scala ./rust ./shell ./dbs ];

  options.languages = { };

}
