{ config, lib, pkgs, ... }:
let cfg = config.languages.nix;
in
{
  options.languages.nix = { enable = lib.mkEnableOption "Enable nix module."; };

  config = lib.mkIf cfg.enable {
    home.packages = with pkgs; [
      nixpkgs-fmt # yeah, nix formatter
      cachix
      nixd
    ];

  };
}
