{ config, lib, pkgs, ... }:
let cfg = config.languages.latex;
in
{
  options.languages.latex = {
    enable = lib.mkEnableOption "Enable latex module.";
  };
  # TODO:bcm  make sure it is enabled in "emacs" as it's required by doom

  config = lib.mkIf cfg.enable {
    home.packages = with pkgs; [
      # Latex
      # texlive.combined.scheme-medium
      (texlive.combine {
        inherit (texlive)
          # emacs + org exporting - baseline
          scheme-medium minted wrapfig capt-of fvextra upquote catchfile xstring
          kvoptions fancyvrb pdftexcmds etoolbox xcolor lineno framed
          # bibliography
          hanging biblatex
          # xournalpp
          varwidth standalone scontents
          ;
      })

      biber
    ];

  };
}
