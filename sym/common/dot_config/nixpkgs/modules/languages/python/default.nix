{ config, lib, pkgs, ... }:
let
  cfg = config.languages.python;
  pricehist = pkgs.callPackage ../../../packages/python-packages/pricehist/default.nix { };
in
{
  options.languages.python = {
    enable = lib.mkEnableOption "Enable python module.";
    dbs = lib.mkEnableOption "Enable database drivers";
  };

  config = lib.mkIf cfg.enable {

    # Ignoring collisions as dependencies of jupyter have conflicting output
    # https://github.com/NixOS/nixpkgs/issues/145968
    home.packages = with pkgs; [

      # FIXME  open-interpreter
      ((python3.withPackages (ps:

        let
          packages =

            with ps; [
              pylint
              isort
              flake8
              black

              beancount
              # pricehist problematic

              numpy
              pandas
              openpyxl # for reading excel in pandas
              tqdm
              jupyter
              matplotlib
              # seaborn

              ptpython
              pip
              uv
              virtualenv
            ];
          dbPackages =
            if cfg.dbs then with ps;[
              redis
              sqlalchemy
              psycopg2
              pymongo
            ] else [ ];
        in
        packages ++ dbPackages
      )).override (args: { ignoreCollisions = true; }))
      mypy
      pyright
    ];

  };
}
