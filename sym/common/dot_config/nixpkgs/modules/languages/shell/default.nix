{ config, lib, pkgs, ... }:
let
  cfg = config.languages.shell;
in
{
  options.languages.shell = {
    enable = lib.mkEnableOption "Enable shell language module.";
  };

  config = lib.mkIf cfg.enable {

    home.packages = with pkgs; [
      shfmt
      shellcheck

      pgformatter
      # python39Packages.sqlparse ## FIXME: not here ;)
    ];
  };
}
