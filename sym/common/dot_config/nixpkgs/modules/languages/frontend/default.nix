{ config, lib, pkgs, ... }:
let
  cfg = config.languages.frontend;
in
{
  options.languages.frontend = {
    enable = lib.mkEnableOption "Enable frontend module.";
  };

  config = lib.mkIf cfg.enable {

    home.packages = with pkgs;
      [

        #Javascript
        nodePackages.prettier
        #nodePackages.ts-node
        nodejs
        yarn
        nodePackages.typescript-language-server
        nodePackages.typescript
        eslint
        html-tidy

      ];
  };
}
