{ config, lib, pkgs, ... }:
let cfg = config.ai;
in
{
  options.ai = {
    enable = lib.mkEnableOption "Enable ai module.";
    diagrams = lib.mkEnableOption "Enable diagram module.";
  };

  config = lib.mkIf cfg.enable {


    home.packages = with pkgs; [
      ollama
      aider-chat
      fabric-ai
      whisper-ctranslate2
    ];

  };
}
