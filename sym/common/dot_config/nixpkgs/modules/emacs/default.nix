{ config, lib, pkgs, ... }:
let
  cfg = config.emacs;
  my-emacs = if (cfg.pgtk) then pkgs.emacs30-pgtk else pkgs.emacs;
in
{
  options.emacs = {
    enable = lib.mkEnableOption "Enable emacs module.";
    pgtk = lib.mkEnableOption "Enable emacs pgtk.";
  };

  config = lib.mkIf cfg.enable {
    home.packages = with pkgs; [

      (mu.override { emacs = my-emacs; })
      isync
      msmtp
      sqlite
      # ispell
      (aspellWithDicts (dicts: with dicts; [ en en-computers pl ]))
      gnupg1
      # TODO
      # binutils # native-comp needs 'as', provided by this

      # ## Doom dependencies
      # git
      # gnutls # for TLS connectivity

      # ## Optional dependencies
      # fd # faster projectile indexing
      # imagemagick # for image-dired
      # zstd # for undo-fu-session/undo-tree compression

      # ## Module dependencies
      # # :checkers spell
      # (aspellWithDicts (ds: with ds; [ en en-computers en-science pl ]))
      # # :checkers grammar
      languagetool
      # # :tools editorconfig
      # editorconfig-core-c # per-project style config
      # # :tools lookup & :lang org +roam
      # # :lang cc
      # ccls
      # # :lang javascript
      # nodePackages.javascript-typescript-langserver
      # # :lang latex & :lang org (latex previews)
      # texlive.combined.scheme-medium
      # # :lang rust
      # rustfmt
      # # unstable.rust-analyzer
      # rust-analyzer
      # FIXME uncomment when LIBRARY_PATH missing is fixed
      # emacsPackages.cask
    ];

    # FIXME uncomment when LIBRARY_PATH missing is fixed
    programs.emacs = {
      enable = true;
      package = my-emacs;
      extraPackages = (epkgs: with epkgs;[
        # vterm
        pkgs.emacsPackages.mu4e
        djvu
      ]);
    };

  };
}
