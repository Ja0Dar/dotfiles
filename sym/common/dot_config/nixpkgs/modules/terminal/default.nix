{ config, lib, pkgs, ... }:
let cfg = config.terminal;
in
{
  options.terminal = {
    enable = lib.mkEnableOption "Enable terminal module.";
    diagrams = lib.mkEnableOption "Enable diagram module.";
    jira = lib.mkEnableOption "Enable jira module.";
  };

  config = lib.mkIf cfg.enable {



    programs = {

      neovim = {
        vimAlias = true;
        enable = true;

        plugins = with pkgs.vimPlugins;[
          nvim-tree-lua
          vim-commentary
          vim-surround
          vim-visual-multi
          orgmode
          vim-table-mode
          onedark-nvim
        ];
      };

      autojump = {
        enable = true;
        enableFishIntegration = true;
        enableBashIntegration = true;
      };
    };

    home.packages = with pkgs; [
      idasen
      # Basics
      fish
      tmux # terminal muliplexer

      # Vifm + dependencies
      vifm
      curlftpfs
      sshfs-fuse
      chafa

      # Core-utils replacements
      eza # better ls
      bat # better cat
      fd # better find
      sd # better sed
      ripgrep # best (?) grep
      cloc
      du-dust
      # ripgrep-all # rg for all documents. a lot of dependencies.
      trash-cli # better rm... well no but yes
      gitAndTools.delta #better diff
      difftastic #even better diff?


      # Converting text/docs
      pandoc # uber document converter
      catdocx
      poppler_utils # pdftoppm, pdftotext, pdftohtml
      odt2txt
      jq # json manipulator  - still used on arch
      gron # json => lines
      libxml2 # xml formatter - xmllint
      xmlstarlet # xml maipulator
      pup # html manipulator
      readability-cli # simplify those webpages
      skim # rust fuzzy finder
      fzy # minimalist fuzzy finder
      up # ultimate plumber

      # Archives
      atool # manages and gives information about archives.
      zip
      unzip
      p7zip
      rsync

      # System info
      htop
      btop
      neofetch # Represent!
      # nmon
      # atop



      # Toys
      nushell
      starship

      # Misc
      scc
      bruno # FIXME not a terminal app

      asciinema # record commandline
      translate-shell # CLI google translate

      termdown # terminal stopwatch/timer
      entr # file watcher



      (lib.mkIf cfg.diagrams plantuml)
      (lib.mkIf cfg.diagrams (mermaid-cli.override { chromium = pkgs.brave; }))
      (lib.mkIf cfg.diagrams graphviz)
      (lib.mkIf cfg.jira jira-cli-go)

      fava
    ];

  };
}
