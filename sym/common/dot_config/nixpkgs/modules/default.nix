{
  imports = [
    ./ai
    ./browser
    ./dev
    ./emacs
    ./fonts
    ./for-nixos
    ./gui
    ./languages
    ./media
    ./network
    ./terminal
    ./wayland
    ./xorg
  ];
}
