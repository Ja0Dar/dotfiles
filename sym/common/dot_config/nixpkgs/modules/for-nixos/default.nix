{ config, lib, pkgs, ... }:
let cfg = config.for-nixos;
in
{
  options.for-nixos = {
    enable = lib.mkEnableOption "Enable for-nixos module.";
  };

  config = lib.mkIf cfg.enable {

    xdg.configFile."tmux.os.conf".text = ''
      set-option -g default-shell ~/.nix-profile/bin/fish
    '';

    home.packages = with pkgs; [
      file
      xdg-utils
      lsof
      bind
      killall
      v4l-utils

      networkmanagerapplet
      hicolor-icon-theme # For networkmanagerapplet icon
      adwaita-icon-theme

      podman-compose

      # Xorg-heavy
      geoipWithDatabase # geoiplookup for statusbar modules

      # Archives
      bzip2

      # # TODO:bcm  - move
      xorg.xwininfo
      # Imgs
    ];

  };
}
