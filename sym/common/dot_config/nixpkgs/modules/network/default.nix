{ config, lib, pkgs, ... }:
let cfg = config.network;
in
{
  options.network = { enable = lib.mkEnableOption "Enable network module."; };

  config = lib.mkIf cfg.enable {
    home.packages = with pkgs; [

      # Network
      # wireshark # FXIME - not exactly cli
      # ngrok
      netcat-openbsd
      # httpie # better curl
      # socat # network piping
      # websocat
      # there is also wscat - more user friendly - `npx wscat`

      # # FIXME: remove after https://github.com/NixOS/nixpkgs/pull/266855#issuecomment-1858027279 is no longer relevant
      (winbox.override { wine = wineWowPackages.waylandFull; })
    ];

  };
}
