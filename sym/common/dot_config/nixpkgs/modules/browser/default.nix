{ config, lib, pkgs, ... }:
let cfg = config.gui.browser;
in
{
  options.gui.browser = {
    chromium = lib.mkEnableOption "Enable chromium.";
    firefox = lib.mkEnableOption "Enable friefox.";
    nyxt = lib.mkEnableOption "Enable nyxt.";
    tor = lib.mkEnableOption "Enable tor browser.";
  };

  config = {

    # FXIME use NIXOS_OZONE_WL?
    nixpkgs.config.chromium.commandLineArgs =
      (if config.wayland.enable then "--enable-features=UseOzonePlatform --ozone-platform=wayland"
      else
        "");
    home.packages = with pkgs; [

      #Browsers
      mullvad-browser

      # (lib.mkIf cfg.tor torbrowser)
      (lib.mkIf (cfg.nyxt) (jlib.glWrap pkgs { bin = "nyxt"; }))

      (lib.mkIf (cfg.firefox) (makeDesktopItem {
        name = "firefox-hm-temp";
        desktopName = "Firefox for burp (hm-tmp)";
        genericName = "Open a Firefox window";
        icon = "firefox";
        exec = "firefox --profile /home/owner/.mozilla/firefox/hm-tmp";
        categories = [ "Network" ];
      }))
    ];

    programs = {
      chromium = {
        enable = cfg.chromium;
        # Extensions are updated via switching package to 'chromium' and opening chrome://extensions
        package = (pkgs.jlib.glWrap pkgs { bin = "brave"; });
        extensions = [
          # { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; } # uBlock Origin - https://github.com/gorhill/uBlock
          # { id = "dbepggeogbaibhgnhhndojpepiihcmeb"; } # Vimium - https://github.com/philc/vimium
          { id = "gfbliohnnapiefjpjlpjnehglfpaknnc"; } # Surfingkeys
          # { id = "gcbommkclmclpchllfjekcdonpmejbdp"; } # HTTPS Everywhere - https://github.com/EFForg/https-everywhere
          # "einpaelgookohagofgnnkcfjbkkgepnp" # Random User-Agent - https://github.com/tarampampam/random-user-agen
        ];
      };

      firefox = {
        enable = cfg.firefox;
        package = (if config.wayland.enable then pkgs.firefox-wayland else (pkgs.jlib.glWrap pkgs { bin = "firefox"; package = pkgs.firefox-bin; }));



        profiles = {
          hm-default = {
            id = 0;

            # HACK - avoid overwriting user.js
            settings = { };
            extraConfig = "";
            bookmarks = { };
            extensions.packages = with pkgs.nur.repos.rycee.firefox-addons; [
              ### Privacy:
              ublock-origin
              # facebook-container
              keepassxc-browser
              # redirector
              multi-account-containers
              temporary-containers

              ### Usability:
              # shodan
              darkreader
              simple-tab-groups
              surfingkeys
              polish-dictionary

              ### Dumped
              # chameleon
              # cookie-autodelete
              # canvasblocker
            ];
          };

          hm-tmp = {
            id = 1;

            extensions.packages = with pkgs.nur.repos.rycee.firefox-addons; [
              ### Privacy:
              ublock-origin
              multi-account-containers
              temporary-containers
              polish-dictionary
              bitwarden
            ];

            settings = {
              "signon.rememberSignons" = false;
              "extensions.pocket.enabled" = false;
              "browser.newtabpage.activity-stream.section.highlights.includePocket" = false;
              "keyword.enabled" = false; # Disable search bar
            };

            search = {
              force = true;
              engines = {
                "Startpage" = {
                  urls = [{
                    template = "https://startpage.com/do/search?query={searchTerms}";
                  }];
                  definedAliases = [ "s" ];
                };
                "Brave" = {
                  urls = [{
                    template = "https://search.brave.com/search?q={searchTerms}";
                  }];
                  definedAliases = [ "b" ];
                };
              };
            };
          };


        };
      };
    };
  };
}
