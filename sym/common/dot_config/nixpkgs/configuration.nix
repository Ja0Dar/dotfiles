# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
inputs: overlays:
{ config, pkgs, ... }:

# TODO: modularize some day

let
  sddm-theme = import ./packages/simple-sddm/default.nix { inherit pkgs; inherit config; lib = pkgs.lib; };
  cfg = config.os;

in
{

  options.os = {
    android = pkgs.lib.mkEnableOption "Enable android part.";
  };


  config = {

    nix =
      {
        nixPath = [ "nixpkgs=${inputs.nixpkgs}" ];
        package = pkgs.nixVersions.stable;
        extraOptions = ''
          experimental-features = nix-command flakes
        '';

        settings.auto-optimise-store = true;
        settings.trusted-users = [ "root" "owner" "@wheel" ];
      };

    # Use the systemd-boot EFI boot loader.
    nixpkgs = {
      config.allowUnfree = true;

      # NOTE: https://discourse.nixos.org/t/do-flakes-also-set-the-system-channel/19798/5 fix command-not-found
      inherit overlays;
      # overlays = overlays ++ [
      #   (self: super: {
      #     programs_sqlite = super.runCommandLocal "programs_sqlite" { } ''cp ${inputs.nixos-channel}/programs.sqlite $out '';
      #   })
      # ];
    };

    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;

    networking.networkmanager.enable = true;
    systemd.services.NetworkManager-wait-online.enable = false;

    # Set your time zone.
    time.timeZone = "Europe/Warsaw";



    i18n.defaultLocale = "en_US.UTF-8";
    console = { keyMap = "pl"; };

    environment.etc = {
      "xdg/gtk-2.0/gtkrc".text = "gtk-application-prefer-dark-theme=1";
      "xdg/gtk-3.0/settings.ini".text = ''
        [Settings]
        gtk-application-prefer-dark-theme=1
      '';
      "xdg/gtk-4.0/settings.ini".text = ''
        [Settings]
        gtk-application-prefer-dark-theme=1
      '';
    };

    environment.systemPackages = with pkgs; [
      ghostscript

      # SDDM theme
      libsForQt5.qt5.qtquickcontrols2
      libsForQt5.qt5.qtgraphicaleffects
      libsForQt5.qt5.qtsvg
      nerd-fonts.jetbrains-mono

      (lib.mkIf (cfg.android) android-tools)

      upower
      # exfatprogs
      ntfsprogs
      gnumake
      gcc
      wget
      git
      pulseaudio
      pinentry-qt
      swaynotificationcenter
      libnotify

      # Won't work well in home manager
      networkmanager
      swaylock
    ];

    programs.kdeconnect.enable = true;

    programs.nh = {
      enable = true;
      clean.enable = false;
      clean.extraArgs = "--keep-since 4d --keep 3";
      flake = "/home/owner/git/dotfiles/sym/common/dot_config/nixpkgs";
    };

    # https://unix.stackexchange.com/questions/522822/different-methods-to-run-a-non-nixos-executable-on-nixos
    programs.nix-ld = {

      enable = true;
      libraries = with pkgs;[

        libelf

        # Required
        glib
        expat

        # From baseLibraries
        zlib #for scala-cli
        stdenv.cc.cc
        openssl
        libsodium
        util-linux
        xz
        systemd

      ];
    };

    programs.gnupg.agent = {
      enable = true;
      pinentryPackage = pkgs.pinentry-qt;
      # enableSSHSupport = false;
    };

    programs.ssh.startAgent = true;


    security.polkit.enable = true;
    security.sudo.extraConfig = ''
      Defaults timestamp_type=global
    '';

    services = {
      gvfs.enable = true;
      earlyoom.enable = true;
      logind = {
        powerKey = "suspend";
        lidSwitch = "suspend";
        lidSwitchExternalPower = "suspend";
        lidSwitchDocked = "suspend";
      };

      auto-cpufreq.enable = true;
      # mullvad-vpn.enable = true;
      gnome.gnome-keyring.enable = true;
      pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
      };
    };

    # TODO ONLY for SWAY?
    systemd.user.services."swaync" = {
      enable = true;
      description = "";
      wantedBy = [ "default.target" ];
      serviceConfig.Restart = "always";
      serviceConfig.RestartSec = 2;
      serviceConfig.ExecStart = "${pkgs.swaynotificationcenter}/bin/swaync";
    };

    # networking.extraHosts = pkgs.lib.fileContents ../../secrets/hosts.txt;


    services.displayManager.sddm = {
      enable = true;
      wayland.enable = true;
      enableHidpi = true;
      theme = "${sddm-theme}";
    };



    programs.sway = {
      enable = true;
      wrapperFeatures.gtk = true; # so that gtk works properly
      extraPackages = with pkgs; [
        swaylock # Wont work with home-manager https://github.com/NixOS/nixpkgs/issues/158025
      ];
    };

    # TODO Make it configurable in config.nix
    programs.hyprland = {
      withUWSM = true;
      enable = true;
    };


    programs.light.enable = true;
    virtualisation = {
      podman = {
        enable = true;
        dockerCompat = true; # `docker` alias for podman
        defaultNetwork.settings.dns_enabled = true; # for podman-compose containers to be able to talk to each other.
      };

      # QEMU/KVM
      libvirtd.enable = false;
      lxd.enable = true;
    };


    services.printing = {
      enable = true;
      drivers = with pkgs;[ gutenprint ];
    };


    # useful for reading bat status of wacom, not sure if for anything else
    services.upower.enable = true;

    # Enable graphics.
    hardware.graphics = {
      enable = true;
    };

    # For calibre
    services.udisks2.enable = true;

    services.clamav = {
      daemon.enable = true;
      updater.enable = false;
    };


    services.blueman.enable = true;
    hardware.bluetooth = {
      enable = true;
      powerOnBoot = true;
    };

    # Enable touchpad support (enabled default in most desktopManager).
    services.libinput.enable = true;
    services.fwupd.enable = true;

    # FIXME Not sure how does it compare to hyrpland
    xdg =
      {
        portal = {
          enable = true;
          extraPortals = with pkgs; [
            xdg-desktop-portal-wlr
            xdg-desktop-portal-gtk
          ];
        };
      };


    environment.sessionVariables = {
      MOZ_ENABLE_WAYLAND = "1";
      # XDG_CURRENT_DESKTOP = "sway";
    };


    # Define a user account. Don't forget to set a password with ‘passwd’.
    users.groups = { uinput = { }; kmonad = { }; };
    users.users.owner = {
      isNormalUser = true;
      home = "/home/owner";
      createHome = true;
      shell = pkgs.bash;
      useDefaultShell = true;
      extraGroups = [
        "wheel" # Enable ‘sudo’ for the user.
        "video" # for 'light' to work
        "libvirtd"
      ];
    };

    users.users.kmonad =
      {
        isNormalUser = true;
        createHome = false;
        group = "kmonad";
        extraGroups = [
          "uinput"
          "input"
        ];
      };


    # https://docs.qmk.fm/#/faq_build?id=linux-udev-rules + kmonad
    services.udev.packages = with pkgs; [ qmk-udev-rules yubikey-personalization android-udev-rules ];
    services.udev.extraRules = ''
      # Atmel DFU
      ### ATmega32U4
      SUBSYSTEMS=="usb", ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="2ff4", TAG+="uaccess"

      # KMonad user access to /dev/uinput
      KERNEL=="uinput", MODE="0660", GROUP="uinput", OPTIONS+="static_node=uinput"
    '';
    # Some programs need SUID wrappers, can be configured further or are
    # started in user sessions.
    # programs.mtr.enable = true;
    # programs.gnupg.agent = {
    #   enable = true;
    #   enableSSHSupport = true;
    # };

    fonts = {
      fontDir.enable = true;
      enableGhostscriptFonts = true;
      # fonts = with pkgs; [
      #   ubuntu_font_family
      #   # noto-fonts-emoji # already included 🤷‍
      #   # siji # Great glyphs for polybar etc
      # ];
      fontconfig.defaultFonts = {
        sansSerif = [ "Linux Bolinum" ];
        monospace = [ "Hack" ];
      };
    };


    services.flatpak.enable = true;
    # Enable the OpenSSH daemon.
    networking.firewall.enable = true;
    services.openssh.enable = false;
    networking.firewall.allowedTCPPorts = [ ];
    # Open ports in the firewall.
    # Or disable the firewall altogether.
    networking.firewall.allowedUDPPorts = [
      5678 # Winbox
    ];



  };

}
