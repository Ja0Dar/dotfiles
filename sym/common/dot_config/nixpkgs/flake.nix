{
  description = "JakDar nix config";

  inputs = {
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    nur.url = "github:nix-community/NUR";


    nix-index-database = {
      url = "github:nix-community/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixGL = {
      url = "github:guibou/nixGL/main";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    zowier = {
      url = "git+ssh://git@github.com/chatbotizeteam/zowier.git?ref=main";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    spicetify-nix = {
      url = "github:Gerg-L/spicetify-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    scalaproto = {
      url = "github:JakDar/scalaproto";
      flake = false;
    };

    veracrypt-nixpkgs.url =
      "github:nixos/nixpkgs?rev=a27871180d30ebee8aa6b11bf7fef8a52f024733";

    pinned-nixpkgs.url =
      "github:nixos/nixpkgs?rev=5b2753b0356d1c951d7a3ef1d086ba5a71fff43c";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = { self, ... }@inputs:
    let
      system = "x86_64-linux";
      pin-overlay = final: prev: {
        #NOTE: What a shishow this commit is :D
        whisper-ctranslate2 = inputs.pinned-nixpkgs.legacyPackages.${system}.whisper-ctranslate2;
        mullvad-browser = inputs.pinned-nixpkgs.legacyPackages.${system}.mullvad-browser;
        fava = inputs.pinned-nixpkgs.legacyPackages.${system}.fava;
        eslint = inputs.pinned-nixpkgs.legacyPackages.${system}.eslint;
        bitwarden-cli = inputs.pinned-nixpkgs.legacyPackages.${system}.bitwarden-cli;

        veracrypt = inputs.veracrypt-nixpkgs.legacyPackages.${system}.veracrypt;
        # hello is a placeholder ;)
        hello = inputs.pinned-nixpkgs.legacyPackages.${system}.hello;
        # open-interpreter = inputs.pinned-nixpkgs.legacyPackages.${system}.open-interpreter;
        # calibre = inputs.pinned-nixpkgs.legacyPackages.${system}.calibre;
      };
      mkHomeConfig = (import ./lib inputs).mkHomeConfig;

      # FIXME - can get rid of this on nixos?
      overlays = [
        inputs.nur.overlays.default
        pin-overlay

        (self: super:
          let
            mpv-unwrapped-full = super.mpv-unwrapped.override { ffmpeg = super.ffmpeg-full; };
          in
          {
            mpv = super.mpv-unwrapped.wrapper {
              mpv = mpv-unwrapped-full;
              scripts = with self.mpvScripts; [ mpris ];
            };

            jre = super.jdk21;
            jdk = super.jdk21;

            zowier = inputs.zowier.packages.x86_64-linux.zowier;
            zap = (super.callPackage ./packages/zap { });
            # Lets try to use the nixpkgs version for now
            # metals = (super.callPackage ./packages/metals { });
            easy-ocr-cli = (super.callPackage ./packages/easy-ocr-cli { });
            nixGLIntel = (super.callPackage "${inputs.nixGL}/nixGL.nix" { }).nixGLIntel;

            scalaproto = (super.callPackage "${inputs.scalaproto}/package-bin.nix" { });
          })
      ];
    in
    {
      homeConfigurations = {
        t14 = mkHomeConfig "owner" ./hosts/t14/home-config.nix overlays;
        "owner@t14" = mkHomeConfig "owner" ./hosts/t14/home-config.nix overlays; # make nh happy
        "owner@yoga" = mkHomeConfig "owner" ./hosts/yoga/home-config.nix overlays; # make nh happy
      };

      nixosConfigurations = {
        t14 = inputs.nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            (import ./hosts/t14/host-specific-configuration.nix inputs overlays)
            (import ./configuration.nix inputs overlays)
            inputs.nixos-hardware.nixosModules.lenovo-thinkpad-t14

            # FIXME: make it work with fish https://github.com/nix-community/nix-index-database?tab=readme-ov-file
            inputs.nix-index-database.nixosModules.nix-index
          ];
        };

        yoga = inputs.nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            (import ./hosts/yoga/host-specific-configuration.nix inputs overlays)
            (import ./configuration.nix inputs overlays)
            # FIXME: make it work with fish https://github.com/nix-community/nix-index-database?tab=readme-ov-file
            inputs.nix-index-database.nixosModules.nix-index
          ];
        };
      };


      t14 = self.homeConfigurations.t14.activationPackage;
      yoga = self.homeConfigurations.yoga.activationPackage;
    };
}
