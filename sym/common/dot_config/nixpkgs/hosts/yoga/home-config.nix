{ pkgs, ... }:

{
  config = {

    ai.enable = true;
    wayland.enable = true;
    terminal = {
      enable = true;
      diagrams = true;
    };
    emacs = {
      enable = true;
      pgtk = true;
    };
    gui = {
      enable = true;
      redundant = true;
      minecraft = false;
      office = true;
      calibre = true;

      browser = {
        firefox = true;
        chromium = true;
        nyxt = false;
        tor = false;
      };

      non-essential = true;
    };
    media = {
      enable = true;
      bloat = true;
      editors = false;
    };
    network.enable = true;
    dev = {
      enable = true;
      burp = true;
      zap = true;
      zw = false;
    };

    for-nixos.enable = true;

    languages = {
      nix.enable = true;
      frontend.enable = true;
      latex.enable = true;
      python = {
        enable = true;
        dbs = false;
      };
      dbs.enable = false;
      scala.enable = true;
      rust.enable = true;
      shell.enable = true;
    };

    fonts.enable = true;
  };
}
