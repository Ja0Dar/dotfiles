# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
inputs: overlays:
{ config, pkgs, ... }:
{

  # Kuba-managed
  os = {
    android = true;
  };

  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  networking.hostName = "yoga";


  services.tailscale = {
    enable = true;
    useRoutingFeatures = "client";
  };

  # https://github.com/tailscale/tailscale/issues/4432#issuecomment-1112819111
  networking.firewall.checkReversePath = "loose";


  boot.initrd.luks.devices = {
    crypted = {
      device = "/dev/disk/by-uuid/ddb8287d-9dcd-4dc6-ade9-a5e5a1c6c7a6";
      preLVM = true;
    };
  };

  boot.kernelParams = [ "processor.max_cstate=4" "amd_iomu=soft" "idle=nowait" ];

  boot.kernelPackages = pkgs.linuxPackages_latest;


  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
}
