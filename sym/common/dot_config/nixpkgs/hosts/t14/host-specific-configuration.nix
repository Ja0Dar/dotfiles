# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
inputs: overlays:
{ config, pkgs, ... }:

{


  # Kuba-managed
  os = {
    android = false;
  };


  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  networking.hostName = "t14";

  services.tailscale = {
    enable = true;
    useRoutingFeatures = "client";
  };

  boot.initrd.luks.devices = {
    crypted = {
      device = "/dev/disk/by-uuid/06db72ff-9092-4973-8cbc-8482da9c1170";
      preLVM = true;
    };
  };




  boot.extraModprobeConfig = ''
    options kvm_intel nested=1
    options kvm_intel emulate_invalid_guest_state=0
    options kvm ignore_msrs=1
  '';


  boot.kernelParams = [ "intel_idle.max_cstate=4" ];
  boot.kernelPackages = pkgs.linuxPackages;
  networking.extraHosts = pkgs.lib.fileContents ../../secrets/hosts.txt;

  virtualisation = {
    waydroid.enable = true;
  };


  # But I will start waydroid on my own
  systemd.services.waydroid-container = {
    enable = false;
    restartIfChanged = false;
  };

  hardware.graphics.extraPackages = with pkgs; [ intel-compute-runtime ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
