entrypoint:
{ pkgs, config, lib, inputs, ... }:
let
  spicePkgs = inputs.spicetify-nix.legacyPackages.${pkgs.system};
  cfg = config.media;
in
{
  # allow spotify to be installed if you don't have unfree enabled already
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "spotify"
  ];

  # import the flake's module for your system
  imports = [ inputs.spicetify-nix.homeManagerModules.default entrypoint ];


  # configure spicetify :)
  programs.spicetify =
    {
      enable = cfg.enable;
      theme = spicePkgs.themes.default;
      colorScheme = "mocha";

      enabledExtensions = with spicePkgs.extensions; [
        keyboardShortcut
        hidePodcasts
        trashbin
      ];
    };
}
