{ home-manager, nixpkgs, ... }@inputs:

username: entrypoint: overlays:

let
  system = "x86_64-linux";
  pkgs = (import nixpkgs { inherit system; });
  config = (import entrypoint pkgs).config;
  additionalOverlays = [ (self: super: { jlib = import ./jlib { inherit config; }; }) ];
  spicetifyModule = (import ./spicetify.nix entrypoint);
in


home-manager.lib.homeManagerConfiguration {
  # inherit system;
  pkgs = nixpkgs.legacyPackages.x86_64-linux;
  extraSpecialArgs = { inherit inputs; };

  modules =
    [
      {

        xdg.configFile."nix/nix.conf".source = ../configs/nix/nix.conf;
        nixpkgs.config = import ../configs/nix/config.nix;
        nixpkgs.overlays = overlays ++ additionalOverlays;
        imports = [ ../home.nix entrypoint ];
      }

      {
        home = {
          inherit username;
          homeDirectory = "/home/${username}"; # fixme dont duplicate it in home.nix
          # This value determines the Home Manager release that your
          # configuration is compatible with. This helps avoid breakage
          # when a new Home Manager release introduces backwards
          # incompatible changes.
          #
          # You can update Home Manager without changing this value. See
          # the Home Manager release notes for a list of state version
          # changes in each release.
          stateVersion = "21.03";
        };
      }
      spicetifyModule

    ];
}
