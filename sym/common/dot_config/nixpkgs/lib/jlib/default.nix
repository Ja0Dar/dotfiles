{ config, ... }@inputs:
# stolen from  https://github.com/NobbZ/nix-dotfiles/blob/main/lib/default.nix

{
  glWrap = import ./gl_wrap.nix inputs;
  wrapPassingShare = import ./wrap_passing_share.nix inputs;
  javaFontWrap = import ./java_font_wrap.nix inputs;
}
