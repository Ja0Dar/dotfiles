{ config, ... }:
pkgs: { bin, package ? pkgs."${bin}" }:

pkgs.writeShellScriptBin "${bin}" ''
  #!/usr/bin/env sh
  export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dsun.java2d.uiScale=1.4'
  ${package}/bin/${bin} "$@"
''
