{ config, ... }:
pkgs: { bin, package ? pkgs."${bin}", script }:
pkgs.callPackage
  (inputs:
  let pkg = (package.override inputs);
  in
  pkgs.stdenv.mkDerivation {
    name = "${bin}";
    pname = "${bin}";
    phases = [ "installPhase" ];
    installPhase = ''
      mkdir -p "$out/bin" "$out/share"
      # cp -pRL "${pkg}/share" "$out/"

      for f in '${pkg}'/share/*; do
        ln -s -t "$out/share/" "$f"
      done

      cat >> "$out/bin/${bin}" << EOF
      ${script}
      EOF
      chmod u+x  "$out/bin/${bin}"
    '';
  })
{ }
