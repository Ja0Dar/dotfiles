{
  allowUnfree = true;
  keep-derivations = true;
  keep-outputs = true;
  permittedInsecurePackages = [
    "electron-27.3.11"
  ];
}
