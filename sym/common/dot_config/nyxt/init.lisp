(define-configuration (buffer web-buffer prompt-buffer)
    ((default-modes (append '(nyxt::vi-insert-mode) %slot-default%))))

(define-configuration web-buffer
  ((default-modes (append
                   '(;;auto-mode
                     ;; emacs-mode
                     ;; noscript-mode
                     blocker-mode
                     reduce-tracking-mode)
                   %slot-default%))))

(defmethod nyxt::startup ((browser browser) urls)
  "Home"
  (window-make browser)
  (let ((window (current-window)))
    (window-set-buffer window (make-buffer :url (quri:uri "https://ddg.gg")))))


(define-configuration buffer
    ((search-engines
      (list
      (make-instance 'search-engine
                      :shortcut "gh"
                      :search-url "https://github.com/search?q=~a"
                      :fallback-url "https://github.com/trending")
       (make-instance 'search-engine
                      :shortcut "yt"
                      :search-url "https://invidious.snopyta.org/search?q=~a"
                      :fallback-url "https://invidious.snopyta.org/")
       (make-instance 'search-engine
                      :shortcut "so"
                      :search-url "https://stackoverflow.com/search?q=~a"
                      :fallback-url "https://stackoverflow.com/")
       (make-instance 'search-engine
                      :shortcut "w"
                      :search-url "https://www.wikipedia.org/w/index.php?title=Special:Search&search=~a"
                      :fallback-url "https://www.wikipedia.org/")
       (make-instance 'search-engine
                      :shortcut "s"
                      :search-url "https://www.startpage.com/search?q=~a"
                      :fallback-url "https://www.startpage.com")))))


(load (nyxt-init-file "emacs.lisp"))
