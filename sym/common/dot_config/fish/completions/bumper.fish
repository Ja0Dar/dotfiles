#!/usr/bin/env fish

complete -c bumper -n 'not __fish_seen_argument -l no-pull'  -l no-pull -d "Dont pull before aplying changes"
complete -c bumper -f

complete -c fbump -n 'not __fish_seen_argument -l no-pull'  -l no-pull -d "Dont pull before aplying changes"
complete -c fbump -f
