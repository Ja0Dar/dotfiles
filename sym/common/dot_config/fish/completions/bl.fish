# complete -c bl -n '__fish_seen_argument -s o' -s o -d "only"
# complete -c bl -n 'not __fish_seen_argument -s z' -s z -d "Find specific test case by name."
# complete -c bl -l noi -d "Non-incremental compile."


complete  -c bl -s e -l ensure -d 'Ensure correct javahome'
complete  -c bl -s p -l print -d 'Print command instead of executing it'
complete  -c bl -s o -l only -d 'Run only one test'
complete  -c bl -s z -d 'Run only one test interactively'
complete  -c bl -s t -l test -d 'Run tests'
complete  -c bl -s r -l run -d 'Run main method'
complete  -c bl -s w -l watch -d 'Watch for changes'
complete  -c bl -s noi -l noincremental -d 'Disable incremental compilation'
complete  -c bl -s h -l help -d 'Print this help'
complete -c bl -f
