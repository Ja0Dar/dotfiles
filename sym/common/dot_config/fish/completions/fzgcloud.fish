#!/usr/bin/env fish

set -l modes "list start stop cache"
complete -c fzgcloud -n 'not __fish_seen_argument -s f' -s f -l force -d "Force retrieving instance lsit."
complete -c fzgcloud -n "not __fish_seen_subcommand_from $modes" -a $modes
complete -c fzgcloud -n "not __fish_seen_subcommand_from $modes" -w ssh
complete -c fzgcloud -f
