#!/usr/bin/env fish

set -l modes "in out all"
set -l turns "on off"
complete -c noise_reduce -n "not __fish_seen_subcommand_from $modes" -a $modes
complete -c noise_reduce -n "not __fish_seen_subcommand_from $turns" -a $turns
complete -c noise_reduce -f
