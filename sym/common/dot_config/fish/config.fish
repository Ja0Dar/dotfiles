
[ -f "$HOME/.config/fish/nix.fish" ]
and source "$HOME/.config/fish/nix.fish" or "Echo couldn't source nix"

if command -v starship > /dev/null
    starship init fish | source
end

# set my_prompt "$HOME/git/foss/fish/theme-es/fish_prompt.fish"
# if [ -f $my_prompt ]
#     source $my_prompt
# end

# Let's try vanilla bindings for a while, I dont use vim too much either way
# set -g fish_key_bindings fish_hybrid_key_bindings


function fish_greeting --description 'Initial message'
end

set -g fish_color_command '#51afef' # from doom
set -l mode 6 # bar steady

if [ "$fish_key_bindings" = fish_hybrid_key_bindings ] || [ "$fish_key_bindings" = fish_vi_key_bindings ]
    bind -M insert \cx edit_cmd
    bind -M insert \e\[1\;3A 'cd ..; commandline -f repaint' # Alt-up
    bind -M insert \e\[1\;4A 'cd ..; echo "ls ..";ls; commandline -f repaint;' # Alt-shift-up
    bind -M insert \cH backward-kill-word
    bind -M insert \cr history_sk
else
    bind \cx edit_cmd
    bind \e\[1\;3A 'cd ..; commandline -f repaint' # Alt-up
    bind \e\[1\;4A 'cd ..; echo "ls ..";ls; commandline -f repaint;' # Alt-shift-up
    bind \cH backward-kill-word
    bind \cr history_sk
end


# Git

abbr --add gst git status
alias gste='TERM=xterm-direct emacsclient -create-frame --tty -a ""  --eval "(progn (magit-status) (delete-other-windows))"'
abbr --add ga git add
abbr --add gaA git add -A

abbr --add gc git commit
abbr --add gb git branch

abbr --add gco git checkout
abbr --add gcb git checkout -b
abbr --add gcm git checkout master

abbr --add gdca git diff --cached

abbr --add grh git reset
abbr --add grhh git reset --hard

alias currentMillis="sh -c \"date +'%s%N'  | head -c -7\""
abbr --add dateymd date -u +"%Y_%m_%d"

# Git
alias glod='git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset"'
abbr --add gsu git submodule update --remote --init
abbr --add ggh 'cd (git rev-parse --show-toplevel)'
abbr --add ggp 'cd (project_root -g)'
alias gpsup="git push --set-upstream origin (git branch| grep '*'| sed s'/* //')"
# git remote prune origin
alias gbda="git branch --no-color --merged | command grep -vE '^(\*|\s*(master|develop|dev|release)\s*\$)' | command xargs -n 1 git branch -d"
alias gpr="git pull --rebase"



alias ls="eza"
alias ll="eza -lh"
alias la="eza -lah"
abbr --add tree 'eza -T'
alias t='trash'
abbr --add rm  trash
abbr --add zf 'zathura --fork'
abbr --add upf up --unsafe-full-throttle

# Rest
abbr --add p1 ping 1.1
abbr --add p podman
abbr --add b boiler

# Htop
abbr --add hmem htop --sort-key PERCENT_MEM
abbr --add hcpu htop --sort-key PERCENT_CPU

abbr --add unfreeze_emacs 'pkill -SIGUSR2 emacs' # https://emacs.stackexchange.com/a/21645

set -x DIRENV_LOG_FORMAT ""

source ~/.config/aliases/base.sh


if [ -f ~/.config/fabric/fabric-bootstrap.inc ]
    source ~/.config/fabric/fabric-bootstrap.inc
end
