function fzt -d "Switch tmux session"
    tmux list-sessions -F "#{session_name}" | sk | read -l result
    and tmux switch-client -t "$result"
end
