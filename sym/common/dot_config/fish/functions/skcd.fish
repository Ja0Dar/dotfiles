#!/usr/bin/env fish

function skcd --description 'Cd into subdirectory.'
    set -l directory (fd -t d --max-depth 3 | sk --header="Directory to cd into:")
    if [ -n "$directory" ]
        cd "$directory"
    else
        echo >&2 "No directory selected."
    end
end
