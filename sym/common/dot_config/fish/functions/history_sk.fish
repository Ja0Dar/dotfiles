#!/usr/bin/env fish
function history_sk --description 'Pretty history'

    set -l prompt (commandline -b)
    set -l data (history | awk "{if(length < 400){print}}"\
        | sk --preview 'echo {} | fold -s | awk -vs="\\\\\\\\" "{if(nr==1){printf \"%s\", \$0} else { print s; printf \"%s\", \$0 }}"' \
        --preview-window "down:20%:wrap" --algo=clangd -q "$prompt")

    if [ -n "$data" ]
        commandline -r "$data"
        commandline -C 500
    end

end



