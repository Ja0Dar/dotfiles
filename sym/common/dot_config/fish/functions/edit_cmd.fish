#!/usr/bin/env fish
function edit_cmd --description 'Edit cmdline in editor'
    set -l f (mktemp --tmpdir=/tmp).fish
    commandline -b >$f

    set -l emacs_major (emacs --version | head -n 1 | awk '{split($NF,a,"."); print a[1]}')

    # not working on gccemacs, so for 28+ we use regular ec
    if [ $emacs_major -lt 28 ]
        # Use emacs daemon to speedup that 230ms startup <3
        if ! emacsclient -t --socket-name=line $f 2>/dev/null
            ec --eval "(setq confirm-kill-emacs nil)" --bg-daemon=line 2>/dev/null
            emacsclient -t --socket-name=line $f
        end
    else
        ec --eval "(setq confirm-kill-emacs nil)" $f
    end
    # nvim -c set\ ft=fish $f
    commandline -r (cat $f)
    commandline -f repaint
    commandline -C 500

    rm $f
end
