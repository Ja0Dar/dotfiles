#!/usr/bin/env fish

function cdp --description 'Cd into (sub)project.'
    set -l project (project_root -f | sk --header="Project to cd into:" -q "$argv")
    if [ -n "$project" ]
        cd $project
    else
        echo >&2 "No project selected."
    end
end
