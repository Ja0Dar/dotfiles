function fbump --description 'Run bumper and cd into directory'
    set -l bump_out (bumper $argv)

    if [ $status = 0 ]
        set -l bump_dir $bump_out[-1]
        set -l cmd "cd $bump_dir && git diff --cached"
        eval "$cmd"

        echo "Ran:"
        echo "$cmd"
    else
        echo $bump_out
    end
end
