if command -v direnv >/dev/null
    eval (direnv hook fish)
end

if contains "$HOME/.nix-profile" $PATH

    set -x NIX_LINK "$HOME/.nix-profile"
    # echo $NIX_LINK

    set -x NIX_PATH "$NIX_PATH:$HOME/.nix-defexpr/channels"

    # Set up environment.
    # This part should be kept in sync with nixpkgs:nixos/modules/programs/environment.nix
    set -x NIX_PROFILES "/nix/var/nix/profiles/default $HOME/.nix-profile"

    # Set $NIX_SSL_CERT_FILE so that Nixpkgs applications like curl work. (Arch)
    set -x NIX_SSL_CERT_FILE "/etc/ssl/certs/ca-certificates.crt"

    if [ -n "$MANPATH" ]
        set -x MANPATH "$NIX_LINK/share/man:$MANPATH"
    end

    set -x PATH "$NIX_LINK/bin:$PATH"
    set -u NIX_LINK
end

alias ns="nix-shell --run fish"
