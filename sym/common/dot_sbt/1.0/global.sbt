addCommandAlias("r","reload")
addCommandAlias("c","compile")
addCommandAlias("proto","protocGenerate")
addCommandAlias("pb",";protocGenerate;compile;bloopInstall")
addCommandAlias("optimizeImports","scalafix RemoveUnused")

addCommandAlias("fixEverything", ";scalafixEnable;scalafix;test:scalafix;scalafmtAll")
addCommandAlias("scalafixAll", ";scalafixEnable;scalafix;test:scalafix")

shellPrompt := { state =>
  "\u001b[38;5;10m:sbt: \u001b[38;5;13m%s \u001b[38;5;9m🚀 \u001b[38;5;10m::\u001b[38;5;15m ".format(Project.extract(state).currentProject.id.replace("-service","").replace("gateway","gw"))
}
