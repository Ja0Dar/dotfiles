// Check updates
addSbtPlugin("com.timushev.sbt" % "sbt-updates"         % "0.6.2")

// For removing unused dependencies
addSbtPlugin("com.github.cb372" % "sbt-explicit-dependencies" % "0.2.16")

// Owasp dependency check for sbt?
// addSbtPlugin("net.vonbuchholtz" % "sbt-dependency-check" % "3.3.0")

// Restart app after changes
// addSbtPlugin("io.spray" % "sbt-revolver" % "0.9.1")

// Read .env files
// addSbtPlugin("au.com.onegeek" %% "sbt-dotenv" % "2.1.233")

// Plot dependency graphs
// addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.10.0-RC1")

credentials += Credentials(Path.userHome / ".sbt" / ".credentials")
