#!/usr/bin/env sh
# Syncs repositories and downloads updates, meant to be run as a cronjob.

setup_colors() {
    if [ -t 2 ] && [ -z "${NO_COLOR-}" ] && [ "${TERM-}" != "dumb" ]; then
        NOFORMAT='\033[0m' GREEN='\033[0;32m' GRAY='\033[0;37m'
    else
        NOFORMAT='' GREEN='' GRAY=''
    fi
}

check_doom() {
    emacs_dir="$HOME/.emacs.d"
    if ! [ -d "$emacs_dir" ]; then
        emacs_dir="$HOME/.config/emacs"
    fi
    remote_name="origin"
    branch_name="master"

    if ! [ -d "$emacs_dir" ]; then
        notify-send "checkup" "No emacs installed"
        echo "No emacs installed" >&2
        exit 1
    fi

    cd "$emacs_dir" || exit

    # Fetch the latest state from the remote repository without updating local files
    git fetch "$remote_name"

    local_commit=$(git rev-parse HEAD)
    local_commit_date=$(git show -s --date=format:'%d %b %Y' --format=%ad "$local_commit")

    remote_commit=$(git rev-parse "$remote_name/$branch_name")
    remote_commit_date=$(git show -s --date=format:'%d %b %Y' --format=%ad "$remote_commit")

    commits_behind=$(git rev-list --count HEAD.."$remote_name/$branch_name")

    if [ -n "$local_commit" ] && [ -n "$remote_commit" ] && [ "$local_commit" != "$remote_commit" ]; then
        notify-send "Checkup" "Doom Emacs is behind by $commits_behind commits. \nLocal:     $local_commit_date\nRemote: $remote_commit_date."
        diff_url="https://github.com/doomemacs/doomemacs/compare/$local_commit...$remote_commit"
        echo "Doom Emacs is behind by $commits_behind commits."
        echo ""
        echo "$diff_url"
    else
        message="Doom Emacs is up to date. Last commit date: $local_commit_date."
        notify-send "Checkup" "$message"
        echo "$message"
    fi
}

print_help() {
    setup_colors
    printf "${GREEN}$(basename $0)${NOFORMAT}

A script for checking for updates.

Options:
    -h, --help    Show this help message
    --doom        Only check Doom Emacs updates
    (no args)     Run all checks (Doom Emacs, server, sound inbox)
"
}

case "$1" in
    "help" | "--help" | "-h")
        print_help
        ;;
    "--doom")
        check_doom
        ;;
    *)
        check_doom

        if [ -f "$HOME/git/dotfiles/secrets/scripts/server_check" ]; then
            "$HOME/git/dotfiles/secrets/scripts/server_check"
        fi

        if [ -f "/home/owner/git/dotfiles/scripts/gtd_ingest_sound_inbox" ]; then
            "/home/owner/git/dotfiles/scripts/gtd_ingest_sound_inbox"
        fi

        # Sometimes
        if [ "$(seq 1 3 | shuf | head -n 1)" = "1" ]; then
            notify-send "checkup" "Toggling desk position"

            # Switch background
            if [ -f "$HOME/git/dotfiles/sym/linux/dot_local/bin/tools/setbg" ]; then
                "$HOME/git/dotfiles/sym/linux/dot_local/bin/tools/setbg" random
            else
                notify-send "checkup" "No setbg script found"
            fi

            # And desk position
            if height_cm=$(idasen height | awk '{printf "%.d", $1 * 100}'); then
                if [ -n "$height_cm" ]; then
                    if [ "$height_cm" -gt 100 ]; then
                        idasen sit
                    else
                        idasen bal
                    fi
                fi
            else
                notify-send "checkup" "No idasen height found"
            fi

        fi
        ;;
esac
