#!/usr/bin/env bash

declare -a options=(
    "Night People\0icon\x1f~/git/dotfiles/img/night-ppl.jpg"
    "LoFi"
    "Till I Collapse"
    "Zowier\0icon\x1f~/git/dotfiles/img/monocle.png"
)

ensure_spotify_runs() {
    if ! pgrep spotify; then
        spotify &
        sleep 3
    fi
}

spotify_set_loop_status() {
    qdbus org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.LoopStatus "$1"
}

spotify_open_uri() {
    qdbus org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.OpenUri "$1"
}

function run() {
    export SCRATCHPAD_HIDE=T

    case "$1" in
        "Night People")
            ensure_spotify_runs
            spotify_open_uri spotify:track:45wrle3GrO10wrjZyD8hpc
            spotify_set_loop_status "Track"
            scratchpad "^Spotify" "spotify"
            ;;
        "LoFi")
            ensure_spotify_runs
            spotify_open_uri spotify:playlist:0vvXsWCC9xrXsKd4FyS8kM
            spotify_set_loop_status "Playlist"
            scratchpad "^Spotify" "spotify"
            ;;
        "Till I Collapse")
            ensure_spotify_runs
            spotify_open_uri spotify:track:4xkOaSrkexMciUUogZKVTS
            spotify_set_loop_status "Track"
            scratchpad "^Spotify" "spotify"
            ;;
        "Zowier")
            [ -f "$HOME/.private/scripts/zowier_runner" ] && "$HOME/.private/scripts/zowier_runner"
            ;;
    esac

}

choice=$(printf "%b\n" "${options[@]}" | rofi -p "Dispatcher" -i -dmenu -theme ~/.config/rofi/dispatcher.rasi)

if [[ -n "$choice" ]]; then
    run "$choice"
fi
