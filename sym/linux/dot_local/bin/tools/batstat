#!/usr/bin/env sh
# Requires upower and solaar.
# cannot use nix upower on arch because of (porbably) required systemd service

# Script for showing battery percentages of peripherial devices
# Not generic, useful for what I use ;)
battery=BAT0

charge_control_thresh_file="/sys/class/power_supply/$battery/charge_control_end_threshold"

get_charge_threshold() {
    if [ -f "$charge_control_thresh_file" ]; then
        cat $charge_control_thresh_file
    else
        echo "100"
    fi
}

get_laptop_percentage() {
    if laptop_bat=$(cat /sys/class/power_supply/$battery/capacity); then
        charging_status=$(cat /sys/class/power_supply/$battery/status)

        if [ "$(get_charge_threshold)" = "100" ]; then
            echo "$charging_status, ${laptop_bat}%"
        else
            echo "$charging_status, ${laptop_bat}%/$(get_charge_threshold)%"
        fi
    else
        echo "unknown"
    fi
}

notify_battery_status() {
    notify-send "Battery" "Getting statuses"
    status=""

    if laptop_bat=$(cat /sys/class/power_supply/$battery/capacity); then
        charging_status=$(cat /sys/class/power_supply/$battery/status)
        status="Laptop:\n\t$(get_laptop_percentage)"
    fi

    # if mx_master_battery=$(solaar show "MX Master 3 Wireless Mouse" | awk -F ':' '/Battery/{ if($0 ~ /unknown/){exit 1} else {split($2,a,","); print a[1]; exit 0}}'); then
    #     if [ "$mx_master_battery" = " N/A" ]; then
    #         status="$status\n🖱 MX Master 3 :  charging"
    #     elif [ -n "$mx_master_battery" ]; then
    #         status="$status\n🖱 MX Master 3 :  $mx_master_battery"
    #     fi
    # fi

    if wacom_id=$(upower -e | grep "tablet_wacom"); then
        wacom_battery=$(upower -i "$wacom_id" | awk '/percentage/ {print $2}')
        status="$status\n  Wacom Intous:   $wacom_battery"
    fi

    notify-send "Battery" "$status"
}

set_charge_threshold() {
    thresh="$1"

    if [ -z "$thresh" ] || [ "$thresh" -lt 0 ] || [ "$thresh" -gt 100 ]; then
        echo "Wrong threshold given $thresh"
        exit 1
    fi

    echo "$1" | sudo tee $charge_control_thresh_file
}

case "$1" in
    -s | "--set-charge-threshold")
        set_charge_threshold "$2"
        ;;
    -n | --notify)
        notify_battery_status
        ;;
    "--get-charge-threshold")
        get_charge_threshold
        ;;
    "--pretty-stat")
        notify_battery_status
        ;;
    "--print-percentage")
        get_laptop_percentage
        ;;
    "-h")
        echo "Usage: $0 [-s|--set] [-n|--notify]"
        exit 1
        ;;
    *)
        notify_battery_status
        ;;
esac
