#!/usr/bin/env sh
killall wob

if [ -z "$WOBSOCK" ]; then
    notify-send "wob" "WOBSOCK not set"
    exit 1
else
    rm -f "$WOBSOCK"
    notify-send "wob" "$WOBSOCK set"
    mkfifo "$WOBSOCK" && tail -f "$WOBSOCK" | wob
fi
