#!/usr/bin/env bash

date_formatted=$(date "+%a %F %H:%M")

# Returns the battery percentage:
battery_status=$(cat /sys/class/power_supply/BAT0/capacity)

echo "${battery_status}% 🔋 $date_formatted"
