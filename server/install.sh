#!/usr/bin/env bash

# wget https://bitbucket.org/Ja0Dar/dotfiles/raw/master/server/install.sh
# wget https://bitbucket.org/Ja0Dar/dotfiles/raw/master/server/install.sh && bash install.sh

bashrc_addition="$HOME/.bashrc.addition"
bashrc="$HOME/.bashrc"
tools_dir="$HOME/.local/bin/tools"
mkdir -p "$tools_dir"

install_fzf() {
    # wget https://github.com/junegunn/fzf-bin/releases/download/0.20.0/fzf-0.20.0-linux_amd64.tgz -O fzf.tgz
    # tar xzf fzf.tgz
    # rm fzf.tgz
    # mv fzf /usr/bin/fzf

    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install --key-bindings --completion --update-rc
}

install_emacs() {
    sudo add-apt-repository ppa:kelleyk/emacs
    sudo apt update
    sudo apt install emacs26-nox -y
    echo "alias ec=emacs26" >>"$bashrc_addition"
    mkdir ~/.emacs.d
    # wget https://bitbucket.org/Ja0Dar/dotfiles/raw/master/sym/common/.emacs.d_lite/init.el -O ~/.emacs.d/init.el
    echo "TODO ;)"
}

install_master_scripts() {

    for name in dspsl dspsr stack-select; do
        wget https://bitbucket.org/Ja0Dar/dotfiles/raw/master/scripts/docker_master/$name -O "$tools_dir/$name"
        chmod u+x "$tools_dir/$name"
    done

}

apt_install() {
    progs=""

    command -v wget || progs="$progs wget"
    command -v tree || progs="$progs tree"
    command -v git || progs="$progs git"
    command -v tmux || progs="$progs tmux"

    if [ -n "$progs" ]; then
        apt-get update
        apt-get install $progs -y
    fi
}

apt_install

fzf --version || install_fzf

cat >"$HOME/.tmux.conf" <<EOL
set -g mouse on
set -sg escape-time 0
set -g default-terminal "screen-256color"
setw -g mode-keys vi
unbind-key -T copy-mode-vi v
bind-key -T copy-mode-vi 'v' send -X begin-selection # Begin selection in copy mode.
bind-key -T copy-mode-vi 'C-v' send -X rectangle-toggle # Begin selection in copy mode.
bind-key -T copy-mode-vi 'y' send -X copy-selection # Yank selection in copy mode.
EOL

# TODO: make it idempotent

echo "#ADDING CONFIG" >"$bashrc_addition"

wget https:/wget https://bitbucket.org/Ja0Dar/dotfiles/raw/master/scripts/dexec -O "$tools_dir/dexec"
chmod u+x "$tools_dir/dexec"
/bitbucket.org/Ja0Dar/dotfiles/raw/master/scripts/dexec -O "$tools_dir/dexec"
chmod u+x "$tools_dir/dexec"

wget https://bitbucket.org/Ja0Dar/dotfiles/raw/master/scripts/dlog -O "$tools_dir/dlog"
chmod u+x "$tools_dir/dlog"

cat >"$bashrc_addition" <<EOL
alias remap="setxkbmap -option caps:escape"
alias remap_default="setxkbmap -option caps:caps"
alias gc="git commit"
alias ga="git add"
alias gst="git status"
alias rulz="git push"
alias bringz="git pull"

#set -o vi
set -o emacs

alias glod='git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset"'
EOL

echo "PATH=\"\$PATH:$tools_dir\"" >>"$bashrc_addition"

src_line="source $bashrc_addition"

if ! grep -q "$src_line" <"$bashrc"; then
    echo "$src_line" >>"$bashrc"
fi

echo -e "please exec:\nsource \"$HOME/.bashrc\""

# install_emacs
# install_master_scripts
