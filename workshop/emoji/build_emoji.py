#!/usr/bin/env python


from data_dict import EMOJI_DATA


def print_emoji(key: str, value: dict):
    print(key, " ", value["en"])

    if "alias" in value:
        for alias in value["alias"]:
            print(key, " ", alias)


for name, value in EMOJI_DATA.items():
    print_emoji(name, value)
