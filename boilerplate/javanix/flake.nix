{
  description = "Java development environment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        java = "openjdk11";

        # Recreate your package override pattern
        pkgs = import nixpkgs {
          inherit system;
          config = {
            packageOverrides = p: {
              jdk = p.${java};
              jre = p.${java};
            };
          };
        };
      in
      {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            pkgs.${java}
            sbt
            # metals
            # ammonite_2_12
            # ammonite_2_13
          ];

          shellHook = ''
            export JAVA_HOME="${pkgs.${java}}"
          '';
        };
      });
}
