{ pkgs ? import <nixpkgs> { } }:

let
  my-python-packages = python-packages:
    with python-packages; [
      flake8
      black
      ipython
      ipykernel
      pip

      # sqlalchemy
      # psycopg2
      matplotlib
      numpy
      pandas
    ];
  python-with-my-packages = pkgs.python39.withPackages my-python-packages;
in
with pkgs;
mkShell {
  buildInputs = [ python-with-my-packages ];
  shellHook = ''
    # Tells pip to put packages into $PIP_PREFIX instead of the usual locations.
    # See https://pip.pypa.io/en/stable/user_guide/#environment-variables.
    export PIP_PREFIX=$(pwd)/_build/pip_packages
    export PYTHONPATH="$PIP_PREFIX/${pkgs.python39.sitePackages}:$PYTHONPATH"
    export PATH="$PIP_PREFIX/bin:$PATH"
    unset SOURCE_DATE_EPOCH
    # export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath [ pkgs.stdenv.cc.cc ]}
  '';
}
