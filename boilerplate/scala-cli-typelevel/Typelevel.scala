//> using scala "3.3.0"
//> using lib "org.typelevel::toolkit:0.1.6"
//> using lib "com.lihaoyi::pprint:0.8.1"

// examples: https://typelevel.org/toolkit/examples.html

import cats.effect.*
import cats.syntax.all.*

// Http
import org.http4s.ember.client.*
import org.http4s.*
import org.http4s.implicits.*
import org.http4s.circe.*
import _root_.io.circe.Decoder

case class Data(value: String)
given Decoder[Data] = Decoder.forProduct1("data")(Data.apply)
given EntityDecoder[IO, Data] = jsonOf[IO, Data]

def runHttp(method: String) =
  EmberClientBuilder.default[IO].build.use { client =>
    val request: Request[IO] =
      Request(Method.GET, uri"https://example.com/")

    client
      .expect[String](request)
      .flatMap { r => IO.println(r) }
  }

// Cli
import com.monovore.decline.*
import com.monovore.decline.effect.*
val method = Opts.option[String]("method", "", "m").withDefault("GET")
val verbose = Opts.flag("verbose", "", "v").orFalse

object Main extends CommandIOApp("doCurl", "does a curl"):
  def main = (method, verbose).mapN { (method, verbose) =>
    runHttp(method) *>  IO.println(method).as(ExitCode.Success)
  }
