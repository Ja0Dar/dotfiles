#!/usr/bin/env bash
set -euo pipefail

texFile=$1

pdflatex_command="pdflatex -halt-on-error ${texFile}.tex"

case "$OSTYPE" in
    darwin*)
        echo "Make for OSX"
        docker run -i --rm -w "$(pwd)" -v "$(pwd):$(pwd)" tianon/latex $pdflatex_command
        docker run -i --rm -w "$(pwd)" -v "$(pwd):$(pwd)" tianon/latex biber ${texFile}.bcf
        docker run -i --rm -w "$(pwd)" -v "$(pwd):$(pwd)" tianon/latex $pdflatex_command
        docker run -i --rm -w "$(pwd)" -v "$(pwd):$(pwd)" tianon/latex $pdflatex_command
        ;;
    linux*)
        echo "Make for Linux"
        $pdflatex_command
        biber ${texFile}.bcf
        $pdflatex_command
        $pdflatex_command
        ;;
    *) echo "Unsupported os type: $OSTYPE" ;;
esac
