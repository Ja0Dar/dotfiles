{ pkgs ? import <nixpkgs> { } }:


# https://github.com/scalapb/ScalaPB/issues/505
# Comes back up at points, not sure why
(pkgs.buildFHSUserEnv {
  name = "proto-env";

  targetPkgs = pkgs: [ pkgs.sbt pkgs.glibc ];

  runScript = "sbt";
}).env
