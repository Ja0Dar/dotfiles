from flask import Flask, jsonify, request, redirect, Response

app = Flask(__name__)


@app.route("/articles/<int:id>/pretty")
def pretty_route(id):
    print(id)
    return redirect("https://example.com")


@app.route("/logout/<phone>/<device>", methods=["POST"])
def logout(phone, device):
    print("phone: {}, device: {}".format(phone, device))
    return Response(status=200)


@app.route("/headers", methods=["POST", "GET"])
def print_headers():
    print(request.headers)
    return Response(status=200)


@app.route("/authenticate", methods=["POST"])
def auth():
    print(request.get_json())
    resp = jsonify({"phoneNumber": "505505505", "deviceId": "deviceId"})

    resp.headers["X-Correlation-ID"] = request.headers["X-Correlation-ID"]

    return resp


@app.route("/", methods=["POST", "GET"], defaults={"path": ""})
@app.route(
    "/<path:path>",
    methods=["POST", "GET"],
)
def main(path):
    print(request.headers)
    print(path)
    print(request.get_json())
    return Response(status=200)


if __name__ == "__main__":
    app.run(debug=True, port=12345)
