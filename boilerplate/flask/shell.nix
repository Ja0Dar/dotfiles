{ pkgs ? import <nixpkgs> { } }:

let
  my-python-packages = python-packages: with python-packages; [ flask ];
  python-with-my-packages = pkgs.python39.withPackages my-python-packages;
in
with pkgs; mkShell { buildInputs = [ python-with-my-packages ]; }
