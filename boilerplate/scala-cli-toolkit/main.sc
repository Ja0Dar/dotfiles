#!/usr/bin/env -S scala-cli -S 3
//> using scala "3.3.0"
//> using toolkit "latest"
import sttp.model.MediaType
import sttp.model.MediaTypes
import sttp.model.Header
import sttp.model.Headers
import sttp.client4.quick.*
import sttp.client4.Response


def makeCurl(): Response[String] = quickRequest.post(uri"http://localhost:12345/elo")
  .body("""{"ala": "ola"}""")
  .withHeaders(Seq(Header.contentType(MediaType.ApplicationJson)))
  .send()


val response: Response[String] =
  quickRequest.get(uri"http://httpbin.org/ip").send()
val fileName = "my-ip.txt" // write our ip down to "my-ip.txt" file
val ip =
  ujson
    .read(response.body)("origin")
    .str // read "origin" property of the json response
os.write.over(os.pwd / fileName, ip)
