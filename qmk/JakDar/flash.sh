#!/usr/bin/env sh

# based on
# https://docs.qmk.fm/#/getting_started_docker
base="/home/owner/git/foss"

mkdir -p "$base"

# go

# install qmk
if ! [ -e "$base/qmk_firmware" ]; then
    cd "$base" || exit 1
    git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git
    cd - || exit 1
fi

if [ -d "$base/qmk_firmware" ]; then
    rm -rf "$base/qmk_firmware/keyboards/splitkb/kyria/keymaps/JakDar"
    cp -r "/home/owner/git/dotfiles/qmk/JakDar" "$base/qmk_firmware/keyboards/splitkb/kyria/keymaps/JakDar"
fi

export RUNTIME="podman"
cd "$base/qmk_firmware" || exit 1
# ./util/docker_build.sh splitkb/kyria/rev1:JakDar
./util/docker_build.sh splitkb/kyria/rev1:JakDar:flash
